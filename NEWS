NEWS for ttn-pers-elisp


- 1.59 | 2008-03-03

  - Installation changes

    - Dropped imports

      linuxproc.el
      etach.el
      eval-expr.el
      expect.el
      mailcrypt.el (plus mc-{gpg,pgp,pgp5,remail,setversion,toplev}.el)
      rmime.el

    - Dropped libraries

      admin.el
      delayed-checkin.el
      describe-this.el
      diced-message.el
      dired-compile-last-change-log.el
      doc-entry-to-undef.el
      from.el
      gud-cont-display.el
      inline-timestamp.el
      italy-time.el
      macintosh-word-cleanup.el
      oink.el
      phone.el
      ping.el
      pll-spreadsheet.el
      rcs-to-cvs.el
      reorder-headers.el
      reset-emacs-font.el
      reset-x-bell.el
      run-python.el
      snitch.el
      spew.el
      su-shell.el
      toggle-vars.el
      trl.el
      update-changelog.el
      updating-shell-command.el
      uuapply.el
      wup.el
      xscreensaver-command.el

      Some of these were experimental, non-functional, or trivial.
      Others have found new homes for their functionality, either
      wholely or in part.

    - Dropped directory: doc/

      A long journey beginning as a quest for better documentation has
      mired ttn in other systems, with other worries.  Left unwatered,
      husk crumbles senza respira, just some bits for a future dream.

    - All installed Emacs Lisp files now byte-compiled

      Previously, only a double-handful were byte-compiled.  Now, they
      all are on "make all", and in addition, manage to do so w/ no
      warnings (except `noruntime', which is suppressed for "make
      all") with Emacs 23.0.60.

    - No `load-path' munging outside `setup-ttn-pers-elisp'

      The command "make install" now creates an empty file ".nosearch"
      in the directory .../site-lisp/ttn-pers-elisp/, which inhibits
      Emacs from adding subdirectories.  That job is now completely
      handled by `setup-ttn-pers-elisp'.  The result is less junk in
      the `load-path' for users not using ttn-pers-elisp.

    - Use-in-place support

      The function `setup-ttn-pers-elisp' can be used directly from
      the build directory (presuming you don't move things around :-),
      using `load-history-filename-element' to deduce its location.
      Just use `load-file' instead of `require'.

    - VMS support cleaned up

      Under VMS, you can execute the command procedure VMSBUILD.COM to
      do the equivalent of "make all", and use the library in-place.

  - Imported apt-utils.el upgraded (version 2.10.0)

  - Bugfixes

    - `scroll-LR-by-20-minor-mode' toggles `automatic-hscrolling'

      This minor mode used to (uselessly) dynamically bind the
      variable `automatic-hscrolling' to nil.  Now, the command
      `scroll-LR-by-20-minor-mode' toggles that variable.

    - `more-v-c-set-state' now uses `revert-buffer'

      It used to use `vc-update', which was misguided at best.

  - Refinements to existing code

    - `source-wrap' inserts '$State$:$Name$' only for RCS, CVS

    - `insert-time-stamp' can produce UTC

      If the STYLE is a negative integer, take it as positive and
      format UTC instead of current time.  Blech.

    - `release-dired' does nothing more gracefully

      It displays "No files in state `Exp'" in the echo area in that
      case, instead of an empty directory listing.

    - Version control system log prettification revamped

      This used to be implemented as advice around `vc-print-log'
      which worked by changing the buffer text.  That approach was
      suboptimal because it defeated View Log mode motion commands.
      Now, we use `log-view-mode-hook' and `more-vc-pretty-up-log',
      which works using overlays.  There is also new support for Git.

    - Dropped binding: `my-prog-env' no longer binds `C-c C-p'

      That command was some monstrosity of a gdb kludge, best left
      unexamined on a queasy stomach.  Let's let the tides bring in a
      better approach.

  - New stuff

    - New `update-rmanglement' method for Git

      Concomitant w/ this is conversion of `rmangle-BACKEND-file-status'
      to conventional (symbol property) `update-rmanglement' methods.

    - New command: delete-indentation-dwim

      This is provided, and (locally) bound to `M-#',
      by `my-prog-env'.

    - New command: mouse-where-am-i

      Yes, that rat thing is creeping around again.  Let's see how
      long we can tolerate it this time.  This command is bound to
      [(shift mouse-3)] in `my-prog-env'.

    - New library: value-change-dump-mode.el

      This provides a major mode to view Value Change Dump files, such
      as produced by THUD <http://www.gnuvola.org/software/thud/>.
      Note that is just some modest font-locking; it is NOT a waveform
      viewer (although if you want to make it so, patches are welcome).


- 1.58 | 2007-08-25

  - License now GPLv3+ (see COPYING)

  - Dropped libraries

    bg-shell-command.el
    blink.el
    buffer-extract-image.el
    ch-cvs-state.el
    ch-rcs-state.el
    clone-frame.el
    compose-mail.el
    eval-last-page.el
    go.el
    idx-of.el
    ln-mode.el
    ong-da.el
    outline-freshmeat-mail.el
    praise.el
    saved-shell-command.el
    setup-zenirc.el
    ttn-base64.el
    ttn-cvs-cancel-version.el
    turn-on-tnt.el
    zone.el

    Some of these were experimental, non-functional, or trivial.
    Others have found new homes for their functionality, either
    wholely or in part.

  - Dropped command: query-release (see below)

  - New library: prog-env/outside.el

    This combines bg-shell-command.el, saved-shell-command.el,
    and a new string munging func `rewrite-shell-command-w/cd'.

  - New library: prog-env/more-vc.el

    Additional version control functionality.  Some of this may
    eventually wind up in Emacs proper after (years of) testing.

  - New library: editing/ovab.el

    Overl{a}y abbreviate default-directory and other things.
    Variable `ovab-alist' lists what to overlay and their abbrevs.

  - New library: editing/eximlog-mode.el

    This provides eximlog-mode for /var/log/exim4/* files, with
    font-locking of timestamp, "Start", "Completed", router type and
    recipient email address.  Buffer is made read-only, w/ truncated
    lines, and w/ auto-revert-tail mode activated.

  - New command: zonk-definition

  - New command: watch-file-preciously

  - New command: release-dired

    This replaces `query-release', which was bletcherously
    synchronous in the wrong places, not to mention slow and
    temperamental.  In contrast, `release-dired' is quick and
    temperamental and bletcherously synchronous only on the outside.

  - Compilation COMMAND undergoes `rewrite-shell-command-w/cd'

    This is done internally (by advising (feh) `compilation-start'
    or `compile-internal') in order to leave var `compile-command'
    undisturbed.  "But ttn, wait a second!  Compilation already DTRT
    for `cd $foo'!", you might think.  Well, not when foo's value
    begins w/ tilde and the shell does tilde expansion before env
    var expansion (such as is the case for bash).


- 1.57 | 2007-04-20

  - Bugfix: `C-u M-x surround-w-if-block' works as advertized

  - Dropped: low-stress/symbol-at-point.el

    Emacs has thingatpt.el, w/ a better function (that actually
    returns a symbol, not a string) by the same name.

  - Autoload file renamed to ttn-loaddefs.el

    This used to be named loaddefs.el, which might conflict w/
    Emacs' loaddefs.el in some (never experienced personally, but
    we change it anyway to be sure) cases.

  - New imported package: apt-utils.el

  - New imported package: fsm.el

    A temporary measure until we get this fine piece of codified
    thought into Emacs proper (by hook or by crook ;-).

  - New command: switch-window-contents

    Switch the contents of the selected window with next one.
    Optional arg CURSOR-TOO-P means also move the cursor to
    the next window (selecting it and the other buffer).
    If there is only one window, do nothing.

  - Many changes to keybindings

    C-c R       rename-buffer
    C-c _       raise-sexp
    C-j         ppq (Lisp Interaction mode)
    S-return    invisible-cursor-mode
    C-c s-d     disassemble
    C-c r       revert-buffer-t-t
    Scroll_Lock emacs-uptime
    S-/         switch-window-contents
    C-c SPC     transient-mark-mode
    C-c C-m     gnus-no-server
    C-x m       message-mail
    C-c p       undefined

  - Many other small changes (see ChangeLog files)


- See file OLD-NEWS for older news.

	Local Variables:
	mode: outline
	outline-regexp: "\\([ ][ ]\\)*- "
	End:
