#!/bin/sh
# usage: sh -x autogen.sh

autoreconf --verbose --install --symlink --force

# autogen.sh ends here
