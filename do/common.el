;;; common.el
;;;
;;; Copyright (C) 2016 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

(defvar about nil
  "Alist of the project configuration, consulted by `aget'.")

(defun aget (key)
  "Return the value from `about' associated with KEY."
  (cdr (assq key about)))

(defvar verbose (equal "1" (getenv "VERBOSE"))
  "Does env var `VERBOSE' have value \"1\"?")

(defun hey (fmt &rest args)
  "If `verbose', like `message'; otherwise, do nothing."
  (when verbose
    (apply #'message fmt args)))

(defun extend-package-version-based-on-VCS-repo ()
  "Arrange for ‘(aget :version)’ to use VCS info, maybe.
This currently works only for Git.

Normally, PACKAGE-VERSION in a distribution has the form MAJOR.MINOR.
If the build tree has a top-level .git/ subdir, use \"git describe\"
to get more specific info VCSVERS.  If VCSVERS is non-empty,
arrange for ‘(aget :version)’ to return one of:

 VCSVERS
   if PACKAGE-VERSION is a prefix of VCSVERS
   (example: \"1.59-313-gc23b093-dirty\")

 VCSVERS (dist PACKAGE-VERSION)
   otherwise
   (example: \"4.20 (dist 1.59)\")

The latter case is useful for downstream repos, should there be any.  :-D"
  (when (file-directory-p ".git")
    (hey "Found .git/")
    (let* ((cur (assq :version about))
           (was (cdr cur))
           (now (shell-command-to-string
                 (mapconcat #'identity
                            '("git describe"
                              "--tags"
                              "--long"
                              "--match '[0-9]*.[0-9]*'"
                              "--dirty"
                              "2>/dev/null")
                            " "))))
      (if (string= "" now)
          (hey "Could not find any tags of the form MAJOR.MINOR")
        (setf now (concat (substring now 0 -1)
                          "-"
                          (format-time-string "%Y%m%d-%H%M"))
              (cdr cur) (if (let ((len (length was)))
                              (and (eq t (compare-strings was nil nil
                                                          now nil len))
                                   (< len (length now))
                                   (= ?- (aref now len))))
                            now
                          (format "%s (dist %s)" now was)))
        (hey "Tweaked ‘:version’ from %S to %S"
             was (aget :version))))))

;;;---------------------------------------------------------------------------
;;; Do it!

(load (expand-file-name "about.el")
      ;; ‘load-file’ has nil for 3rd argument NOMESSAGE
      nil t t)

(setq

 ;; disable vc
 vc-handled-backends nil

 ;; comma-v.el needs this
 max-specpdl-size (* 2 max-specpdl-size)

 ;; keep the noise down
 backup-inhibited t

 ;; Add more variable assignments here.
 )

;;;---------------------------------------------------------------------------
;;; That's it!

(provide 'common)

;;; common.el ends here
