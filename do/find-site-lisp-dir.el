;;; find-site-lisp-dir.el
;;;
;;; Copyright (C) 2002, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Find first ".../emacs/site-lisp" dir and `message' it.

(message "SITE_DIR=%s" (cl-find "emacs/site-lisp$" load-path
                                :test 'string-match))

;;; find-site-lisp-dir.el ends here
