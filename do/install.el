;;; install.el
;;;
;;; Copyright (C) 2002, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Copy files to installation dirs, making them if necessary.

(require 'common (expand-file-name "do/common.el"))

(defun rm-rf (dir)
  (dolist (f (directory-files dir))
    (unless (member f '("." ".."))
      (let ((name (expand-file-name f dir)))
        (if (not (file-directory-p name))
            (delete-file name)
          (rm-rf name)
          (delete-directory name))
        (hey "Deleted: %s" name)))))

(defun do-the-install! ()
  (let* ((lispdir (expand-file-name "lisp"))
         (parent (file-name-as-directory (car command-line-args-left)))
         subdirs instdir)
    (load (expand-file-name "about.el") nil t t)
    (setq subdirs (mapcar 'symbol-name (aget :subdirs))
          instdir (file-name-as-directory (expand-file-name
                                           (aget :package)
                                           parent)))
    (hey "Installing under: %s" instdir)
    (when (file-directory-p instdir)
      (hey "Clearing out previous under: %s" instdir)
      (rm-rf instdir))
    (cl-flet
        ((install (from to &optional verbose)
                  (copy-file from to t t)
                  (when verbose
                    (hey "Wrote %s" (expand-file-name
                                     (file-name-nondirectory from)
                                     to)))))
      (dolist (dir subdirs)
        (let ((src (expand-file-name dir lispdir))
              (dst (file-name-as-directory (expand-file-name dir instdir))))
          (hey "D: %s" (file-relative-name dst instdir))
          (make-directory dst t)
          (dolist (file (directory-files src t "\\.elc*$"))
            (hey "F: %s" (file-relative-name file lispdir))
            (install file dst))))
      (dolist (rel '(core autoloads setup))
        (install (expand-file-name (format "%s.el" rel)
                                   lispdir)
                 instdir t)))
    (with-temp-file (expand-file-name ".nosearch" instdir))))

(do-the-install!)

;;; install.el ends here
