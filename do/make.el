;;; make.el
;;;
;;; Copyright (C) 2002, 2003, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: emacs --script $0
;;
;; Compile some things then make the setup file.

;;; Code:

(require 'common (expand-file-name "do/common.el"))
(extend-package-version-based-on-VCS-repo)

(when (and (fboundp 'package-initialize)
           (aget :pkginit))
  (let* ((bef (copy-sequence load-path))
         (new (progn (package-initialize)
                     (cl-set-difference
                      (copy-sequence load-path)
                      bef)))
         (len (length new)))
    (hey "%d dir%s added by ‘package-initialize’"
         len (if (= 1 len)
                 ""
               "s"))
    (dolist (dir new)
      (hey "- %s" dir))))

(let* ((lispdir (expand-file-name "lisp"))
       desc
       subdirs)
  (let* ((core (expand-file-name "core.el" lispdir))
         (default-directory (file-name-as-directory
                             (expand-file-name "core" lispdir)))
         (comps (mapcar (lambda (comp)
                          (expand-file-name (format "%s.el" comp)))
                        (aget :core-components))))
    (unless (and (file-exists-p core)
                 (equal (make-list (length comps) t)
                        (mapcar (lambda (comp)
                                  (file-newer-than-file-p core comp))
                                comps)))
      (with-temp-file core
        (insert ";;;; core.el\n\n")
        (dolist (comp comps)
          (insert-file-contents comp)
          (when (search-forward "\n;;; Description: " nil t)
            (delete-region (match-beginning 0) (point))
            (push (delete-and-extract-region (point) (line-end-position)) desc))
          (goto-char (point-max)))
        (insert "\n;;;; core.el ends here")
        (goto-char (point-min))
        (search-forward "\n;;;\n\n")
        (forward-char -1)
        (insert ";;; Description: " (mapconcat 'identity
                                               (reverse desc)
                                               "  ")
                "\n"))
      (hey "Wrote %s" core)))
  (setq subdirs (mapcar (lambda (dir)
                          (expand-file-name dir lispdir))
                        (mapcar 'symbol-name (aget :subdirs))))
  (setq load-path (append subdirs load-path))
  (let ((byte-compile-warnings '(not noruntime cl-functions))
        (byte-compile-verbose verbose))
    (dolist (dir subdirs)
      (byte-recompile-directory dir 0)))
  (let ((setup "lisp/setup.el")
        (template "lisp/setup.in"))
    (when (or
           ;; elephant (search-backward "package-version" nil nil 2)
           (string-match "-" (aget :version))
           ;; mouse
           (not (file-exists-p setup))
           (not (file-newer-than-file-p setup template)))
      (with-temp-file setup
        (insert-file-contents template)
        (while (re-search-forward "@\\(:[-a-z]+\\)@" nil t)
          (let* ((var (intern (match-string 1)))
                 (val (or (aget var)
                          (error "PEBKAC: Invalid config var: ‘%S’"
                                 var))))
            (replace-match (format "%s" val)
                           t t))))
      (hey "Wrote %s" setup))))

;;; make.el ends here
