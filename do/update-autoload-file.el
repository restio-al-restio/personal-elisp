;;; update-autoload-file.el
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: emacs --script $0 FILENAME
;;
;; Update autoload file FILENAME.
;; Temporarily set (let-bind) `generated-autoload-file' to FILENAME,
;; then determine FILENAME's directory's immediate subdirectories and
;; apply `update-directory-autoloads' to them.

;;; Code:

(require 'common (expand-file-name "do/common.el"))
(require 'autoload)

(let* ((uda (cond ((fboundp 'update-directory-autoloads)
                   'update-directory-autoloads)
                  ((fboundp 'update-autoloads-from-directories)
                   'update-autoloads-from-directories)
                  (t (error "Cannot determine `uda'"))))
       (generated-autoload-file (expand-file-name
                                 (car command-line-args-left)))
       (lispdir (file-name-directory generated-autoload-file))
       (subdirs-el (expand-file-name
                    "subdirs.el" (file-name-directory
                                  generated-autoload-file)))
       dirs)
  (dolist (x (directory-files lispdir))
    (cond ((member x '("." "..")))
          ((file-directory-p (setq x (expand-file-name x lispdir)))
           (push x dirs))))
  (hey "Updating autoload file: %s" generated-autoload-file)
  ;; Emacs checks for existence of subdirs.el; if found, the filenames
  ;; are basename only; otherwise, the relative name.  We want basename
  ;; only for hysterical raisons, but don't use the normal subdirs.el
  ;; mechanism, hence its temporary existence around the ‘uda’ call.
  (call-process "touch" nil nil nil subdirs-el)
  (apply uda dirs)
  (delete-file subdirs-el))

;;; update-autoload-file.el ends here
