;;; update-readme-files.el
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: emacs --script $0 FILE...
;;
;; For each FILE, update its tail using the command specified therein.
;; This makes FILE writable if it is not so already.

;;; Code:

(require 'common (expand-file-name "do/common.el"))

(dolist (readme-file command-line-args-left)
  (let ((f (expand-file-name readme-file)))
    (hey "Updating README file: %s" readme-file)
    (unless (file-writable-p f)
      (set-file-modes f (logior #o200 (file-modes f))))
    (find-file f)
    ;; Look for " output from command:"; read and eval the form following
    ;; it as a shell command; look for regexp "will be lost.*\n-+\n";
    ;; execute the shell command, replacing text from end of regexp to eob
    ;; with the output of the command; insert a newline (if necessary, to
    ;; have one blank line) and "# FILENAME ends here\n", where FILENAME is
    ;; the result of `(file-name-nondirectory (buffer-file-name))'.
    (search-forward " output from command:")
    (let ((command (eval (read (current-buffer)))))
      (re-search-forward "will be lost.*\n-+\n")
      (delete-region (point) (point-max))
      (shell-command command t))
    (goto-char (point-max))
    (insert (if (string= "\n\n" (buffer-substring (- (point) 2) (point)))
                "" "\n")
            "# " (file-name-nondirectory (buffer-file-name))
            " ends here\n")
    (save-buffer)))

;;; update-readme-files.el ends here
