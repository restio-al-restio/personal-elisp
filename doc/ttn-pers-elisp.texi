\input texinfo
@c %**start of header
@setfilename ttn-pers-elisp.info
@documentencoding UTF-8
@settitle TTN Personal Elisp
@ifinfo
@paragraphindent 0
@end ifinfo
@c %**end of header

@c Copyright (C) 2020 Thien-Thi Nguyen

@dircategory Emacs
@direntry
* ttn-pers-elisp: (ttn-pers-elisp).     Parenthetical ponderings by ttn.
@end direntry

@copying
Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.

Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided that
the entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Free Software Foundation.
@end copying

@syncodeindex tp cp

@include version.texi

@titlepage
@title TTN Personal Elisp

@author Thien-Thi Nguyen
@page
@vskip 0pt plus 1filll

Copyright @copyright{} 2020 Thien-Thi Nguyen

@insertcopying
@end titlepage

@summarycontents
@contents

@c ---------------------------------------------------------------------------
@node Top
@top TTN Personal Elisp

@ifinfo
TTN Personal Elisp is a collection of Emacs Lisp libraries written by
Thien-Thi Nguyen (and selected guests).
This documentation is the @value{UPDATED} Edition,
corresponding to TTN Personal Elisp @value{VERSION}.

@quotation
@insertcopying
@end quotation
@end ifinfo

@menu
* Introduction::

Indices
* Function Index::
* Variable Index::
* Concept Index::
@end menu

@node Introduction
@chapter Introduction

@node Function Index
@unnumbered Function Index

@printindex fn

@node Variable Index
@unnumbered Variable Index

@printindex vr

@node Concept Index
@unnumbered Concept Index

@printindex cp

@c To prevent the Concept Index's last page from being numbered "i".
@page

@bye
