;;; build-emacs.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Build a minimal GNU Emacs.

(require 'outline)

(defvar build-emacs-log nil
  "Buffer logging progress of a GNU Emacs build.")

(defun build-emacs-log (&rest args)
  (unless (equal (current-buffer) build-emacs-log)
    (error "`build-emacs-log' called incorrectly"))
  (let ((msg (apply 'format args)))
    (outline-hide-body)
    (insert "* " msg "\n")
    (message msg)))

(defun build-emacs-call (&rest cmd)
  (let ((cmd-string (mapconcat 'identity cmd " ")))
    (build-emacs-log "Doing: %s" cmd-string)
    (let ((result (apply 'call-process
                         (car cmd) nil build-emacs-log t (cdr cmd))))
      (unless (zerop result)
        (error "Failed: %s" cmd-string)))))

(defun build-emacs-hack-preloads ()
  (build-emacs-log "(build-emacs-hack-preloads) does nothing currently")
  t)

;;;###autoload
(defun build-emacs (tar-file configure-opts)
  "Build a minimal GNU Emacs."
  (interactive "fTar file: \nsConfigure options: ")
  (setq tar-file (expand-file-name tar-file))
  (setq build-emacs-log (generate-new-buffer "*Build Emacs Log*"))
  (switch-to-buffer build-emacs-log)
  (outline-mode)
  (build-emacs-log "Starting: %s" (format-time-string "%c"))
  (cd (file-name-directory tar-file))
  (build-emacs-log "Working directory: %s" default-directory)
  (let ((dir (substring (file-name-nondirectory tar-file) 0 -7)))
    (unless (and (file-exists-p dir) (file-directory-p dir))
      (build-emacs-call "tar" "xvzf" tar-file))
    (cd dir)
    (build-emacs-log "Working directory: %s" default-directory))
  (apply 'build-emacs-call "./configure" (split-string configure-opts))
  (build-emacs-hack-preloads)
  (build-emacs-call "make")
  (build-emacs-log "Finished: %s" (format-time-string "%c"))
  (setq build-emacs-log nil))

(provide 'build-emacs)

;;; build-emacs.el ends here
