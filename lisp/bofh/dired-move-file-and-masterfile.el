;;; dired-move-file-and-masterfile.el               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1999, 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Move file and masterfile to new directory.

(require 'dired)

;;;###autoload
(defun dired-move-file-and-masterfile (dest)
  "Move current file and its RCS masterfile to directory DEST.
The masterfile is assumed to be in subdirectory \"RCS\"."
  (interactive "DMove to directory: ")
  (setq dest (directory-file-name dest))
  (let* ((mdest (concat dest "/RCS"))
         (file (dired-get-filename))
         (base (file-name-nondirectory file))
         (mfile (format "%sRCS/%s,v" (file-name-directory file) base)))
    (when (file-directory-p file)
      (error "Can only move files"))
    (unless (file-exists-p mfile)
      (error "Masterfile does not exist"))
    (unless (file-directory-p mdest)
      (error "Destination RCS dir does not exist"))
    (rename-file file (format "%s/%s" dest base))
    (rename-file mfile (format "%s/%s,v" mdest base))
    (dired-do-redisplay)))

(provide 'dired-move-file-and-masterfile)

;;; dired-move-file-and-masterfile.el ends here
