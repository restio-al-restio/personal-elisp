;;; gxrepl.el                                       -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Run a Guile-PG repl in a buffer.

(defvar gxrepl-default-host nil
  "Default \"host\" for connection via `gxrepl'.")

;;;###autoload
(defun gxrepl (dbname &optional host)
  "Connect interactively to database DBNAME using the gxrepl.
Optional arg HOST is a string specifying either the hostname \(or
ip address\), or a directory name where the PostgreSQL socket
lives.  When called interactively, a prefix arg means to query
for host-as-hostname, otherwise query for host-as-directory."
  (interactive
   (let* ((host (if current-prefix-arg
                    (read-string "Host (hostname or ip address): "
                                 "localhost")
                  (read-directory-name "Host (socket dir): "
                                       gxrepl-default-host
                                       gxrepl-default-host
                                       t)))
          (minus-h (if (string= "" host)
                       host
                     (format " -h %S" (expand-file-name host))))
          (dbname (completing-read
                   "Database: "
                   (split-string (shell-command-to-string
                                  (format "psql%s -ltA | sed 's/|.*//'"
                                          minus-h)))
                   nil t)))
     (list dbname host)))
  (when (string= "" dbname)
    (setq dbname (user-login-name)))
  (setq host (cond ((string= "" host) nil)
                   ((file-directory-p host) (directory-file-name
                                             (expand-file-name host)))
                   (t host)))
  (let ((conn-spec '(dbname)))
    (when host (push 'host conn-spec))
    (switch-to-buffer
     (make-comint (concat "gxrepl " dbname)
                  "guile" nil "-c"
                  (format "%S %S"
                          '(use-modules (database postgres-gxrepl))
                          `(gxrepl ,(mapconcat
                                     (lambda (v)
                                       (format "%s='%s'" v
                                               ;; Under dynamic binding,
                                               ;; we used to be able to use:
                                               ;;- (symbol-value v)
                                               ;; but w/ lexical binding,
                                               ;; we need to map explicitly:
                                               (eval v `((dbname . ,dbname)
                                                         (host . ,host)))))
                                     conn-spec
                                     " ")))))))

(provide 'gxrepl)

;;; gxrepl.el ends here
