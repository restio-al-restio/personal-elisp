;;; process-menu-delete-process.el                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: In process menu, ‘k’ deletes process at point.

;;;###autoload
(defun process-menu-delete-process (&optional force)
  "Delete process at point, asking for confirmation first.
Prefix arg means don't ask."
  (interactive "P")
  (let ((proc (tabulated-list-get-id)))
    (unless (processp proc)
      (error "No process here"))
    (when (or force (y-or-n-p (format "Delete process `%s'?" proc)))
      (let ((line (count-lines (line-beginning-position)
                               (point-min))))
        (delete-process proc)
        (revert-buffer t t)
        (forward-line line)))))

(provide 'process-menu-delete-process)

;;; process-menu-delete-process.el ends here
