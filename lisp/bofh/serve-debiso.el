;;; serve-debiso.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2010 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Run ‘ttn-do serve-debiso’ in a buffer.

(defvar serve-debiso-directory nil
  "Default-directory for ‘serve-debiso’ operation.")

;;;###autoload
(defun serve-debiso ()
  "Run \"ttn-do serve-debiso\" in a buffer.
But first, do dired in ‘serve-debiso-directory’.
Signal error if that is nil."
  (interactive)
  (dired (or serve-debiso-directory
             (user-error "Sorry, ‘serve-debiso-directory’ not set")))
  (let ((name "serve-debiso"))
    (cond ((get-process name)
           (list-processes)
           (message "(Process %s already started.)" name))
          (t (message "Started: %s"
                      (let ((buf (get-buffer-create
                                  (format "*ttn-do %s*" name))))
                        (start-process name buf "ttn-do" name)
                        (switch-to-buffer buf)))))))

(provide 'serve-debiso)

;;; serve-debiso.el ends here
