;;; signal-process-at-point.el                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Context-sensitive ‘signal-process’.

;;;###autoload
(defun signal-process-at-point (pid sig)
  "Like ‘signal-process’ except PID is read from point if called interactively."
  (interactive (list (number-at-point)
                     (read-string "Signal: ")))
  (when (stringp sig)
    (setq sig (read sig)))
  (signal-process pid sig))

(provide 'signal-process-at-point)

;;; signal-process-at-point.el ends here
