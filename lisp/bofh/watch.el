;;; watch.el                                        -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2003, 2004, 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Revulsion or reversion?  You be the judge!

;;;###autoload
(defun watch (command slackfulness)
  "Execute shell COMMAND every SLACKFULNESS seconds, displaying output.
Type any key to stop command execution (the key is passed to Emacs)."
  (interactive "sShell Command: \nnSeconds between redisplay: ")
  (switch-to-buffer
   (get-buffer-create
    (format "watch (%d sec): %s" slackfulness command)))
  (buffer-disable-undo)
  (let (stop)
    (while (not stop)
      (erase-buffer)
      (shell-command (concat "date;" command) (current-buffer))
      (goto-char (point-min))
      (setq stop (not (eq t (sit-for slackfulness))))))
  (rename-buffer (format "(watch stopped): %s" command)))

(defun bury-if-read-only ()
  "If buffer is writable, return t, otherwise bury it and return nil.
When burying, show message:
  (Burying BUFFER-NAME -- make writable to really kill.)
This function is intended to be added to `kill-buffer-query-functions'."
  (cond ((not buffer-read-only))
        (t (message "(Burying %s -- make writable to really kill.)"
                    (buffer-name))
           (bury-buffer)
           nil)))

;;;###autoload
(defun watch-file-preciously (filename &optional tail)
  "Watch file named by FILENAME preciously.
This means it is found read-only with Auto-Revert mode enabled, w/
`bury-if-read-only' added to the local `kill-buffer-query-functions'.
Prefix arg non-nil means use Auto-Revert-Tail mode, instead."
  (interactive "fFilename: \nP")
  (find-file-read-only filename)
  (add-hook 'kill-buffer-query-functions 'bury-if-read-only nil t)
  (funcall (if tail 'auto-revert-tail-mode 'auto-revert-mode) 1))

(provide 'watch)

;;; watch.el ends here
