;;; do-it-now.el
;;;
;;; Copyright (C) 1996, 1997, 1998, 2000, 2002,
;;;   2003, 2004, 2006, 2007, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: No definitions, just actions.


;;;---------------------------------------------------------------------------
;;; Inhibit backup variously.

(never-backup-add-regexps
 "/[.]git/info/\\(attributes\\|exclude\\)$"
 "/[.]git/config$"
 "/[.]git/COMMIT_EDITMSG$"
 ;; Add more regexps here.
 )


;;;---------------------------------------------------------------------------
;;; Ensure the agreeableness of things.

(cl-loop for (setting . things)
         in '((-1
               ;; Add modes to turn off, here.
               line-number-mode
               column-number-mode
               menu-bar-mode
               scroll-bar-mode
               tooltip-mode
               blink-cursor-mode
               global-eldoc-mode
               tool-bar-mode
               electric-indent-mode)
              (+1
               ;; Add modes to turn on, here.
               transient-mark-mode)
              ;; Add unorthodox modes here.
              (buffers ido-mode))
         do (cl-loop for thing in things
                     do (when (fboundp thing)
                          (funcall thing setting))))


;;;---------------------------------------------------------------------------
;;; Keep track of time.

(display-time)


;;;---------------------------------------------------------------------------
;;; Window system stuff.

(when (or window-system
          (and (fboundp 'display-color-p)
               (display-color-p)))
  (unless noninteractive
    (paren-activate)
    ;; works w/ mic-paren.el version 3.7
    (setq paren-sexp-mode         t
          paren-display-message  'never
          paren-match-face       'bold))

  (cond (window-system
         (set-theme 'new-earthy))
        (t
         (set-theme 'dream)
         (set-face-foreground 'highlight "white")))

  (set-cursor-color "lightblue")
  (set-face-attribute 'region nil
                      :foreground "green"
                      :background "black")
  (set-face-background 'highlight "gray20")

  (global-font-lock-mode t)

  ;; old skool
  (when (fboundp 'fringe-mode)
    (fringe-mode 0)))


;;;---------------------------------------------------------------------------
;;; Poor man's face customization.

(mapc (lambda (spec)
        (apply 'set-face-attribute (car spec) nil (cdr spec)))
      '((font-lock-comment-face :foreground "moccasin")
        (font-lock-string-face :foreground "orange")
        (font-lock-keyword-face :bold t :foreground "skyblue")
        (match :background "gray20")))


;;;---------------------------------------------------------------------------
;;; Aliases

(defalias 'fdap 'dired-at-point)
(defalias 'make 'compile)
(defalias 'next-buffer 'bury-buffer)


;;; do-it-now.el ends here
