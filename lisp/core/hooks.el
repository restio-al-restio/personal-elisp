;;; hooks.el
;;;
;;; Copyright (C) 1996, 1997, 1998, 2000, 2002,
;;;   2003, 2004, 2005, 2006, 2007, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: Hooks: Load time, normal and advice.

(defun inhibit-backup-locally ()
  "Make `backup-inhibited' buffer-local and set it to t."
  (interactive)
  (set (make-local-variable 'backup-inhibited) t))


;;;---------------------------------------------------------------------------
;;; Load time hooks.
;;; Some files are pre-loaded in the dumped Emacs;
;;; their associated forms are evaluated immediately.

(let (file) (while (setq file (eval (read)))
              (eval-after-load file (read))))

"text-mode"
(define-keys text-mode-map
   ;; 2009-06-21 16:50:09.  "they're all in loony houses, yeah!"
   [(meta ?T)] 'transpose-paragraphs)

"subr"
(define-keys minibuffer-local-completion-map
  ;; 2005-08-29 09:21:39.  "hang on... just a little bit longer now..."
  ;; beautiful drop from tonic to the fifth, sweeping down one whole for a bit
  ;; to end up back on the fifth for the hold and recycle prep.  i miss it.
  "\M-o" (lambda () (interactive)
           (when (equal ?/ (char-before))
             (delete-char -1))
           (when (re-search-backward "/[^/]+" (point-min) t)
             (forward-char 1)
             (delete-region (point) (match-end 0))))
  ;; 2000/02/02 12:00:34.  a beautiful day, warm breeze blowing in, hiding the
  ;; sound of traffic.  i think of friends on the east coast and my parents,
  ;; who are hip-deep in snow.
  "~" (lambda ()
        (interactive)
        (insert (if (let ((c (char-before (point))))
                      (or (not c) (/= ?/ c)))
                    "~"
                  ;; this doesn't work w/ emacs 21 because the prompt is
                  ;; considered part of the buffer and has `read-only' text
                  ;; property.
                  ;;- (erase-buffer)
                  ;; so instead, we zonk manually...
                  (while (ignore-errors (delete-char -1) t))
                  "~/"))))

;; 2001/11/06 17:18:40.  silence.

"view"
(define-keys view-mode-map
  "{" 'backward-page-ignore-narrow
  "}" 'forward-page-ignore-narrow)

;; 2004/07/08 14:21:00.  piova leggermente.  ucelli matti come al solito.

"subr"
(define-keys minibuffer-local-map "\M-m" 'exit-minibuffer)

"emacs-lisp/lisp-mode"
(progn
  (define-keys lisp-interaction-mode-map
    "\C-j" 'ppq
    "\C-\M-z" (defun zonk-definition ()
                "Do `makunbound' or `fmakunbound' on current top-level form.
Additionally, delete the form and any extra blank lines."
                (interactive)
                (beginning-of-defun)
                (let ((p (point))
                      (form (read (current-buffer))))
                  (funcall (cl-case (car form)
                             ((setq) 'makunbound)
                             ((defun defmacro) 'fmakunbound)
                             (t 'identity))
                           (cadr form))
                  (delete-region p (point)))
                (delete-blank-lines)))
  (define-keys emacs-lisp-mode-map
    "\C-\M-z" 'zonk-definition))

"help"
(define-keys help-map
  "A" 'apropos-command
  "a" 'apropos
  "\C-o" (lambda (ref) (interactive "sDisplay Commentary: ")
           (let ((buf (format "*Commentary* %s" ref)))
             (shell-command (format "guile-tools display-commentary %S"
                                    ref)
                            buf)
             (set-buffer buf)
             (view-mode 1)
             (set (make-local-variable 'view-scroll-auto-exit) nil)
             (setq view-exit-action 'kill-buffer))))

"vc-hooks"
(define-keys vc-prefix-map
  "p" 'display-previous-revision)

"isearch"
(define-keys isearch-mode-map
  [(control meta z)] (defun ttn-isearch-yank-symbol () (interactive)
                       ;; 2010-02-03 10:13:35.
                       ;; symbols, words, sounds, meanings.
                       ;; cruft for cutting, pride for preenings.
                       (isearch-yank-string (thing-at-point 'symbol))))

"bookmark"
(define-keys bookmark-map
  "?" (lambda () (interactive)
        (message "Make Jump Save Load Edit Rename Delete Insert")))

"dired"
(define-keys dired-mode-map
  "\C-m" 'dired-view-file
  "q"    (lambda () (interactive) (kill-buffer nil))
  "z"    'gzip-or-gunzip-from-dired
  "W"    'dired-wipe
  "/"    'dired-mark-directories
  "@"    'dired-mark-symlinks
  "U"    'browse-url-of-dired-file
  ;; 1998.1010.00:37:10-0700.  KCSM 91FM commercial-free jazz.  a standard,
  ;; "feelings"... original tune recorded in 1978.  strange, this is more
  ;; "easy-listening" than usual, but i guess the hour is late and bebop
  ;; might keep one awake.  because they are commercial-free, KCSM must
  ;; periodically undergo a pledge-drive, where they interrupt the usually
  ;; continuous music with annoying begging.  we are currently in the midst
  ;; of one of these month-long drives, but luckily it will be ending soon
  ;; (sunday).
  "I" (lambda () (interactive)
        (info (dired-get-filename)))
  ;; 1998.1015.00:11:28-0700.  92.3 KSJO mainstream rock station.  guns and
  ;; roses -- "knocking on heaven's door".  tomorrow (actually right now)
  ;; is lucy's birthday, the ides of october, i wonder if she's planning to
  ;; do anything special.  will give her a call...
  ;;
  ;; finally got hideshow.el xemacs-compatible.  seems silly to me that
  ;; emacs and xemacs should diverge, the why-can't-we-all-work-together
  ;; part of me is saddened.  although, i suppose personal experience
  ;; should teach me that sometimes viewpoints are so tenaciously held that
  ;; wresting compromise from the hardness of the righteous heart is too
  ;; much to ask.
  "\C-c\C-r" (lambda () (interactive)
               (save-window-excursion
                 (release (dired-get-filename)))
               (dired-do-redisplay)
               (dired-next-line 1))
  ;; 1999/06/05 13:39:13.  more dired fun.
  "_" (lambda () (interactive) (hexl-find-file (dired-get-filename)))
  ;; 2007-08-25 17:52:59.  meno male, ritrovato <http://www.kfjc.org>.
  "\C-c\C-d" (lambda ()
               "Do `edb-interact' or `db-find-file' on current file."
               (interactive)
               (let ((filename (dired-get-filename)))
                 (if (string-match "[.]edb$" filename)
                     (edb-interact filename nil)
                   (db-find-file filename)))))

"ebuff-menu"
(define-keys electric-buffer-menu-mode-map
  "\C-b" (lambda () (interactive)
           (let (buffer-read-only)
             (keep-lines "\\(Inferior Scheme\\)\\|\\(Comint\\)")))
  "\C-o" (lambda () (interactive)
           (let (buffer-read-only)
             (call-interactively 'keep-lines))))

"compile"
(progn
  ;; 2009-07-04 07:16:09.  ucelli.
  (put 'compile-history 'history-length t)
  (add-to-list
   'compilation-finish-functions
   (defun ttn-hey-compilation-finished! (buf status-string)
     ;; We used to check if ‘buf’ was ‘(car (buffer-list))’ but that's too
     ;; indirect; better to check visibility directly via window association.
     (unless (eq (window-buffer) buf)
       (with-current-buffer (window-buffer)
         (yo! (substring status-string 0 -1) 'funky-flash)))))
  (define-keys compilation-mode-map
    "\C-c\C-w" 'more-vc-copy-ref-as-kill
    "\C-c\C-r" 'more-vc-git-rebase
    "\C-c\C-s" 'more-vc-git-show
    [(meta ?\s)] 'scroll-up-line
    "." 'end-of-buffer
    "\C-c\C-q" 'kill-compilation
    ":" (lambda (line) (interactive "sLine: ")
          (let ((proc (get-buffer-process (current-buffer)))
                (buffer-read-only nil))
            (goto-char (process-mark proc))
            (insert (propertize line 'font-lock-face 'bold) "\n")
            (set-marker (process-mark proc) (point)))
          (process-send-string (buffer-name) (concat line "\n")))
    "\C-c\C-v" (lambda () (interactive)
                 (find-file (thing-at-point 'filename))
                 (vc-print-log))))

"isearch"
(define-key isearch-mode-map
  "\C-?" (lambda () (interactive)       ; from gnu.emacs.help probably
           (setq isearch-string
                 (substring isearch-string 0 -1)
                 isearch-message
                 (mapconcat 'isearch-text-char-description
                            isearch-string ""))
           (isearch-update)))

"make-mode"
(add-to-list
 'makefile-font-lock-keywords
 (list
  (concat "[$][({]"
          (regexp-opt
           (cons "call" (mapcar 'car makefile-gnumake-functions-alist))
           'words))
  1 font-lock-keyword-face)
 'append)

"vc-rcs"
(setup-anticipatory-vc-rcs-annotation)

"vc-cvs"
(progn
  (fset 'vc-cvs-show-log 'pretty-up-vc-print-log-output)
  (if (fboundp 'vc-rollback)
      (unless (fboundp 'vc-rcs-rollback)
        (defalias 'vc-cvs-rollback 'ttn-cvs-rollback))
    (unless (fboundp 'vc-cvs-cancel-version)
      (defalias 'vc-cvs-cancel-version 'ttn-cvs-cancel-version))))

"man"
(define-keys Man-mode-map
  "\M-n" 'down-holdcursor
  "\M-p" 'up-holdcursor
  "f"    'Man-follow-manual-reference
  "\C-m" 'Man-follow-manual-reference)

"comint"
(progn
  (add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m t)
  (define-keys comint-mode-map
    "\C-a" 'comint-bol
    ;; 1998.0129.23:58:54-0800.  silence.  getting hungry.
    ;; this is support for the "go" programming convention.
    "\M-\C-g" 'go
    ;; 2004-12-04 14:28:23.  che bello poco movimento.
    "\M-m" (lambda () (interactive)
             (cl-case last-command
               ((comint-previous-input comint-next-input move-end-of-line)
                (comint-send-input))
               (t (call-interactively (lookup-key global-map "\M-m")))))
    ;; 2006-08-31 13:13:38.  whir nowhir whir nowhir whir.
    ;;   Gotta make rent, gotta track sent,
    ;;     don't forget received, and all that was spent.
    ;;   Zero is not failure if mind can twist to tailor
    ;;     that which only ghosts can find to be more paler.
    ;;   New line to nowhere, decide now to go there,
    ;;     loss is a circle whose endpoint is stowed, shared.
    "\M-j" 'erase-buffer))

"sendmail"
(define-keys mail-mode-map
  "\M-&" (lambda (tag) (interactive "sTag: ")
           (rename-buffer (concat "*mail-" tag "*"))))

"help-mode"
(define-keys help-mode-map
  "\M-r" (lambda () (interactive)       ; remember
           (rename-buffer
            (concat "*Help* "
                    (buffer-substring-no-properties
                     (point) (line-end-position))))))

"scheme"
(progn
  ;; 2009-05-11 11:01:34.  silenzio (quasi).
  (add-hook 'scheme-mode-hook
            (defun ttn-scheme-set-macro-indentation ()
              (save-excursion
                (goto-char (point-min))
                (while (re-search-forward
                        (concat
                         "^(def\\S-+\\Sw+\\(\\S-+\\)[^\n]+\n"
                         ;; TODO: parameterize
                         "  ;;#;(declare (indent \\(.\\)")
                        nil t)
                  (put (intern (match-string 1)) 'scheme-indent-function
                       (string-to-number (match-string 2)))))))
  ;; 2009-04-27 02:11:46.  pioggia.  in realtà, il problema è che scheme.el
  ;; (che definisce `scheme-mode-syntax-table' e `scheme-font-lock-keywords')
  ;; impone il sintassa «word» su [0-9a-zA-Z].  per DTRT, quei caratteri
  ;; dovrebbero rimanere «symbol» e i font-lock keywords specificati con
  ;; "\\s +" invece di "\\sw+".
  (mapc (lambda (x)
          (modify-syntax-entry x "w" scheme-mode-syntax-table))
        "àèìòùÀÈÌÒÙéÉ")
  (font-lock-add-keywords
   'scheme-mode
   ;; 2009-04-11 16:57:45.  borbotta la lavatrice.  meglio per Emacs 23 così.
   '(("\\<\\(def\\(ine-\\)*macro\\)\\>[() ]+\\(\\sw+\\)"
      (1 font-lock-keyword-face)
      (3 font-lock-type-face))
     ("\\<\\(define-module\\)\\>\\s-+\\(([^()]+)\\)"
      (1 font-lock-keyword-face)
      (2 font-lock-constant-face))
     ("\\<\\(define-[^() ]+\\)\\>[() ]+\\([^() ]+\\)"
      (1 font-lock-keyword-face)
      (2 font-lock-function-name-face))))
  ;; 2005-08-23 12:23:26.  trapano e martello degli vicini lavorando.
  (ignore-errors (load-file "~/.emacs.d/ttn/scheme-indent-function-1.el"))
  ;; 2005-08-15 21:52:42.  random boombox.  some emacsen define this to
  ;; have string delim syntax, which is allowed, i suppose, but ugly...
  (modify-syntax-entry ?| "_ 23b" scheme-mode-syntax-table)
  ;; 1997.1122.02:57:53-0800.  middle of the night, why didn't i think of
  ;; this sooner?!
  (define-keys scheme-mode-map
    "\M-\C-m" 'scheme-send-definition-and-go))

;; 1998.1013.00:27:42-0700.  89.7 KFJC college station.  strange
;; noise-type sounds, but works for hacking since mostly instrumental.
;; i remember when living at 421 century getting seriously twisted and
;; hacking THUD to this radio station...

"outline"
(define-keys outline-mode-map
  "\C-cF" 'fixup-NEWS
  "\C-cN" 'insert-new-NEWS
  "\C-c\C-h" 'hide-subtree)

;; 1999/04/19 00:33:52.  silence.

"gud"
(define-keys gud-mode-map
  [f9]  'gud-cont
  [f10] 'gud-finish
  [f11] 'gud-step
  [f12] 'gud-next)

;; 1999/10/04 08:24:13.

"sgml-mode"
(define-keys sgml-mode-map
  [f11] (lambda () (interactive) (execute-kbd-macro 'spew)))

;; 2008-06-24 17:09:19.  canzione russa tristissima.
;; purtroppo Emacs 23 vc è un casino grazie ESR (ed altri).  :-/

(or (ignore-errors
      (cadr (symbol-function 'vc-annotate)))
    "vc")
(define-keys vc-annotate-mode-map
  "a" 'vc-annotate-display-autoscale)

"diff-mode"
(progn
  (when (facep 'diff-header)
    (set-face-background 'diff-header "gray20"))
  (when (facep 'diff-refine-change)
    (set-face-attribute 'diff-refine-change nil
                        :background 'unspecified
                        :foreground "green"))
  (define-keys diff-mode-map
    "\C-cU" 'update-diff
    "\M-o" nil
    "\M-r" 'diff-auto-refine-mode
    "\M-z" (defun diff-apply-hunk-to-old-and-delete-region ()
             "Does `diff-apply-hunk' and then `delete-region'.
This modifies the \"old\" file by let-binding `diff-jump-to-old-file'."
             (interactive)
             (let ((diff-jump-to-old-file t)
                   (inhibit-read-only t)
                   (p (point)))
               ;; This changes point...
               (diff-apply-hunk)
               ;; ...so that this can DTRT.
               (delete-region p (point))))))

"grep"
(setq grep-find-command
      ;; Boggle -- why is this here and not in vars.el?
      ;; That's because Emacs requires `grep-compute-defaults'
      ;; to be called first else this value is clobbered.
      ;; Things became even more complicated in Emacs 22, hmmm.
      (concat "find . "
              (mapconcat (lambda (subdir)
                           (format "-name %s -prune" subdir))
                         '(RCS CVS .git)
                         " -o ")
              " -o"
              " -type f -print0"
              " | xargs -0 -e grep -nH -e "))

"texinfo"
(define-keys texinfo-mode-map
  "\M-?" 'where-am-i/texinfo
  "\C-c\C-c\C-i" 'more-texinfo-insert-index-entry
  "\C-c\C-i" 'more-texinfo-insert-cindex-entry
  [(control x) (n) (N)]
  (lambda ()
    (interactive)
    (let ((page-delimiter "^@node\\s-+"))
      (narrow-to-page))))

"info-look"
(more-info-look-add-common)

"cvs-to-git"
(add-hook 'cvs-to-git-finish-fixup-hook 'cvs-to-git-ttn-fixups)

"log-view"
(define-keys log-view-mode-map
  "w" 'more-vc-log-view-copy-id-as-kill)

"add-log"
(progn
  (define-keys change-log-mode-map
    "\C-c\C-l" 'more-vc-likewise
    "\M-i"     'indent-according-to-mode
    "\C-i"     'indent-according-to-mode
    "\C-c\C-n" 'YYYY-MM-DD-today
    "\C-c\C-a" 'add-titled-change-log-entry)
  (add-to-list 'change-log-font-lock-keywords
               '("Release: \\(.*\\)" 1 'change-log-date)))

"git-show-branch"
(define-keys git-show-branch-map
  "\C-c\C-s" 'more-vc-git-show
  "\C-c\C-r" 'more-vc-git-rebase)

"org"
(define-keys org-mode-map
  "\M-=" 'org-open-at-point
  "\M-[" 'org-previous-link
  "\M-]" 'org-next-link)

"info"
(progn
  (define-keys Info-mode-map
    "j" (defun ttn-Info-jump-to-topic ()
          "Do ‘Info-index’ on the topic at point.
If the region is active, use its text as the topic.
Otherwise, use the symbol at point as the topic."
          (interactive)
          (Info-index
           (if (region-active-p)
               ;; TODO: strip leading/trailing whitespace
               ;; TODO: normalize internal whitespace
               (buffer-substring-no-properties
                (region-beginning) (region-end))
             (thing-at-point 'symbol))))
    "`" 'recenter-top-bottom
    "v" 'variable-pitch-mode)
  (when (facep 'Info-quoted)
    (set-face-attribute 'Info-quoted nil
                        :family "Courier 10 Pitch"
                        :height 1.1)))

"subr"
(define-keys minibuffer-local-filename-completion-map
  ;; These next two entries make C-M-SPC insert a space,
  ;; while SPC does ‘minibuffer-complete’ (like the old days).
  (kbd "C-M-SPC") (lambda () (interactive) (insert " "))
  " " 'minibuffer-complete)

"log-edit"
(define-keys log-edit-mode-map
  "\C-c\C-d" 'c2c-log-do-it)

;; kob, 81% volume, overhead fan clicking away...
"gnugo"
(progn
  (setq gnugo-option-history (list "--komi 5.5 --boardsize 13")
        gnugo-imgen-style 'ttn
        gnugo-imgen-sizing-function 'gnugo-imgen-fit-window-height/no-grid-bottom
        gnugo-imgen-char-height-fudge-factor 4.2
        gnugo-xpms 'gnugo-imgen-create-xpms)
  (when (display-images-p)
    (add-hook 'gnugo-start-game-hook
              'gnugo-image-display-mode)))

;; Add load-time hooks here.
nil


;;;---------------------------------------------------------------------------
;;; Normal hooks.

;; To paraphrase some moby CS dude: You KNOW when you are a programmer.

(let (hook) (while (setq hook (read))
              (add-hook hook 'my-prog-env)))
c-mode-common-hook
sh-mode-hook
emacs-lisp-mode-hook
lisp-mode-hook
asm-mode-hook
verilog-mode-hook
perl-mode-hook
sgml-mode-hook
scheme-mode-hook
;; 1998.0730.15:55:35-0700.  useful for wrapping, etc.
makefile-mode-hook
;; 1998.1020.02:39:00-0700.  KCSM 91FM.  some nice Count Basie, perky.  i
;; suppose that's fitting for this entry, my first bow to the inevitable.
java-mode-hook
;; Add programming major modes here.
nil

;; However, when you don't know, you can at least learn...

(let (hook) (while (setq hook (read))
              (add-hook hook (read) t)))

text-mode-hook
turn-on-auto-fill

;; Keep things visually consistent in Dired.  Try for "instant response".

dired-mode-hook
scroll-LR-by-20-minor-mode

asm-mode-hook
(lambda ()
  (setq comment-column 32))

compilation-mode-hook
scroll-LR-by-20-minor-mode

Man-mode-hook
scroll-LR-by-20-minor-mode

electric-buffer-menu-mode-hook
(lambda ()
  (scroll-LR-by-20-minor-mode 1)
  ;; Replace parentdirs w/ environment variables, plus others.
  (buffer-substitute-file-env-vars)
  ;; zonk the header line and adjust the window
  (setq header-line-format nil)
  (unless (one-window-p)
    (enlarge-window -1)))

verilog-mode-hook
(lambda ()
  (hs-minor-mode -1)
  (auto-save-mode -1)
  (abbrev-mode -1)
  (set (make-local-variable 'compile-command) "make ")
  (set (make-local-variable 'compilation-error-regexp-alist)
       '(("^Error!.*\n\\([^\n]+\n\\)*\\s-+\"\\(.*\\)\", \\([0-9]+\\):" 2 3)
         ("^Warning!.*\n\\([^\n]+\n\\)*\\s-+\"\\(.*\\)\", \\([0-9]+\\):"
          2 3))))

mail-mode-hook
(lambda ()
  ;; easier to quote
  (set-fill-column 72))

view-mode-hook
scroll-LR-by-20-minor-mode

help-mode-hook
(lambda ()
  ;; recent emacs does this automatically
  (when (eq this-command 'describe-bindings)
    (let (buffer-read-only)
      (goto-char (point-min))
      (flush-lines "\t\tundefined$"))))

;; 1997-10-14 04:11. make "man -k" pretty and useful.  it surprises me
;; that this wasn't thought of before.  the best way to rephrase the
;; following is:
;;
;;   i like references to be useful.  the original code does not
;;   recognize the reference nature of the output.  thus, the output needs
;;   to be massaged.
;;
;; some hints to ông già: (a) what do we look for?  (b) is there a better
;; way?

Man-mode-hook
(lambda ()
  (when (string-match "[Mm]an -k" (buffer-name))
    (let (buffer-read-only ents ent txt sec pivot)
      (goto-char (point-min))
      (insert Man-see-also-regexp "\n\n")
      (while (re-search-forward
              "^\\(.*\\)\\(([^ ]+)\\)\\(\\s-+-\\s-+.*\\)$"
              (point-max) t)
        (setq ents (match-string 1)
              pivot (list (match-beginning 2) (match-string 2))
              txt (list (match-end 3) (match-string 3)))
        (goto-char (match-beginning 1))
        (cl-do ((i (cl-count ?, ents) (1- i)))
            ((zerop i) nil)
          (re-search-forward "\\(,\\|\n\\)" (point-max) t)
          (delete-char -1)
          (delete-horizontal-space)
          (insert (cadr pivot) "\t" (cadr txt) "\n"))
        (re-search-forward "\\(\\s-+\\)(")
        (delete-region (match-beginning 1) (match-end 1))
        (end-of-line)
        (insert "\n"))
      (Man-build-references-alist))))

;; 2007-02-20 16:10:21.  piccoli movimenti da tmv.  si è addormentata
;; tornando dal'ipercoop e adesso esploriamo il nuovo sistema di gnuvola.
;;
;; ;; 1998.0825.12:53:42-0700.  the whirring of the espresso machine gently
;; ;; drowns out the baroque guitar (there is also some piano).  baroque
;; ;; harmonies rule!  typical comment, i know, but all those geeks can't be
;; ;; wrong!
;;
;; mail-send-hook
;; (lambda ()
;;   (and (boundp 'ttn-jammed-from)
;;        (save-excursion
;;          (goto-char (point-min))
;;          (let ((reply-to (concat "Reply-to: " ttn-jammed-from)))
;;            (unless (search-forward reply-to nil t)
;;              (goto-char (point-min))
;;              (insert reply-to "\n"))))))

;; 1999/03/05 07:42:52.  silence.

view-mode-hook
(lambda ()
  (when (eq major-mode 'outline-mode)
    (define-keys view-mode-map
      "{" 'outline-previous-visible-heading
      "}" 'outline-next-visible-heading
      "+" (lambda ()
            (interactive)
            ;; impl based on ‘outline-toggle-children’ (which almost DTRT)
            (save-excursion
              (outline-back-to-heading)
              (if (outline-invisible-p (line-end-position))
                  (outline-show-subtree)
                (outline-hide-subtree)))))))

vc-before-checkin-hook
(lambda ()
  (save-excursion
    (goto-char (point-min))
    (hs-show-block))
  ;; 1999/05/15 15:51:10.  well this should have been done a long time ago.
  ;; 2007-09-16 12:38:38.  never too late to improve things a bit, however...
  (when (and buffer-file-name (vc-registered buffer-file-name))
    (unless (eq 'change-log-mode major-mode)
      (let ((diff-switches
             (cons
              (cl-case major-mode
                ((emacs-lisp-mode scheme-mode lisp-mode) "-F^(")
                ((texinfo-mode) "-F^@node")
                (t "-p"))
              (if (stringp diff-switches)
                  (list diff-switches)
                diff-switches))))
        (vc-diff nil)))))

ediff-prepare-buffer-hook
turn-off-hideshow

;; 2003/09/04 15:40:45.  camion e vicini.
;; 2014-07-28 10:13:34.  ucelli, tv, motorino.
before-save-hook
time-stamp

change-log-mode-hook
inhibit-backup-locally

display-time-hook
(lambda ()
  (put-text-property 0 5 'help-echo '(italy-time t) display-time-string))

log-edit-mode-hook
abbrev-mode

find-file-hook
(lambda ()
  (when vc-mode
    (rmangle-update-blurb)
    (add-hook 'after-revert-hook 'rmangle-update-blurb nil t)))

log-view-mode-hook
more-vc-pretty-up-log

before-save-hook
delete-trailing-whitespace

;; 2010-12-31 10:52:15.  silenzio.
next-error-hook
hs-show-surrounding-block

find-file-hook
no-thanks

ido-setup-hook
(lambda ()
  (define-keys ido-buffer-completion-map
    "\M-a" 'ido-toggle-ignore
    "\C-n" 'ido-next-match
    "\C-p" 'ido-prev-match))

process-menu-mode-hook
(lambda ()
  (define-keys process-menu-mode-map
    "k" 'process-menu-delete-process))

activate-mark-hook   (lambda () (setq cursor-type 'bar))
deactivate-mark-hook (lambda () (setq cursor-type t))

vc-dir-mode-hook
(lambda () (setq-local c2c-log-files-to-prep-for-function
                       #'vc-dir-marked-files))

git-status-mode-hook
(lambda () (setq-local c2c-log-files-to-prep-for-function
                       #'git-marked-filenames))

erc-mode-hook
(lambda () (setq erc-fill-column (- (window-width) 2)))

org-mode-hook
inhibit-backup-locally

log-edit-mode-hook
more-vc-set-default-directory-to-project-root

read-only-mode-hook
more-vc-set-file-modes-based-on-buffer

;; Add normal hooks here.
nil


;;;---------------------------------------------------------------------------
;;; Advice hooks.
;;;
;;; TODO: Reimplement advice using ‘advice-add’ et al.

(defadvice dired-diff (around diff-last-release)
  (if (not (and (string= "" (file-name-nondirectory
                             (ad-get-arg 0)))
                (string= ".last-release" (file-name-nondirectory
                                          (dired-get-filename)))))
      ad-do-it
    (let ((here default-directory)
          (lr (with-temp-buffer
                (insert-file-contents ".last-release")
                (buffer-substring (point) (line-end-position)))))
      (switch-to-buffer (get-buffer-create (format "*%s*" lr)))
      (shell-command
       (cond ((file-exists-p ".git")
              (concat "git diff --stat -u -M " lr))
             ((file-exists-p "CVS")
              (concat "cvs diff -r " lr))
             ((file-exists-p "RCS")
              (mapconcat
               (lambda (comma-v)
                 (concat "rcsdiff -q -bBwu -kk -r" lr " "
                         (let ((workfile (replace-regexp-in-string
                                          "\\(.+/\\)RCS/\\([^/]+\\),v"
                                          "\\1\\2"
                                          comma-v t)))
                           (if (file-exists-p workfile)
                               workfile
                             (concat "-r " comma-v)))))
               (split-string (shell-command-to-string
                              "find . -name '*,v' -print0")
                             (string 0) t)
               " ; ")))
       1)
      (save-excursion (insert lr "\n"))
      (diff-mode)
      (setq buffer-read-only t)
      (set (make-local-variable 'list-buffers-directory) here))))

(advice-add 'message-insert-signature :after
            'more-message-protect-signature-separator)

(advice-add 'man :around
            ;; NB: This func has no ‘interactive’ spec so ‘advice-add’
            ;;     arranges to inherit the ‘interactive’ spec of ‘man’.
            ;;     Nice (especially if you take a look at that spec)!
            (defun let-bind/env-MANWIDTH (orig-man &rest man-args)
              "Apply ORIG-MAN to MAN-ARGS w/ env var ‘MANWIDTH’ set.

The value of ‘MANWIDTH’ is normally 5 less than the ‘window-width’,
but should the new window be from a horizontal split, the value is
5 less than half of ‘window-width’.  This uses the same logic as
‘split-window-sensibly’ to determine \"likelihood of horiz split\".

This let-binds ‘process-environment’ to temporarily (hence :around)
set ‘MANWIDTH’.  See also man(1)."
              (let ((process-environment
                     (cons (format "MANWIDTH=%d"
                                   (- (/ (window-width)
                                         ;; Ape ‘split-window-sensibly’, ugh.
                                         (let ((w (selected-window)))
                                           (cond ((window-splittable-p w) 1)
                                                 ((window-splittable-p w t) 2)
                                                 (t 1))))
                                      5))
                           process-environment)))
                (apply orig-man man-args))))

(advice-add
 'compilation-start :filter-args
 (defun rewrite-compilation-command-w/cd (args)
   "Replace COMMAND using `rewrite-shell-command-w/cd'."
   (cons (rewrite-shell-command-w/cd (car args)) (cdr args))))

(advice-add 'scheme-send-region :before 'run-guile-echo-region)

;; Add advice here.

;;;---------------------------------------------------------------------------
;;; hooks.el ends here
