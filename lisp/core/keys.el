;;; keys.el
;;;
;;; Copyright (C) 1996, 1997, 1998, 1999, 2000,
;;;   2002, 2003, 2004, 2006, 2007, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: Global keymap bindings.

(let (key) (while (setq key (read))
             (global-set-key key (eval (read)))))

;; First the simple...

"\M-n" 'down-holdcursor                 ; ah, this is the way it should be!
"\M-p" 'up-holdcursor                   ;

"\M-m" 'compile                         ; water the seed

"\C-xg" 'grep                           ; basic editing and movement
[home] 'delete-other-windows            ;
[end] 'delete-window                    ;
[pause] 'split-window-vertically        ;
"\M-*" 'replace-string                  ;
"\C-x\C-v" 'view-file                   ;
"\C-cv" 'view-buffer                    ;
"\C-ct" 'toggle-truncate-lines          ;

"\M-l" 'what-line                       ;

"\M-!" 'saved-shell-command             ;
"\C-xs" 'multi-shell                    ;

"\C-x[" 'backward-page-ignore-narrow    ;
"\C-x]" 'forward-page-ignore-narrow     ;

"\C-x\M-b" bookmark-map                 ;

"\C-x\C-m\C-m" 'mail-this-buffer        ;

"\C-cn" 'insert-time-stamp              ;

"\C-c-" 'insert-separator               ;

"\M-&" 'bg-shell-command                ;

[f11] 'describe-text-properties         ; welcome to 2004 old fogie!

[(shift up)] 'scroll-down               ; for X; most terminals lose here
[(shift down)] 'scroll-up

"\C-cI" 'Info-on-current-buffer         ; while browsing tgz, zip, ...

"\C-x\C-d" 'dired-especially-this-file  ; `list-directory' not so useful

"\C-cR" 'rename-buffer                  ; for diff, etc

"\C-c_" 'raise-sexp                     ; leaving peers underneath

[(shift return)] 'invisible-cursor-mode ; more fun under X

[(control c) (shift d)] 'disassemble    ; curiosity can only hurt a little...

"\C-ca" 'clone-indirect-buffer          ; for quick local searching

[Scroll_Lock] 'emacs-usage              ; olio per il kvm

[(meta ?\])] 'switch-window-contents     ; limited patience for heuristics

"\C-c " 'transient-mark-mode            ; sometimes it can be annoying

[(control x) (m)] 'message-mail         ; fancy

[(super ?\[)] 'previous-error           ; can curiosity be so wrong?
[(super ?\])] 'next-error

"\C-xv\M-r" 'release-dired              ; mark and then Rel, woo hoo!

"\M-#" 'delete-indentation              ; M-^ is too much of a stretch

"\C-x\M-f" 'watch-file-preciously       ; thanks buggy compilers everywhere

"\M-g" 'git-grep-from-top               ; poor man's memory
"\C-c\M-g" 'git-grep                    ; even poorer man's memory

[(control return)] 'newline-and-indent  ; please explore space young ones!

"\C-xvN" 'more-vc-diff-NEW              ; even a branch must begin as a bud

"\M-z" 'zap-up-to-char                  ; don't be too eager to kill

[(control up)] 'up-holdcursor           ; wandering from home, dotard?
[(control down)] 'down-holdcursor

[(super left)] 'previous-buffer         ; victim of the years, feh
[(super right)] 'next-buffer

[(super g)] 'run-guile                  ; "the roads must roll!"

[(control c) (control meta r)] 'revert-some-buffers ; Emacs VC still lame

[(control c) (=)] 'smerge-mode          ; robotic smile, despotic style

[(meta ?')] 'nice-single-quote          ; curvy in the right places

[(super w)] 'toggle-show-trailing-whitespace         ; slop...
[(super d)] 'dont-delete-trailing-whitespace-on-save ; ...accepted :-(

"\C-x\M-h" 'help-show-headers           ; close but not too close to ‘C-h h’

[(super s)] 'hs-show-surrounding-block  ; might as well

"\C-xvH" 'more-vc-toggle-git-post-commit-hook ; avoid rebasing confusion

"\C-xvb" 'git-show-branch               ; aware monkeys live (a bit) longer
[(super b)] 'git-show-branch

"\C-cd" 'buffer-directories             ; snoop automount reticence

[(super ?\s)] [? ]                      ; respect the poorly visioned

"\C-co" 'inhibit-backup-locally         ; ok

"\C-xv." 'git-status                    ; the present is but a point

"\C-xvi" 'more-vc-register-dwim         ; awareness of the instant

"\C-xvo" 'c2c-log-prep-change-logs      ; o rly!

[(shift end)] 'scram!                   ; self-deprecate when work is done

[(super ?a)] 'org-agenda                ; NB: ‘C-c a’ in manual
"\C-xaa" 'org-agenda                    ;     (info "(org) Activation")

;; Add new simple global keybindings here.

;; Then the complex.

"\C-cw"
(defun kill-new-wikipedia-url (topic &optional xsel)
  "Form a Wikipedia URL for TOPIC and ‘kill-new’ it.
In the URL, convert all SPC chars from TOPIC to underscore.
Prefix arg means also ‘x-select-text’ the string,
arranging to additionally set the primary selection.
See ‘interprogram-cut-function’ and ‘x-select-enable-primary’."
  (interactive
   (list (read-string "Topic: "
                      (when (region-active-p)
                        (deactivate-mark)
                        (funcall region-extract-function nil)))
         current-prefix-arg))
  (let ((interprogram-cut-function (when xsel
                                     'x-select-text))
        (x-select-enable-primary nil))
    (kill-new (message "https://en.wikipedia.org/wiki/%s"
                       (subst-char-in-string ?\s ?_ topic t)))))

"\M-+"
(defun delete-region-dammit (beg end &optional dammit)
  "Like ‘delete-region’ but w/ prefix arg, let-bind ‘inhibit-read-only’."
  (interactive "r\nP")
  (if dammit
      (let ((inhibit-read-only t))
        (delete-region beg end))
    (delete-region beg end)))

"\C-cr" (defun revert-buffer-t-t () (interactive) (revert-buffer t t))

"\M-\""
(lambda ()
  (interactive)
  (if (looking-at "\"")
      (forward-char 1)
    (insert ?\" ?\")
    (forward-char -1)))

"\M-o"
(defun other-dwim ()
  (interactive)
  (if (/= 1 (length
             (cl-remove-if
              (lambda (frame)
                (let ((parms (frame-parameters frame)))
                  (or (eq 'icon (cdr (assq 'visibility parms)))
                      (string= "Calendar" (cdr (assq 'name parms))))))
              (frame-list))))
      (other-frame 1)
    (switch-to-buffer
     (let ((bl (cdr (buffer-list))))
       (while (= ?\s (elt (buffer-name (car bl)) 0))
         (setq bl (cdr bl)))
       (car bl)))))

"\C-x\C-k"
(defun scram! (arg)
  "Like `\\[kill-buffer-and-window]', but w/o the hassle.
Do `kill-buffer' (or `bury-buffer' if given a prefix arg), then
`delete-window' unless there is only one non-minibuffer window left."
  (interactive "P")
  (if arg
      (bury-buffer)
    (kill-buffer nil))
  (unless (one-window-p 'nomini)
    (delete-window)))

"\C-cl"
(lambda ()
  (interactive)                         ; line-to-top-of-window
  (recenter 1))

"\C-xw"
(lambda ()
  (interactive)                         ; where-am-i
  (message (or (buffer-file-name)
               (concat default-directory " (no file)"))))

"\C-xp"
(lambda ()
  (interactive)                         ; other-previous-window
  (other-window -1))

"\C-x\C-i"
(lambda (start end prefix)              ; indent-rigidly-with-prefix
  (interactive "*r\nsPrefix to insert (don't need quotes): ")
  (apply-macro-to-region-lines start end prefix))

"\C-x3"
(lambda (arg)                           ; split-into-3-windows
  (interactive "P")
  (let* ((old (if arg (window-width) (window-height)))
         (v (/ (- old 2) 3)))
    (if arg
        (progn
          (split-window-horizontally)
          (while (> (window-width) v)
            (shrink-window-horizontally 1))
          (other-window 1)
          (split-window-horizontally)
          (other-window -1))
      (split-window-vertically)
      (while (> (window-height) v) (shrink-window 1))
      (other-window 1)
      (split-window-vertically)
      (other-window -1))))

;; 1997-10-11 19:38.  listening to some interesting banjo (i think).  i
;; really like the tightness and yet looseness -- the principal is slightly
;; on the lagging side.  :-D
;;
;; anyway, the best way to rephrase the following is:
;;
;;   i like `C-l' to move me to a better place.  the original name captures
;;   some of it, we can all see and appreciate that.  a better place means
;;   more higher comfort but with the memories not lost.  the memories are
;;   important.
;;
;;   binding for `M-r' has right idea but does not cover all the cases.
;;   so, merge the two, you can call it `home'.
;;
;; some hints to ông già: (a) how many dimensions does this expression have?
;; (b) where is the most data motion?

"\C-l"
(lambda (arg &optional arg1)            ; gnarly, dude!
  (interactive "P")
  (if arg1
      (let ((arg0 arg))
        (and (<= (point-min) arg0)
             (< arg1 (point-max))
             (<= arg0 arg1)
             (font-lock-fontify-region arg0 arg1)))

    (cond

     ((null arg)
      (recenter (or (and (boundp 'C-l-gravity)
                         C-l-gravity)
                    '(4))))

     ((integerp arg)
      (recenter arg))

     ((and (listp arg)
           (integerp (car arg)))
      (cl-case (car arg)
        (4  (recenter nil))
        (16 (setq C-l-gravity (count-lines (window-start)
                                           (line-beginning-position))))
        (64 (cond
             ((not (featurep 'font-lock)))
             ((fboundp 'font-lock-ensure) (font-lock-ensure
                                           (point-min)
                                           (point-max)))
             (t (call-interactively
                 'font-lock-fontify-buffer)))))))))

;; 1998.0506.00:01:20-0700.  see above for meaning of `C-x C-k'.

[mode-line mouse-3]
(key-binding "\C-x\C-k")

;; 1998.0707.02:41:49-0700.  this used to be just `bury-buffer', but it's
;; better to make Emacs understand Real Intent.  :->

"\M-_"
(lambda (below-electric-buffer-notice)
  (interactive "P")
  (and below-electric-buffer-notice
       (rename-buffer (concat " " (buffer-name))))
  (bury-buffer))

;; 2021-01-10 17:23:31.  chick corea.

[(super ?l)] 'locate

;; 2007-08-25 18:30:40.  tick tick tick...

[f5] (lambda () (interactive) (set-theme 'caffeine))
[f6] (lambda () (interactive) (set-theme 'new-earthy))

"\C-x\C-b"
(defun ttn-switch-buffer-or-edit (&optional edit)
  "Do ‘ido-switch-buffer’ or (if prefix arg) ‘buffer-menu’.
If the former, rotate the list so that the current buffer is at
the head (so that ‘\\[ttn-switch-buffer-or-edit] C-m’ leaves you where you started)."
  (interactive "P")
  (if edit
      (let* ((orig directory-abbrev-alist)
             (more (cl-loop
                    with home = (expand-file-name "~")
                    with dir
                    for (k v) in (mapcar (lambda (s)
                                           (split-string s "="))
                                         process-environment)
                    when (and v (file-directory-p v)
                              (setq dir (expand-file-name v))
                              (not (string= home dir)))
                    collect (cons dir (propertize (concat "$" k)
                                                  'font-lock-face
                                                  'font-lock-string-face))
                    into ls
                    finally return (sort ls (lambda (a b)
                                              (> (length (car a))
                                                 (length (car b)))))))
             (directory-abbrev-alist
              ;; Do it dynamically because something weird w/ emacsclient
              ;; and git wrecks the workflow (TODO: investigate!).
              (append more orig)))
        (buffer-menu))
    (let ((unread-command-events (list ?\C-r)))
      (call-interactively 'ido-switch-buffer))))

;; Add new complex global keybindings here.

;; Done!
nil

;;; keys.el ends here
