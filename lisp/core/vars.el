;;; vars.el
;;;
;;; Copyright (C) 1996, 1997, 1998, 2000, 2001,
;;;   2002, 2003, 2004, 2006, 2007, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: Set variables variously for various things.


;;;---------------------------------------------------------------------------
;;; Garden-variety editing.

(setenv "PAGER" "cat")
(setenv "MANOPT" "-L C")                ; for man

(setq-default
 ;; add default var jamming here.
 truncate-lines t                       ; depth through breadth
 cursor-in-non-selected-windows nil     ; enough ghosts in the machine
 major-mode 'text-mode                  ; (see text-mode-hook)
 fill-column 78)                        ; big enough

(setq ring-bell-function
      (defun flash-mode-line ()
        "Invert ‘mode-line’ face fg/bg; wait a moment; invert back.
This function is intended to be the value of ‘ring-bell-function’."
        (interactive)
        (invert-face 'mode-line)
        (sit-for 0.042)
        (invert-face 'mode-line)))

(setq

 inhibit-startup-message t              ; hehe, no longer a novice
 backup-by-copying-when-linked t        ; for symlinks
 auto-save-interval 600                 ; keystrokes NOT seconds
 version-control nil                    ; heh, why be like vms?!
 delete-auto-save-files t               ; limited disk space, yasee
 require-final-newline t                ; for other programs
 scroll-conservatively 999              ; smoother scrolling
 auto-save-timeout nil                  ; disable saving based on time
 window-min-height 2                    ; sure, why not?  (NB: 1 => problems)
 display-time-24hr-format t             ; save mode-line space
 next-line-add-newlines nil             ; do not add newlines at EOB
 vc-keep-workfiles t                    ; sure, why not?
 vc-mistrust-permissions t              ; use the SOURCE dammit!
 vc-make-backup-files nil               ; don't need em
 vc-git-diff-switches "--stat"          ; eye high in the sky
 visible-bell t                         ; shut up you infernal machine!
 inferior-lisp-program "gcl"            ; keep the faith, man!
 view-scroll-auto-exit t                ; get out!
 display-time-24hr-format t             ; less ambiguity more space
 add-log-always-start-new-record t      ; finally, sanity reigns
 mode-line-in-non-selected-windows nil  ; keep things quiet for now
 diff-switches "-u"                     ; --unified=1 confuses cvs, sigh
 font-lock-verbose nil                  ; yeah, whatever
 dired-deletion-confirmer 'identity     ; no questions asked
 dired-listing-switches "-alhFG"        ; include file type, but no group
 enable-recursive-minibuffers t         ; contemplate crass confusion
 add-log-time-zone-rule t               ; reduce confusion on earth
 confirm-kill-emacs 'y-or-n-p           ; be sure but not too sure
 ange-ftp-try-passive-mode t            ; keep the bits flowing
 next-error-recenter 2                  ; useful for grep, too
 max-mini-window-height 0.8             ; in case we near the minotaur
 jit-lock-context-time 1.5              ; a little less eager, thanks
 default-input-method "italian-postfix" ; non mi arrendo
 ispell-program-name "ispell"           ; umm, it's spelled like that
 read-quoted-char-radix 16              ; it's hip to be square
 sql-product 'postgres                  ; keeping it real
 initial-scratch-message nil            ; simple simple simple
 smerge-command-prefix "\C-cc"          ; easier to type than ‘C-c ^’
 ido-save-directory-list-file nil       ; no thanks, maybe later
 ido-max-prospects 42                   ; patience is a virtue
 comint-scroll-show-maximum-output t    ; niente saltarino
 split-window-keep-point nil            ; "convenient on slow terminals"
 help-enable-auto-load nil              ; low-memory kindness
 switch-to-buffer-preserve-window-point nil ; less magic, please
 x-select-enable-primary t              ; easier interop w/ CUA crowd?

 ;; add simple var jamming here.

 vc-handled-backends                    ; orig from 2014-02-12 10:32:20
 '(RCS
   CVS
   SVN
   ;; SCCS
   Bzr
   Git
   ;; Hg
   ;; Mtn
   ;; Arch
   )

 org-file-apps '((t . emacs))           ; no external apps, please (for now)
 org-confirm-shell-link-function 'y-or-n-p
 org-confirm-elisp-link-function 'y-or-n-p
 org-cycle-global-at-bob t
 org-cycle-include-plain-lists t
 org-hide-leading-stars t
 org-odd-levels-only t
 org-src-fontify-natively t

 ;; 2004-11-09 12:36:36.  camion e vicini.
 ;; magari questa cosa non ci serve più.
 perl-indent-level                2 ; stile GNU
 perl-continued-statement-offset  2
 perl-continued-brace-offset      0
 perl-brace-offset                0
 perl-brace-imaginary-offset      0
 perl-label-offset               -2

 Man-width (1- (frame-width))
 time-stamp-start "[tT]ime-stamp:[ \t]+\\\\?[\"<]+")


;;;---------------------------------------------------------------------------
;;; Buffer management.

(add-to-list 'same-window-buffer-names "*compilation*")
(add-to-list 'same-window-buffer-names "*Locate*")


;;;---------------------------------------------------------------------------
;;; Mode line customization.

(defvar this-emacs-id (concat
                       user-login-name ":"
                       (let ((name (if (fboundp 'system-name)
                                       (system-name)
                                     ;; var obsolete since 25.1
                                     system-name)))
                         (if (string-match "\\(^[^.]+\\)" name)
                             (match-string 1 name)
                           name))
                       "/"
                       (or (getenv "WINDOW")
                           (format "p%d" (emacs-pid))))
  "String identifying \"this\" Emacs, intended for the mode line.
It includes the `user-login-name', (parts of) the `system-name',
the value of env var `WINDOW' (if set, by screen(1), for example),
and the process id (but only if `WINDOW' is not set).")

(setq-default
 mode-line-mule-info             nil
 mode-line-frame-identification  nil
 mode-line-buffer-identification '("%5b")
 mode-line-modified              '("-%1*%1+  " (-3 "%P") "  ")
 mode-line-position              nil
 mode-line-format                (delete '(-3 . "%p") mode-line-format))

;; old skool: put the time/mail info to the left of "(MODE ...)".
(let ((cut (cl-position 'global-mode-string mode-line-format
                        :key 'car-safe))
      (jam (cl-position 'mode-line-modes mode-line-format)))
  (when (and cut jam (< jam cut))
    (setq-default
     mode-line-format (append (subseq mode-line-format 0 jam)
                              (list 'global-mode-string "  ")
                              (subseq (delete (nth cut mode-line-format)
                                              mode-line-format)
                                      jam)))))

(let* ((t-cell (last mode-line-format))
       (trailing (car t-cell)))
  (unless (eq 'this-emacs-id trailing)
    (setcar t-cell 'this-emacs-id)
    (setcdr t-cell (list trailing))))


;;;---------------------------------------------------------------------------
;;; Properties.

(mapc (lambda (cmd)
        (put cmd 'disabled nil))
      '(eval-expression
        upcase-region
        downcase-region
        narrow-to-page
        narrow-to-region
        erase-buffer
        image-toggle-display
        dired-find-alternate-file
        ;; add liberated commands here.
        ))


;;;---------------------------------------------------------------------------
;;; Munge `auto-mode-alist'.

(mapc (lambda (x)
        (cl-pushnew (cons (format "\\.%s\\'" (car x)) (cdr x))
                    auto-mode-alist :test 'equal))
      '((pl             . perl-mode)
        (ph             . perl-mode)
        (perl           . perl-mode)
        (thud           . scheme-mode)
        (th             . scheme-mode)
        (v              . verilog-mode)
        (twerp          . texinfo-mode)
        (xhtml-data     . scheme-mode)
        (html-data      . scheme-mode)
        (mms            . makefile-mode)
        (af             . scheme-mode)
        (texh           . texinfo-mode)
        (kt             . java-mode)
        ;; Add auto-mode hints here.
        ))


;;;---------------------------------------------------------------------------
;;; Hack to use own copy of hideshow.

(unless (featurep 'hideshow)
  (makunbound 'hs-special-modes-alist))


;;;---------------------------------------------------------------------------
;;; Indentation specifications.
;;; These are for properties other than `lisp-indent-function'.

(dolist (sym '(cron
               and-let*
               ;; from Guile's ice-9/testing-lib.scm
               pass-if
               pass-if-exception
               expect-fail
               expect-fail-exception
               with-test-prefix
               ;; from QA
               qa-do-test
               qa-do-test-expect-fail))
  (put sym 'scheme-indent-function 1))


;;;---------------------------------------------------------------------------
;;; Local variables policy

(setq safe-local-variable-values
      '((outline-regexp . "\\([ ][ ]\\)*- ")
        (outline-regexp . "\\([ ][ ]\\)*[-+] ")
        (backup-inhibited . t)
        (fill-prefix . "\t")
        (user-mail-address . "ttn@gnu.org")
        (git-committer-email . "ttn@gnu.org")
        ;; Add more pairs here.
        ))

(put 'add-log-mailing-address 'safe-local-variable 'stringp)


;;;---------------------------------------------------------------------------
;;; Completion

(mapc (lambda (ext)
        (add-to-list 'completion-ignored-extensions ext))
      (list
       ".installable"
       ".svelte"
       ;; Add other extensions here.
       ))


;;;---------------------------------------------------------------------------
;;; Same window

(dolist (name '("[*]\\(vc\\|git\\)-diff[*]"
                ;; Add more buffer names here.
                ))
  (add-to-list 'display-buffer-alist (list name 'display-buffer-same-window)))

;;;---------------------------------------------------------------------------
;;; vars.el ends here
