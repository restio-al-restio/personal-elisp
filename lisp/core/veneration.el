;;; veneration.el
;;;
;;; Copyright (C) 2000, 2002, 2003, 2004, 2006, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Compatibility for older versions of Emacs.

(unless (fboundp 'display-color-p)
  (defun display-color-p () window-system))

(unless (fboundp 'add-to-list)
  ;; unmodified from the repo 2004-10-13
  (defun add-to-list (list-var element &optional append)
    "Add to the value of LIST-VAR the element ELEMENT if it isn't there yet.
The test for presence of ELEMENT is done with `equal'.
If ELEMENT is added, it is added at the beginning of the list,
unless the optional argument APPEND is non-nil, in which case
ELEMENT is added at the end.

The return value is the new value of LIST-VAR.

If you want to use `add-to-list' on a variable that is not defined
until a certain package is loaded, you should put the call to `add-to-list'
into a hook function that will be run only after loading the package.
`eval-after-load' provides one way to do this.  In some cases
other hooks, such as major mode hooks, can do the job."
    (if (member element (symbol-value list-var))
        (symbol-value list-var)
      (set list-var
           (if append
               (append (symbol-value list-var) (list element))
             (cons element (symbol-value list-var)))))))

(unless (fboundp 'number-sequence)
  ;; unmodified from the repo 2004-12-09
  (defun number-sequence (from &optional to inc)
    "Return a sequence of numbers from FROM to TO (both inclusive) as a list.
INC is the increment used between numbers in the sequence and defaults to 1.
So, the Nth element of the list is \(+ FROM \(* N INC)) where N counts from
zero.  TO is only included if there is an N for which TO = FROM + N * INC.
If TO is nil or numerically equal to FROM, return \(FROM).
If INC is positive and TO is less than FROM, or INC is negative
and TO is larger than FROM, return nil.
If INC is zero and TO is neither nil nor numerically equal to
FROM, signal an error.

This function is primarily designed for integer arguments.
Nevertheless, FROM, TO and INC can be integer or float.  However,
floating point arithmetic is inexact.  For instance, depending on
the machine, it may quite well happen that
\(number-sequence 0.4 0.6 0.2) returns the one element list \(0.4),
whereas \(number-sequence 0.4 0.8 0.2) returns a list with three
elements.  Thus, if some of the arguments are floats and one wants
to make sure that TO is included, one may have to explicitly write
TO as \(+ FROM \(* N INC)) or use a variable whose value was
computed with this exact expression.  Alternatively, you can,
of course, also replace TO with a slightly larger value
\(or a slightly more negative value if INC is negative)."
    (if (or (not to) (= from to))
        (list from)
      (or inc (setq inc 1))
      (when (zerop inc) (error "The increment can not be zero"))
      (let (seq (n 0) (next from))
        (if (> inc 0)
            (while (<= next to)
              (setq seq (cons next seq)
                    n (1+ n)
                    next (+ from (* n inc))))
          (while (>= next to)
            (setq seq (cons next seq)
                  n (1+ n)
                  next (+ from (* n inc)))))
        (nreverse seq)))))

(unless (fboundp 'delete-dups)
  ;; unmodified from the repo 2004-12-09
  (defun delete-dups (list)
    "Destructively remove `equal' duplicates from LIST.
Store the result in LIST and return it.  LIST must be a proper list.
Of several `equal' occurrences of an element in LIST, the first
one is kept."
    (let ((tail list))
      (while tail
        (setcdr tail (delete (car tail) (cdr tail)))
        (setq tail (cdr tail))))
    list))

(unless (fboundp 'invisible-p)
  (cond ((fboundp 'line-move-invisible-p)
         (fset 'invisible-p 'line-move-invisible-p))
        ((fboundp 'line-move-invisible)
         (fset 'invisible-p 'line-move-invisible))
        (t
         (defun invisible-p (pos)
           "Return non-nil if the character after POS is currently invisible."
           (let ((prop
                  (get-char-property pos 'invisible)))
             (if (eq buffer-invisibility-spec t)
                 prop
               (or (memq prop buffer-invisibility-spec)
                   (assq prop buffer-invisibility-spec))))))))

(unless (fboundp 'set-frame-font)
  (fset 'set-frame-font 'set-default-font))

(unless (fboundp 'called-interactively-p)
  (fset 'called-interactively-p (lambda (kind)
                                  (unless (eq 'interactive kind)
                                    (error "KIND not `interactive'"))
                                  (interactive-p))))

(unless (fboundp 'defvar-local)
  ;; Introduced in Emacs 24.3.
  (defmacro defvar-local (var val &optional docstring)
    "Define VAR as a buffer-local variable with default value VAL.
Like `defvar' but additionally marks the variable as being automatically
buffer-local wherever it is set."
    (declare (debug defvar) (doc-string 3))
    ;; Can't use backquote here, it's too early in the bootstrap.
    (list 'progn (list 'defvar var val docstring)
          (list 'make-variable-buffer-local (list 'quote var)))))

;;; veneration.el ends here
