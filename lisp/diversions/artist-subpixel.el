;;; artist-subpixel.el                              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Subpixel-centered circles to extend artist.el.

;;; Commentary:

;; Although artist.el is wonderful, it doesn't (yet) do subpixel-centered
;; circles (or ellipses).  This means that circles rendered by artist.el
;; always have an "outer diameter" modulo 2 of 1, because the origin is *on* a
;; pixel (i.e., coordinate, i.e., intersection of row and column).  That's a
;; bummer when you're trying to generate circles with outer diameter modulo 2
;; of 0, suitable for XPMs usable by gnugo.el, for example.
;;
;; This file adds some funcs to locally rectify (i.e., kludge around) the
;; current situation, with the hope that eventually a generalization can be
;; worked back into artist.el, perhaps as a subpixel-center minor mode of
;; some sort.

;;; Code:

(require 'artist)

(defun circle-quadrant (int-radius)     ; => ((X . Y) ...)
  (unless (integerp int-radius)
    (error "Radius `%s' not an integer" int-radius))
  (mapcar (lambda (v)
            (cons (- (aref v 0) 0.5) (- (aref v 1) 0.5)))
          ;;
          ;; hmm, the ellipse function does not produce beautiful arcs:
          ;;
          ;;                     XXXXXX
          ;;                   XX......XX
          ;;                  X..........X
          ;;                XX............XX
          ;;                X..............X
          ;;               X................X
          ;;              X..................X
          ;;              X..................X
          ;;             X....................X
          ;;
          ;; note the ugly upside-down-L clumps; would be nicer if it were:
          ;;
          ;;                     XXXXXX
          ;;                   XX......XX
          ;;                 XX..........XX
          ;;                X..............X
          ;;               X................X
          ;;               X................X
          ;;              X..................X
          ;;              X..................X
          ;;             X....................X
          ;;
          (artist-ellipse-generate-quadrant int-radius int-radius)))

;;;###autoload
(defun circle-placed (radius ox oy)     ; => ((X . Y) ...)
  (when (or (integerp ox) (integerp oy))
    (error "Origin `(%s %s)' has integer component" ox oy))
  (when (cl-plusp radius)
    (let* ((ls (circle-quadrant radius))
           (ls-4 (mapcar (lambda (factors)
                           (let ((xf (car factors))
                                 (yf (cdr factors)))
                             (mapcar (lambda (c)
                                       (cons (truncate (+ ox (* xf (car c))))
                                             (truncate (+ oy (* yf (cdr c))))))
                                     ls)))
                         `((1 . 1)
                           (1 . -1)
                           (-1 . 1)
                           (-1 . -1)))))
      (delete-dups (apply 'append ls-4)))))

(provide 'artist-subpixel)

;;; artist-subpixel.el ends here
