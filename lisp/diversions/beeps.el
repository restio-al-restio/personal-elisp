;;; beeps.el --- Drive the pc speaker.              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; This library provides `beeps-mode'.
;;
;; To use it, /usr/bin/beep to must exist and be executable by you.

(defvar beeps-duration 40
  "Duration of a beep, in milliseconds.")

(defconst beeps-frequencies
  (let ((ht (make-hash-table :test 'equal))
        (ls '("C"    130.8
              "C#"   138.6
              "D"    146.85
              "D#"   155.55
              "E"    164.8
              "F"    174.6
              "F#"   185.0
              "G"    196.0
              "G#"   207.65
              "A"    220.0
              "A#"   233.1
              "B"    246.95)))
    (while ls
      (let ((name (car ls))
            (freq (cadr ls)))
        (dolist (mult '(1 2 3 4 5))
          (puthash (cons mult name)
                   (format "%s" (* (expt 2 (1- mult)) freq))
                   ht)))
      (setq ls (cddr ls)))
    ht)
  "Hash of frequencies, in Hertz, from 1C through 5B.
Keys are (MULT . NAME), where MULT ranges from 1 through 5 and
NAME is a string in the set: C, C#, D, D#, E, F, G, G#, A, A#, B.
Values are strings, such as \"130.8\" for key (1 . \"C\").")

(defvar beeps-standard-keyboard-keys
  '(s e d r f g y h u j i k l)
  "List of unmodified keys to map to a single register.
Each element is a single-character lower-case symbol.")

(defvar beeps-indirection)

(defun beeps-do-beep ()
  "Call beep(1), specifying duration and frequency.
Duration is ‘beeps-duration’ while frequency depends on which
key (according to ‘this-command-keys-vector’) called this command.
See ‘beeps-standard-keyboard-keys’ and ‘beeps-mode’."
  (interactive)
  (let* ((key (aref (this-command-keys-vector) 0))
         (mod (event-modifiers key))
         (basic (event-basic-type key))
         (k (if mod
                (append mod (list basic))
              (list basic)))
         (freq (gethash k beeps-indirection nil)))
    (when freq
      (message "key:%S  mod:%S  basic:%S  k:%S  freq:%S" key mod basic k freq)
      (call-process "/usr/bin/beep" nil nil nil
                    "-l" (format "%d" beeps-duration)
                    "-f" (gethash freq beeps-frequencies)))))

(defun beeps-increase-duration ()
  "Increase the value of variable `beeps-duration' by 10 milliseconds."
  (interactive)
  (cl-incf beeps-duration 10)
  (message "Beeps duration: %d" beeps-duration))

(defun beeps-decrease-duration ()
  "Decrease the value of variable `beeps-duration' by 10 milliseconds."
  (interactive)
  (cl-decf beeps-duration 10)
  (message "Beeps duration: %d" beeps-duration))

(defun beeps-mode ()
  "Major mode for driving the pc speaker.
In this mode, keys do not self-insert.  Instead, the unmodified keys listed
in variable `beeps-standard-keyboard-keys' play the \"second register\"
pitches C, C#, D, D#, E, F, F#, G, G#, A, A#, B, C (last C in next register),
and key modifiers select other registers: `control' 1, `shift' 3, `meta' 4.

Additionally, `prior' and `next' run commands `beeps-increase-duration'
and `beeps-decrease-duration', respectively."
  (interactive)
  (kill-all-local-variables)
  (setq major-mode 'beeps-mode
        mode-name "Beeps")
  (set (make-local-variable 'beeps-indirection)
       (make-hash-table :test 'equal))
  (let ((m (make-sparse-keymap))
        (two '("C" "C#" "D" "D#" "E" "F" "F#" "G" "G#" "A" "A#" "B")))
    (suppress-keymap m)
    (cl-flet
        ((full-add (kb note scale mod)
                   (let* ((n (aref (format "%s" kb) 0))
                          (k (if mod
                                 (list mod n)
                               (list n))))
                     (define-key m (vector k) 'beeps-do-beep)
                     (puthash k
                              (cons scale note)
                              beeps-indirection))))
      (cl-mapcar (lambda (kb note)
                   (cl-flet
                       ((add (scale mod) (full-add kb note scale mod)))
                     (add 1 'control)
                     (add 2 nil)
                     (add 3 'shift)
                     (add 4 'meta)))
                 beeps-standard-keyboard-keys
                 two)
      (let ((kb (car (last beeps-standard-keyboard-keys))))
        (full-add kb "C" 2 'control)
        (full-add kb "C" 3 nil)
        (full-add kb "C" 4 'shift)
        (full-add kb "C" 5 'meta)))
    (define-key m [(prior)] 'beeps-increase-duration)
    (define-key m [(next)]  'beeps-decrease-duration)
    (use-local-map m)))

(provide 'beeps)

;;; beeps.el ends here
