;;; browse-pointer-shapes.el                        -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1998, 1999, 2000, 2001, 2002,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Browse pointer shapes.

;;;###autoload
(defun browse-pointer-shapes ()
  "Browse all pointer shapes.  Choose by saying `y'.
Return shape chosen as an integer, or nil if none chosen."
  (interactive)
  (catch 'shape-choice
    (all-completions
     "x-pointer-" obarray
     (lambda (sym)
       (and (boundp sym)
            (let ((val (symbol-value sym)))
              (and (integerp val)
                   (progn
                     (setq x-pointer-shape val)
                     (set-mouse-color nil)
                     (when (y-or-n-p (format "(%s) Done? " val))
                       (throw 'shape-choice val))))))))))

(provide 'browse-pointer-shapes)

;;; browse-pointer-shapes.el ends here
