;;; emacs-usage.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Display buffer and file counts in echo area.

;;;###autoload
(defun emacs-usage ()
  "Display buffer and file counts in echo area."
  (interactive)
  (let ((ls (buffer-list)))
    (message "%s buffers, %s files"
             (length ls)
             (length (delq nil (mapcar (lambda (buf)
                                         (buffer-local-value
                                          'buffer-file-truename buf))
                                       ls))))))

(provide 'emacs-usage)

;;; emacs-usage.el ends here
