;;; fcookie.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

(require 'bindat)

(defconst fcookie-index-bindat-spec
  '((:version  u32)
    (:count    u32)
    (:longest  u32)
    (:shortest u32)
    (:flags    u32)
    (:delim    u8)
    (:ignored  fill 3)
    (:offset   repeat (:count)
               ;; bindat.el misparses in the
               ;; absence of the field name `:foo'
               (:foo u32))
    (:filesize u32))
  "Description of a fortune cookie index file.
This was puzzled out by ttn and is by no means \"authoritative\",
although it seems to do the job.  Here is a short explanation:
  :version     -- typically 1 for old files and 2 for newer ones
  :count       -- number of cookies
  :longest     -- number of bytes of longest cookie
  :shortest    -- number of bytes of shortest cookie
  :flags       -- logior of 0x1 (random)
                            0x2 (ordered)
                            0x4 (rotated)
  :delim       -- traditionally '%'
  :offset      -- array of :count file-position offsets
  :filesize    -- size (in bytes) of the fortune cookie file
See info node ‘(elisp) Bindat Spec’ for specific details.")

(defmacro fcookie--with-temp-unibyte-buffer-on-file (filename &rest body)
  (declare (indent 1))
  `(with-temp-buffer
     (set-buffer-multibyte nil)
     (insert-file-contents-literally ,filename)
     ,@body))

(defun fcookie-create-index (cookies &optional index delim)
  "Scan COOKIES, a fortune cookie file, and write out its index file.
Optional second arg INDEX specifies the index filename, which is by
default constructed by appending \".dat\" to COOKIES.  Optional third
arg DELIM specifies the unibyte character which, when found on a line
of its own in COOKIES, indicates the border between entries."
  (interactive "fCookies file: ")
  (setq delim (or delim ?%))
  (let ((delim-line (string delim ?\n))
        (max 0)
        min offsets filesize)
    (unless (= 2 (string-bytes delim-line))
      (error "Delimiter cannot be represented in one byte"))
    (fcookie--with-temp-unibyte-buffer-on-file
        cookies
      (setq
       filesize (1- (point-max))
       offsets (cl-loop
                for (beg end . _)
                on (cl-loop
                    with holes = (list (point-min))
                    while (search-forward delim-line nil t)
                    do (let ((p (point)))
                         (push (- p 2) holes)
                         (push p holes))
                    finally return
                    (nreverse (cons filesize (mapcar #'1- holes))))
                by #'cddr
                unless (= beg end)
                collect (let ((len (- end beg)))
                          (setq max (max max len)
                                min (min (or min max) len))
                          beg))))
    (let ((coding-system-for-write 'binary))
      (write-region
       (bindat-pack
        fcookie-index-bindat-spec
        `((:version  . 2)
          (:count    . ,(length offsets))
          (:longest  . ,max)
          (:shortest . ,min)
          (:flags    . 0)
          (:delim    . ,delim)
          (:offset   . ,(mapcar (lambda (o)
                                  (list (cons :foo o)))
                                offsets))
          (:filesize . ,filesize)))
       nil
       (or index (concat cookies ".dat"))))))

;;;###autoload
(defun fcookie (cookies &optional index)
  "Display a random fortune cookie from COOKIES, a fortune cookie file.
Optional second arg INDEX specifies the associated index filename, which
is by default constructed by appending \".dat\" to COOKIES.  Display in
buffer \"*Fortune Cookie: BASENAME*\" where BASENAME is COOKIES without
the directory part."
  (interactive "fCookies file: ")
  (let* ((info (fcookie--with-temp-unibyte-buffer-on-file
                   (or index (concat cookies ".dat"))
                 (bindat-unpack fcookie-index-bindat-spec (buffer-string))))
         (count (bindat-get-field info :count))
         (flags (bindat-get-field info :flags))
         (flagp (lambda (flag)
                  (cl-plusp (logand flags (cl-ecase flag
                                            ((random)  #x1)
                                            ((ordered) #x2)
                                            ((rotated) #x4))))))
         (degenerate (zerop count))
         (sel (if degenerate
                  count
                (random count)))
         (beg (or (cdar (bindat-get-field info :offset sel))
                  0))
         (need-scan (or (funcall flagp 'ordered)
                        degenerate
                        (= count (1+ sel))))
         (len (if need-scan
                  (min (bindat-get-field info :longest)
                       (- (bindat-get-field info :filesize)
                          beg))
                (- (cdar (bindat-get-field info :offset (1+ sel)))
                   beg
                   ;; sizeof delim-line
                   2))))
    (switch-to-buffer (get-buffer-create
                       (format "*Fortune Cookie: %s*"
                               (file-name-nondirectory cookies))))
    (erase-buffer)
    (insert-file-contents-literally cookies nil beg (+ beg len))
    (when (and need-scan
               (search-forward (string (bindat-get-field info :delim)
                                       ?\n)
                               nil t))
      (delete-region (match-beginning 0) (point-max))
      (goto-char (point-min)))
    (when (funcall flagp 'rotated)
      (rot13-region (point-min) (point-max)))))

(provide 'fcookie)

;;; fcookie.el ends here
