;;; fortunate-loop.el                               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2000, 2001, 2004, 2006,
;;;   2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Fortune cookie factory!  :->

(defvar enable-fortunate-loop t
  "If nil, fortunate-loop just returns.")

;;;###autoload
(defun fortunate-loop (&optional nice-time)
  "Spews fortunes until user input."
  (interactive "p")
  (when enable-fortunate-loop
    (when (or (null nice-time) (= 1 nice-time))
      (setq nice-time 3600))
    (cl-flet
        ((timestamp ()
                    (let ((time (current-time)))
                      (logior (lsh (car time) 16) (cadr time)))))
      (save-window-excursion
        (let ((start-time (timestamp))
              (hey (concat "idle at " (current-time-string) " -- waiting "))
              (cmd "fortune"))
          (switch-to-buffer "*Fortunate Loop*")
          (goto-char (point-max))
          (while
              (let* ((cookie (shell-command-to-string cmd))
                     (len (length cookie)))
                (insert cookie "\n\n")
                (when (> (point-max) 2000)
                  (delete-region 1 200))
                (goto-char (point-max)) (recenter -1)
                (prog1
                    (let ((wait (+ 4 (/ (* 3.5 len) 80))))
                      (message (concat hey (number-to-string wait) "s"))
                      (sit-for wait))
                  (when (> (- (timestamp) start-time) nice-time)
                    (setq cmd "fortune"))))))))))

(unless (cl-find 'fortunate-loop timer-idle-list
                 :key (lambda (x) (aref x 5)))
  (run-with-idle-timer (* 30 60) t 'fortunate-loop))

(provide 'fortunate-loop)

;;; fortunate-loop.el ends here
