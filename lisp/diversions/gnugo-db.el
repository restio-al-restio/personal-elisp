;;; gnugo-db.el                                     -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Edit pattern databases for GNU Go.
;;; Version: 1.0

;;; Commentary:

;; This is a quick hack for cruising around GNU Go database files.
;; It provides GNUGO Pattern Database mode, which is just a handful
;; of motion commands (that understand narrowing), plus font-locking,
;; plus file overview, plus head-scratching.
;;
;; We hope that by making the database files easier to edit (for Emacs
;; users), more people (who use Emacs) will write database entries.
;;
;; TODO: Commands to
;;        - scan all database files for "dups";
;;        - factor dups into new entries: base + specialization;
;;        - rebuild (in elisp) and run the resulting DFAs;
;;        - dynamically add autohelper funcs as keywords;
;;        - gather stats on autohelper func usage and refactor.
;;       Pretty (oops excuse me, "modern, hi-tech" :-) image display.

;;; Code:

(defvar gnugo-db-font-lock-keywords
  '(("^#.*$" 0 font-lock-comment-face)
    "goal_elements"
    "callback_data"
    ("^[:;>].*$" 0 font-lock-type-face)
    ("^\\(Pattern\\)\\s-+\\(.+\\)$"
     (1 font-lock-keyword-face)
     (2 font-lock-function-name-face))
    ("^\\(|*[ ]*[()<>!$@.*?a-zA-Z]+|*\\)\\(.*\\)$"
     (1 font-lock-string-face)
     (2 font-lock-comment-face))
    ("^\\([+]*-+[+]*\\)\\(.*\\)$"
     (1 font-lock-string-face)
     (2 font-lock-comment-face)))
  "Keywords to highlight when editing a GNU Go pattern database.")

(defvar gnugo-db-mode-map nil
  "Keymap for GNUGO Pattern Database mode.")

(defun gnugo-db-occur ()
  "Do the equivalent of M-x occur ^Pattern RET."
  (interactive)
  (widen)
  (goto-char (point-min))
  (occur "^Pattern"))

(defun gnugo-db-what-the-hell ()
  "Visit the Patterns chapter in the GNU Go info page."
  (interactive)
  (info "(gnugo)Patterns"))

(defun gnugo-db-beginning-of-pattern ()
  "Find the beginning of the current Pattern."
  (interactive)
  (re-search-backward "^Pattern")
  (beginning-of-line))

(defun gnugo-db-narrow-to-pattern ()
  "Narrow to the current Pattern, leaving point at the head."
  (interactive)
  (unless (looking-at "^Pattern")
    (gnugo-db-beginning-of-pattern))
  (let ((p (point)))
    (end-of-line)
    (re-search-forward "^Pattern" (point-max) t)
    (beginning-of-line)
    (narrow-to-region p (point))
    (goto-char p)))

(defun gnugo-db-next-pattern ()
  "Go to the next Pattern."
  (interactive)
  (when (looking-at "^Pattern")
    (forward-char 1))
  (let ((narrowedp (not (and (= 1 (point-min))
                             (= (point-max) (1+ (buffer-size)))))))
    (widen)
    (when (re-search-forward "^Pattern" (point-max) 1)
      (beginning-of-line)
      (when narrowedp
        (save-excursion
          (gnugo-db-narrow-to-pattern))))))

(defun gnugo-db-prev-pattern ()
  "Go to the previous Pattern."
  (interactive)
  (let ((narrowedp (not (and (= 1 (point-min))
                             (= (point-max) (1+ (buffer-size)))))))
    (widen)
    (unless (looking-at "^Pattern")
      (gnugo-db-beginning-of-pattern))
    (gnugo-db-beginning-of-pattern)
    (when narrowedp
      (gnugo-db-narrow-to-pattern))))

(defun gnugo-db-mode ()
  "Major mode for editing GNU Go Pattern Database files.
You may wish to extend it by customizing `gnugo-db-font-lock-keywords'.

\\{gnugo-db-mode-map}"
  (interactive)
  (text-mode)
  (auto-fill-mode -1)
  (setq major-mode 'gnugo-db-mode
        mode-name "GNUGO Pattern Database"
        font-lock-defaults '(gnugo-db-font-lock-keywords
                             t          ; keywords only
                             nil        ; case-fold
                             nil        ; syntax alist
                             gnugo-db-beginning-of-pattern))
  (use-local-map gnugo-db-mode-map))

;; load-time actions
(unless gnugo-db-mode-map
  (setq gnugo-db-mode-map
        (let ((m (make-sparse-keymap)))
          (define-key m "\M-?" 'gnugo-db-what-the-hell)
          (define-key m "\C-\M-n" 'gnugo-db-next-pattern)
          (define-key m "\C-\M-p" 'gnugo-db-prev-pattern)
          (define-key m "\C-c\C-h" 'gnugo-db-what-the-hell)
          (define-key m "\C-c\C-o" 'gnugo-db-occur)
          (define-key m "\C-c\C-w" 'widen)
          (define-key m "\C-c\C-n" 'gnugo-db-narrow-to-pattern)
          m)))

(provide 'gnugo-db)

;;; gnugo-db.el ends here
