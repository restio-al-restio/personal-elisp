;;; insert-world.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Create HTMLized world and insert in current buffer.

;;;###autoload
(defun insert-world (&optional links)
  "Insert ASCII picture of globe in current buffer.
When optional arg LINKS is specified, the output is marked up so that
varying contiguous characters form the text of an HTML <a> tag, with
the reference taken randomly from LINKS.  Additionally, the entire output
is surrounded by <tt> and <pre> tags.

LINKS may be the name of a file containing one URL per line.
LINKS may also be a list or vector of URLs.
When called interactively, Emacs prompts for a file name."
  (interactive "fFile w/ links: ")
  (let* ((cmd (concat "/usr/X11R6/bin/xearth -ppm -size 150,150 -pos random"
                      " | mogrify -negate pbm:-"
                      " | pbmtoascii -2x4 -"    ; todo: use aview
                      " | tr '<' L"))           ; safety kludge
         (links (cond ((and (stringp links) (not (file-directory-p links)))
                       (apply 'vector (split-string
                                       (with-temp-buffer
                                         (insert-file-contents links)
                                         (buffer-string)))))
                      ((listp links)
                       (apply 'vector links))
                      ((vectorp links)
                       links)
                      (t nil)))
         (num-unique (length links))
         (preamble  (if (zerop num-unique) "" "\n<tt><pre>\n"))
         (postamble (if (zerop num-unique) "" "\n</pre></tt>\n"))
         (count 0))
    (insert preamble)
    (let ((start (point)))
      (insert (shell-command-to-string cmd))
      (unless (zerop num-unique)
        (save-excursion
          (let ((re (apply 'vector
                           (mapcar (lambda (i)
                                     (apply 'concat
                                            (make-list i "[^ \n]")))
                                   '(0 1 2 3 4 5 6 7))))
                chunk)
            (while (progn
                     (setq chunk (+ 2 (random 6)))
                     (re-search-backward (aref re chunk) start t))
              (cl-incf count)
              (save-excursion
                (insert "<a href=\"" (aref links (random num-unique)) "\">")
                (forward-char chunk)
                (insert "</a>")))))))
    (insert postamble)
    (cons count num-unique)))

(provide 'insert-world)

;;; insert-world.el ends here
