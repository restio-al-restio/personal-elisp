;;; random-life.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Run `life' with a random starting grid.

(require 'life)

;;;###autoload
(defun random-life ()
  (interactive)
  (let* ((w (+ 10 (random (- (frame-width) 20))))
         (h (+ 10 (random (- (frame-height) 20))))
         ;; a pleasant propensity to delightful density
         (n (random (round (/ (* w h) 2))))
         (r (mapcar (lambda (_) (make-string w 32))
                    (number-sequence 1 h))))
    (while (cl-plusp n)
      (aset (nth (random h) r) (random w) ?@)
      (setq n (1- n)))
    (let ((life-patterns (vector r)))
      (life 0))))

(provide 'random-life)

;;; random-life.el ends here
