;;; set-pointer-shape.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2002, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Set pointer shape.

;;;###autoload
(defun set-pointer-shape (n)
  (interactive "nPointer shape number: ")
  (setq x-pointer-shape n)
  (set-mouse-color nil))

(provide 'set-pointer-shape)

;;; set-pointer-shape.el ends here
