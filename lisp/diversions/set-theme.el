;;; set-theme.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2002, 2003, 2004, 2006, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Select an appearance configuration.

(defvar set-theme-themes
  '( ;; name     bg    fg        m-fg    m-bg (optional)
    (classic-ttn \#a85 black     white   black)
    (new-earthy  black sienna    gray30)
    (zzzzzzzzzz  black darkgreen black)
    (caffeine    black yellow    white)
    (polar       white black     white   black)
    (dream       black cyan      white   blue)
    (fuori-fuso  black green     magenta)
    (vt220       black goldenrod gray30))
  "List w/ elements of form:
 \(NAME BACKGROUND FOREGROUND MODELINE-FOREGROUND [MODELINE-BACKGROUND]\)
If MODELINE-BACKGROUND is not specified, it defaults to BACKGROUND.
All elements are symbols.  Use `\\#RGB' to specify a color using RGB components.")

;;;###autoload
(defun set-theme (name)
  "Select appearance configuration NAME (see `set-theme-themes')."
  (interactive (list (completing-read "Theme: "
                                      set-theme-themes
                                      nil       ;;; predicate
                                      t)))      ;;; require-match
  (when (symbolp name)
    (setq name (symbol-name name)))
  (if (string= "" name)
      (message "(%d themes, none chosen)"
               (and
                (describe-variable 'set-theme-themes)
                (length set-theme-themes)))
    (cl-flet
        ((jam (face back fore)
              (set-face-attribute face nil
                                  :background back
                                  :foreground fore)))
      (cl-destructuring-bind (bg fg m-fg &optional m-bg)
          (mapcar #'symbol-name (cdr (assq (intern name)
                                           set-theme-themes)))
        (jam 'default bg fg)
        (let ((two (list (or m-bg bg)
                         m-fg)))
          (apply #'jam 'mode-line
                 (if (face-inverse-video-p 'mode-line)
                     (nreverse two)
                   two)))))))

(provide 'set-theme)

;;; set-theme.el ends here
