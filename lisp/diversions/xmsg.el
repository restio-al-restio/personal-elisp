;;; xmsg.el                                         -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Wrapper for X program xmessage(1).

;;;###autoload
(defun xmsg (string &optional geom process-name)
  "Invoke xmessage(1) to display message STRING.
However, do nothing if `window-system' is not `x'.  Other args
are optional: GEOM is the window geometry (default \"+0+0\");
PROCESS-NAME is the process object's name (default \"xmessage\")."
  (interactive "sMessage: ")
  (when (eq 'x window-system)
    (start-process (or process-name "xmessage") nil "xmessage"
                   "-geometry" (or geom "+0+0")
                   (or (and (cl-plusp (length string))
                            string)
                       "Emacs says hi!"))))

(provide 'xmsg)

;;; xmsg.el ends here
