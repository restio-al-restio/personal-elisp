;;; another-line.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Copy current line, incrementing integers.  Bus-friendly.  :->

(require 'line-at-point)

;;;###autoload
(defun another-line ()
  "Copy line, preserving cursor column, and increment any integers found."
  (interactive)
  (let ((col (current-column))
        (copy (replace-regexp-in-string
               "[0-9]+" (lambda (old)
                          (number-to-string
                           (1+ (string-to-number
                                old))))
               (line-at-point))))
    (end-of-line)
    (insert "\n" copy)
    (move-to-column col)))

(provide 'another-line)

;;; another-line.el ends here
