;;; append-to-nth-buffer.el                         -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Like `append-to-buffer' but not exactly.
;;; Based on distribution's version found in simple.el.
;;; Args different.

;;;###autoload
(defun append-to-nth-buffer (start end &optional which)
  "Append to specified buffer the text of the region.
It is inserted into that buffer before its point.

When calling from a program, give three arguments:
START, END and WHICH.
START and END specify the portion of the current buffer to be copied.
WHICH is a prefix argument interpreted as an index into the buffer-list.
  When no argument is given, the target buffer is the next one down."
  (interactive "r\np")
  (let ((target (if (= 1 which)
                    (other-buffer)
                  (elt (buffer-list) which))))
    (if (or (not target)
            (cdr (assoc 'buffer-read-only (buffer-local-variables target))))
        (message "Cannot write to buffer `%s'." target)
      (append-to-buffer target start end)
      (when (called-interactively-p 'interactive)
        (message "Modified buffer `%s'." target)))))

(provide 'append-to-nth-buffer)

;;; append-to-nth-buffer.el ends here
