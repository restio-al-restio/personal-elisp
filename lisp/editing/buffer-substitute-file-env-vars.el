;;; buffer-substitute-file-env-vars.el              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1998, 1999, 2000, 2004, 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: In current buffer, substitute env vars that look like files.

;;; Commentary:

;; Rewritten for inclusion in Emacs (remove external dependency).

;;; Code:

;;;###autoload
(defun buffer-substitute-file-env-vars ()
  (let* (buffer-read-only
         (ms-lose (memq system-type '(ms-dos windows-nt)))
         (home (getenv "HOME"))
         (hlen (length home)))
    (save-excursion
      (dolist (ev-pair
               ;; FEV-PAIRS is a sorted canonicalized list, longest first.
               (sort
                (let (fev-pairs)
                  (dolist (ev process-environment)
                    (let* ((x (string-match "=" ev))
                           (v (and x (substring ev (1+ x))))
                           (val (and
                                 v
                                 (not (string= "" v))
                                 (if (and (<= hlen (length v))
                                          (string= (substring v 0 hlen)
                                                   home))
                                     (concat "~" (substring v hlen))
                                   v))))
                      (when (and val (or (eq ?~ (aref val 0))
                                         (eq ?/ (aref val 0))
                                         (and ms-lose
                                              (string-match
                                               "^[a-zA-Z]:/" val))))
                        (setq fev-pairs
                              (cl-acons (substring ev 0 x)
                                        (file-name-as-directory val)
                                        fev-pairs)))))
                  fev-pairs)
                (lambda (a b)
                  (> (length (cdr a)) (length (cdr b))))))
        ;; In buffer, replace EV w/ backward mapping.
        (goto-char (point-max))
        (let ((var (car ev-pair))
              (val (cdr ev-pair)))
          (while (search-backward val (point-min) t)
            (replace-match (concat "$" var "/")))))
      ;; Do HOME, then HOOD replacements.
      ;; HOOD is short for neighborhood, the parent dir of HOME.
      (goto-char 1)
      (while (re-search-forward "$HOME\\>" (point-max) t)
        (replace-match "~"))
      (let* ((hood (file-name-directory (getenv "HOME"))))
        (unless (string= "/" hood)
          (goto-char (point-max))
          (while (search-backward hood (point-min) t)
            (replace-match "~")))))))

(provide 'buffer-substitute-file-env-vars)

;;; buffer-substitute-file-env-vars.el ends here
