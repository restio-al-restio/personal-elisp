;;; c2c-log.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Changes to ChangeLog and ChangeLog to commit log.

(require 'add-log)
(require 'more-add-log)
(require 'log-edit)
(require 'mode-local)
(require 'hashutils)


;;; changes to change logs

(defvar c2c-log-files-to-prep-for-function nil
  "Function to return a list of filenames for ‘c2c-log-prep-change-logs’.
This is called w/ no args.")

;;;###autoload
(defun c2c-log-prep-change-logs (title)
  "Start an entry w/ TITLE in various relevant change logs.

If ‘c2c-log-files-to-prep-for-function’ is a function,
call it w/ no args to get a list of affected filenames.
Otherwise, use the current buffer's file name as the only affected file.

To find the relevant change logs, use ‘c2c-log-find-change-log-files’.

In each relevant change log, do ‘add-titled-change-log-entry’
and insert \"* FILENAME\" for each affected file.

Finally, display a message listing the count and name of the
change log files, and arrange for their buffers to be \"first\"
in the list (via ‘switch-to-buffer’)."
  (interactive "sTitle: ")
  (cl-loop
   for (chlog . rels)
   in (c2c-log-find-change-log-files
       (cond ((functionp c2c-log-files-to-prep-for-function)
              (funcall c2c-log-files-to-prep-for-function))
             (buffer-file-name
              (list buffer-file-name))
             (t
              (user-error "Sorry, this buffer is not visiting a file"))))
   collect (let ((buf (find-file-noselect chlog)))
             (with-current-buffer buf
               (add-titled-change-log-entry title)
               (delete-region (line-end-position 0) (point))
               (dolist (rel rels)
                 ;; The emphasized trailing SPC makes a good "to do" reminder.
                 (insert "\n\t* " rel " ")))
             buf)
   into bufs
   finally (let ((count (length bufs)))
             (message "%d buffer%s: %s"
                      count (if (= 1 count)
                                ""
                              "s")
                      (mapconcat #'buffer-name bufs " | "))
             (mapc #'switch-to-buffer (reverse bufs)))))


;;; change logs to commit log

(defvar c2c-log-pseudo-ChangeLog (format ".%s.ChangeLog"
                                         (or user-login-name
                                             "c2c"))
  "Name of the file in Change Log mode.
This should have no directory component.
The default value is \".USER.ChangeLog\", where USER is the
value of ‘user-login-name’ if non-nil, and \"c2c\" otherwise.
See ‘c2c-log-do-it’.")

(defun c2c-log--affected ()
  (cl-flet
      ((log-edit-mode-p (symbol)
                        (eq 'log-edit-mode symbol)))
    (if (or (log-edit-mode-p major-mode)
            (log-edit-mode-p (get-mode-local-parent major-mode)))
        log-edit-initial-files
      ;; TODO: Make customizable, or heuristical, or both.
      (user-error "Unhandled major-mode: %S" major-mode))))

;;;###autoload
(defun c2c-log-find-change-log-files (affected)
  "Find change log files related to AFFECTED, a list of filenames.
The change log filename is the value of ‘c2c-log-pseudo-ChangeLog’ if
that file exists in ‘default-directory’, otherwise \"ChangeLog\".

Return an alist, associating change log filenames with
the subset (list) of AFFECTED that it \"covers\".

All filenames in the alist are relative; the keys to ‘default-directory’,
the values (or rather, each filename in the values) to the change log
filename's directory."
  (cl-loop
   with chlog = (if (file-exists-p c2c-log-pseudo-ChangeLog)
                    c2c-log-pseudo-ChangeLog
                  "ChangeLog")
   with ht = (make-hash-table :test 'equal)
   for filename in affected
   do (let ((dir (expand-file-name
                  (or (locate-dominating-file
                       (expand-file-name filename)
                       chlog)
                      (user-error "Could not find %s for %s"
                                  chlog filename)))))
        (push (file-relative-name
               filename dir)
              (gethash (file-relative-name
                        (expand-file-name
                         chlog dir))
                       ht)))
   finally return (let ((alist (ht-collect :cons ht)))
                    (dolist (ls alist)
                      (setcdr ls (sort (cdr ls) #'string<)))
                    (sort alist (lambda (a b)
                                  (string< (car a)
                                           (car b)))))))

;;;###autoload
(defun c2c-log-do-it ()
  "Compose and insert commit log text gleaned from changelog entries.

This uses the value of ‘log-edit-initial-files’ if the buffer's
‘major-mode’ or its parent ‘major-mode’ is ‘log-edit-mode’.
If it is not, signal error.

Of these \"affected\" files, if any of them are \"ChangeLog\" files,
coalesce those directly.  Otherwise, search the dirs of the affected
files for ‘c2c-log-pseudo-ChangeLog’ files and coalesce those.
If no changelog files can be found, signal error.

A \"proper\" changelog entry has the form:

  YYYY-MM-DD  AUTHOR  <EMAIL>

          TITLE

          [PARAGRAPH...]
          * FILENAME ...
          ...

w/ an optional blurb \"  (tiny change)\" following the email.
Coalescing compares all the changelog files' date, author,
email (and blurb) and title.  On any difference, signal error.

If those elements are identical in all the changelog files,
they are \"factored out\" and the title is inserted once, in
the current buffer.  The rest of each changelog file, from the
paragraph on, is then inserted, with each FILENAME prefixed w/
the relative directory name of the changelog file that names it,
if necessary; and all bol tab chars removed."
  (interactive)
  (let ((changelogs (mapcar #'car
                            (c2c-log-find-change-log-files
                             (cl-remove-if
                              (lambda (filename)
                                (string= "ChangeLog" (file-name-nondirectory
                                                      filename)))
                              (c2c-log--affected)))))
        head body)
    (dolist (fn changelogs)
      (with-temp-buffer
        (insert-file-contents fn)
        (search-forward "\n\n" nil nil 2)
        (push (buffer-substring-no-properties
               (point-min) (point))
              head)
        (unless (or (null (cdr head))
                    (string= (car head)
                             (cadr head)))
          (user-error "Mismatch in changelog head for %s" fn))
        (push (buffer-substring-no-properties
               (point) (cond ((cl-flet
                                  ((try (rx)
                                        (re-search-forward rx nil t)))
                                (or (try change-log-start-entry-re)
                                    (try "^\f")
                                    (try "^\\S-")))
                              (line-beginning-position))
                             (t
                              ;; better than nothing; user must fix
                              (point-max))))
              body)))
    (setq head (car head)
          head (and (string-match "\n\n\t" head)
                    (substring head (match-end 0)))
          body (cl-map
                'list
                (lambda (fn bod)
                  (let ((pre (file-name-directory fn)))
                    (with-temp-buffer
                      (insert bod)
                      (goto-char (point-min))
                      (while (search-forward "\t* " nil 1)
                        (when pre
                          (insert pre)))
                      (while (re-search-backward "^\t" nil t)
                        (delete-char 1))
                      (buffer-string))))
                (nreverse changelogs)
                body))
    ;; TODO: Factor [PARAGRAPH...] if possible.
    (apply 'insert head body)))

(provide 'c2c-log)

;;; c2c-log.el ends here
