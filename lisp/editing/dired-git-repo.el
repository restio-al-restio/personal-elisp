;;; dired-git-repo.el                               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2020 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Dired the ‘git ls-files’.

(require 'vc-git)

;;;###autoload
(defun dired-git-repo (&optional specifically)
  "Do dired on the files controlled by Git at/under ‘default-directory’.
This is the list returned by ‘git ls-files’.  Optional arg SPECIFICALLY
are more command-line arguments for ‘git ls-files’."
  (interactive "sgit ls-files ")
  (dired (cons default-directory
               (split-string
                (shell-command-to-string
                 (concat "git ls-files -z "
                         specifically))
                (string ?\[ 0 ?\])
                'omit-nulls))))

;;;###autoload
(defun dired-git-repo-changed-since (&optional specifically)
  "Do dired on the files controlled by Git at/under ‘default-directory’.
This is the list returned by ‘glog-files-changed-since’.  Optional arg
SPECIFICALLY are more command-line arguments for ‘glog-files-changed-since’."
  (interactive "sglog-files-changed-since ")
  (let ((default-directory (vc-git-root default-directory)))
    (dired (cons default-directory
                 (split-string
                  (shell-command-to-string
                   (concat "glog-files-changed-since "
                           specifically)))))))

(provide 'dired-git-repo)

;;; dired-git-repo.el ends here
