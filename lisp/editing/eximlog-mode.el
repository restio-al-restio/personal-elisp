;;; eximlog-mode.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Make /var/log/exim4/* files more readable.

(define-generic-mode eximlog
  ;; comment-list
  nil
  ;; keyword-list
  '("Start"
    "Completed")
  ;; font-lock-list
  '(("....-..-.. ..:..:.."
     . font-lock-variable-name-face)
    ("[-=]> \\(\\S-+\\)"
     1 font-lock-warning-face)
    ("R=\\(\\S-+\\)"
     1 font-lock-doc-face))
  ;; auto-mode-list
  '("^/var/log/exim4/")
  ;; function-list
  '(read-only-mode
    auto-revert-tail-mode
    toggle-truncate-lines)
  ;; docstring
  "Mode for /var/log/exim4/* log files.
Includes font locking for bol timestamp, \"Start\" and
\"Completed\", router type and recipient email address.")

(provide 'eximlog-mode)

;;; eximlog-mode.el ends here
