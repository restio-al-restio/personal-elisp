;;; find-many-files.el                              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1996, 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Slurp all files in some dir, given a regular expression.
;;;
;;; todo: Use globbing instead of rx.

;;;###autoload
(defun find-many-files (&optional dir pattern)
  "Open all the files in current directory or in DIR if non-nil."
  (interactive "DDirectory? \nsPattern? ")
  (cd dir)
  (when (string= "" pattern) (setq pattern nil))
  (let ((all (reverse (directory-files dir t pattern))))
    (mapcar 'find-file all)))

(provide 'find-many-files)

;;; find-many-files.el ends here
