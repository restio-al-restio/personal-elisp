;;; forward-page-ignore-narrow.el                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Like `forward-page', except ignores page narrowing.

(require 'narrowed-to-page-p)

;;;###autoload
(defun forward-page-ignore-narrow (&optional count)
  "Like `forward-page', except ignores page narrowing."
  (interactive "p")
  (if (not (narrowed-to-page-p))
      (forward-page count)
    (widen)
    (forward-page count)
    (narrow-to-page)))

(provide 'forward-page-ignore-narrow)

;;; forward-page-ignore-narrow.el ends here
