;;; gzip-or-gunzip-from-dired.el                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: From dired, gzip or gunzip a file.  Probably bound to `z'.

(require 'dired-aux)

;;;###autoload
(defun gzip-or-gunzip-from-dired () "gzip or gunzip.  (only from dired)"
  (interactive)
  (let* ((fn (dired-get-filename nil t))
         (buffer-read-only nil))
    (unless (or (not fn) (file-directory-p fn))
      (cond ((string= ".gz" (substring fn -3))
             (call-process "gunzip" nil nil nil "--best" fn)
             (setq fn (substring fn 0 (- (length fn) 3))))
            (t
             (call-process "gzip" nil nil nil "-f" fn)
             (setq fn (concat fn ".gz"))))
      (dired-update-file-line fn))))

(provide 'gzip-or-gunzip-from-dired)

;;; gzip-or-gunzip-from-dired.el ends here
