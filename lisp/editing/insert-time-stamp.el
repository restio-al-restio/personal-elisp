;;; insert-time-stamp.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2001, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Insert time stamp string.

;;;###autoload
(defun insert-time-stamp (style)
  "Insert time string in current buffer, using prefix arg STYLE.
If STYLE is 1 (default), use format string \"%F %T\".
If STYLE is 2, use format string \"%Y.%m%d.%T%z\".
If STYLE is 3, use format string \"%Y.%m%d.%T%z, d%j, %A\".
If STYLE is 4, use format string \"%F\".

If STYLE is negative, take it as positive and use UTC time."
  (interactive "p")
  (and (not buffer-read-only)
       ;; hey, before you think you should waste more time...
       ;; ISO 8601 is ugly: "%Y-%m-%dT%T%z" => 1997-11-07T21:30:39-0800
       ;; current: "%Y.%m%d.%T%z"           => 1997.1107.21:34:10-0800
       (insert (format-time-string
                (cl-case (abs style)
                  (1 "%F %T")
                  (2 "%Y.%m%d.%T%z")
                  (3 "%Y.%m%d.%T%z, d%j, %A")
                  (4 "%F"))
                (current-time)
                (cl-minusp style)))))

(provide 'insert-time-stamp)

;;; insert-time-stamp.el ends here
