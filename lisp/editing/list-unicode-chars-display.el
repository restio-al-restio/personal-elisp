;;; list-unicode-chars-display.el                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Display Unicode codepoints, chars, names in a buffer.

;;;###autoload
(defun list-unicode-chars-display ()
  "Display in buffer *Unicode Chars* the formatted contents of ‘ucs-names’.
The buffer contains three tab-separated columns:

 UUUU  C  NAME

where UUUU is the hexadecimal code point of character C, named NAME.
C might look strange if Emacs can't find a font to display it correctly."
  (interactive)
  (switch-to-buffer (get-buffer-create "*Unicode Chars*"))
  (erase-buffer)
  (cl-loop
   for (name . i) in (sort (ucs-names)
                           (lambda (a b)
                             (< (cdr a) (cdr b))))
   do (insert (format "%04x" i) ?\t i ?\t name ?\n))
  (goto-char (point-min)))

(provide 'list-unicode-chars-display)

;;; list-unicode-chars-display.el ends here
