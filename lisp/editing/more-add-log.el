;;; more-add-log.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2011, 2013 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Commands for editing ChangeLog files.

(require 'add-log)
(require 'nps)

;;;###autoload
(defun add-titled-change-log-entry (title)
  "Add a ChangeLog entry to the current buffer, with title."
  (interactive "*sTitle: ")
  (nps-writable)
  (add-change-log-entry nil buffer-file-name)
  (save-excursion
    (goto-char (point-min))
    (forward-line 1)
    (insert "\n\t" title "\n")))

;;;###autoload
(defun YYYY-MM-DD-today ()
  "Update timestamp around point to today (UTC).
If there is no YYYY-MM-DD around point, do nothing."
  (interactive)
  (let ((p (point)))
    (skip-chars-backward "0-9-")
    (when (looking-at "[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}")
      (delete-region (match-beginning 0)
                     (match-end 0))
      (insert (format-time-string "%Y-%m-%d" nil t)))
    (goto-char p)))

(provide 'more-add-log)

;;; more-add-log.el ends here
