;;; more-isearch.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2020 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Enhancements to isearch.

;;;###autoload
(define-key search-map "k" 'more-isearch-forward-scheme-keyword)

;;;###autoload
(defun more-isearch-forward-scheme-keyword (&optional _ no-recursive-edit)
  "Do incremental search forward for a scheme keyword.
That is, something that looks like #:SYMBOL.
The prefix argument is currently unused.
Like ordinary incremental search except that your input is treated
as a symbol surrounded by symbol boundary constructs #: and \\_>.
See the command `isearch-forward' for more information."
  (interactive "P\np")
  (isearch-mode t nil nil (not no-recursive-edit)
                'more-isearch-scheme-keyword-regexp))

(defun more-isearch-scheme-keyword-regexp (string &optional lax)
  "Return a regexp which matches STRING as a symbol.
Creates a regexp where STRING is surrounded by symbol delimiters #: and \\_>.
If LAX is non-nil, the end of the string need not match a symbol boundary."
  (concat "#:" (regexp-quote string) (unless lax "\\_>")))

(put 'more-isearch-scheme-keyword-regexp
     'isearch-message-prefix
     "scheme keyword ")

(provide 'more-isearch)

;;; more-isearch.el ends here
