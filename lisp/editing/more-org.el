;;; more-org.el                                     -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: More stuff for Org mode felicity.

(require 'org)

(defun more-org-add-link-abbrev-triple (project homepage commit file)
  "Add three PROJECT-related entries to ‘org-link-abbrev-alist’.
HOMEPAGE, COMMIT and FILE are format strings with a single ‘%s’
to be replaced by PROJECT, to produce, respectively, url-HOMEPAGE,
url-COMMIT and url-FILE.  The link name is formed by concatenating
PROJECT with \"-homepage\", \"-commit\" and \"-file\".  However,
if any of HOMEPAGE, COMMIT or FILE is nil, skip that aspect.

For example, evaluating:

 (more-org-add-link-abbrev-triple
  \"nice-hack\"
  nil
  \"http://example.org/src/%s/commit/?id=\"
  \"http://example.org/src/%s/file/%%s?lnum=t\")

would add the following entries:

 (\"nice-hack-commit\" . \"http://example.org/src/nice-hack/commit/?id=\")
 (\"nice-hack-file\" . \"http://example.org/src/nice-hack/file/%s?lnum=t\")

Note the ‘%%s’ in FILE to produce ‘%s’ in url-FILE."
  (cl-flet
      ((add (aspect url-fmt)
            (when url-fmt
              (add-to-list 'org-link-abbrev-alist
                           (cons (concat project "-" aspect)
                                 (format url-fmt project))))))
    (add "homepage" homepage)
    (add "commit"   commit)
    (add "file"     file)))

;;;###autoload
(defun more-org-add-gnu-link-abbrevs (project)
  "Add entries for GNU-ish PROJECT to ‘org-link-abbrev-alist’.
Entries are named PROJECT-ASPECT.  Here are the aspects, expansions:

 homepage  http://www.gnu.org/software/PROJECT
 commit    http://git.savannah.gnu.org/cgit/PROJECT.git/commit/?id=
 file      http://git.savannah.gnu.org/cgit/PROJECT.git/tree/

NB: ‘homepage’ does NOT end in ?/, to allow for a tag that begins with ?#."
  (interactive "sProject: ")
  (more-org-add-link-abbrev-triple
   project
   "http://www.gnu.org/software/%s"
   "http://git.savannah.gnu.org/cgit/%s.git/commit/?id="
   "http://git.savannah.gnu.org/cgit/%s.git/tree/"))

;;;###autoload
(defun more-org-add-nongnu-link-abbrevs (project)
  "Add entries for non-GNU-ish PROJECT to ‘org-link-abbrev-alist’.
Entries are named PROJECT-ASPECT.  Here are the aspects, expansions:

 homepage  http://www.nongnu.org/PROJECT
 commit    http://git.savannah.nongnu.org/cgit/PROJECT.git/commit/?id=
 file      http://git.savannah.nongnu.org/cgit/PROJECT.git/tree/

NB: ‘homepage’ does NOT end in ?/, to allow for a tag that begins with ?#."
  (interactive "sProject: ")
  (more-org-add-link-abbrev-triple
   project
   "http://www.nongnu.org/%s"
   "http://git.savannah.nongnu.org/cgit/%s.git/commit/?id="
   "http://git.savannah.nongnu.org/cgit/%s.git/tree/"))

;;;###autoload
(defun more-org-scan-backward-replace-with-link-expansion (char)
  "Expand abbrev components and tag between CHAR position and point.
Take the text between the previous CHAR and point as abbreviation
components and tag (delimited by whitespace, which is discarded).
Form an abbreviation by interspersing hyphens, and subsequently
an Org-mode \"link\" by concatenating the abbreviation, a colon,
and the tag.  Expand the link via ‘org-link-expand-abbrev’.
If the result is the same as the input link, signal ‘user-error’.
Otherwise (an actual expansion took place), replace the buffer
text with the expansion.

Interactively, query for a single character (type RET for default
of ‘?\\n’ -- newline).

For example, if prior to invocation the buffer looks like (-!- is point):

  X  foo commit deadbeef01234567   -!-

and there is the entry in ‘org-link-abbrev-alist’:

  (\"foo-commit\" . \"http://example.org/foo.git/cgit/commit/?id=\")

then the link is \"foo-commit:deadbeef01234567\" and after invocation
with CHAR ?X, the buffer looks like (-!- is point):

  X  http:/example.org/foo.git/cgit/commit/?id=deadbeef01234567   -!-

Note that the two SPC chars before and the three SPC chars after
both the original and expanded text are preserved."
  (interactive "cType char that starts the region before point (default: LF): ")
  (setq char (string char))
  (let* ((finish (point-marker))
         (end (if (looking-back "\\s-+" (line-beginning-position) t)
                  (match-beginning 0)
                (point)))
         (beg (+ (search-backward char)
                 (progn (forward-char 1) 1)
                 (skip-chars-forward " ")))
         (ls (split-string
              (buffer-substring-no-properties beg end)
              nil t))
         (abbrev (mapconcat 'identity
                            (butlast ls)
                            "-"))
         (was (format "%s:%s" abbrev (car (last ls))))
         (now (org-link-expand-abbrev was)))
    (unwind-protect
        (if (string= was now)
            (user-error "No expansion found for abbrev ‘%s’" abbrev)
          (goto-char beg)
          (delete-region beg end)
          (insert-before-markers now))
      (goto-char finish))))

(provide 'more-org)

;;; more-org.el ends here
