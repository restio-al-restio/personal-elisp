;;; more-texinfo.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2012 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: More commands for Texinfo editing.

(require 'texinfo)

;;;###autoload
(defun more-texinfo-insert-index-entry (nick entry)
  (interactive "cIndex nick (single character): \nsEntry: ")
  (goto-char (line-beginning-position))
  (unless (eolp)
    (insert "\n")
    (forward-char -1))
  (insert "@" nick "index " entry))

;;;###autoload
(defun more-texinfo-insert-cindex-entry (entry)
  (interactive "sEntry: ")
  (more-texinfo-insert-index-entry ?c entry))

(provide 'more-texinfo)

;;; more-texinfo.el ends here
