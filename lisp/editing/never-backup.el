;;; never-backup.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Inhibit backup based on filename.

(defvar never-backup-regexps nil
  "List of regexps for ‘never-backup-predicate’.")

;;;###autoload
(defun never-backup-predicate (filename)
  "Return t if FILENAME should be backed up.
If FILENAME matches any regexp in ‘never-backup-regexps’,
return nil.  Otherwise, call ‘normal-backup-enable-predicate’
and return what it returns."
  (when (cl-notany (lambda (re)
                     (string-match re filename))
                   never-backup-regexps)
    (normal-backup-enable-predicate filename)))

;;;###autoload
(defun never-backup-add-regexps (&rest regexps)
  "Add each string in REGEXPS to ‘never-backup-regexps’.
This uses ‘add-to-list’ to avoid duplicate elements."
  (dolist (re regexps)
    (add-to-list 'never-backup-regexps re)))

;;;###autoload
(defun never-backup-under (dir)
  "Arrange to never backup files in and under DIR.
This works by adding a regexp to ‘never-backup-regexps’."
  (interactive "DDirectory: ")
  (never-backup-add-regexps
   (concat "^" (regexp-quote
                (expand-file-name
                 (file-name-as-directory dir))))))

;;; insinuation
(setq-default backup-enable-predicate 'never-backup-predicate)

(provide 'never-backup)

;;; never-backup.el ends here
