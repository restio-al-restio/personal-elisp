;;; nice-double-quote.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Support for U+201C, U+201D.

;;;###autoload
(defun nice-double-quote-query-replace ()
  "Replace \" and \" with “ and ”, respectively, using `query-replace'.
When point stops at a open double-quote, type b to enter a recursive edit.
Type b again to run a small command (bound to a keymap in an overlay
that covers the entire buffer (!)) that replaces the close double-quote
with a U+201D (RIGHT DOUBLE QUOTATION MARK), arranges for the open
double-quote to be replaced with U+201C (LEFT DOUBLE QUOTATION MARK)
in the `query-replace' loop, and continues replacing."
  (interactive)
  (let ((ov (make-overlay (point) (point-max)))
        (m (make-sparse-keymap))
        (query-replace-map query-replace-map))
    (define-key query-replace-map "b" 'edit)
    (define-key m "b" (lambda () (interactive)
                        (search-forward "\"" nil nil
                                        ;; don't find the open again;
                                        ;; skip it and look for close
                                        2)
                        (delete-char -1)
                        (insert "”")
                        (push ?y unread-command-events)
                        (exit-recursive-edit)))
    (overlay-put ov 'keymap m)
    (query-replace "\"" "“")
    (delete-overlay ov)))

(provide 'nice-double-quote)

;;; nice-double-quote.el ends here
