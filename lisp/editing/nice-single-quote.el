;;; nice-single-quote.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2010 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Insert U+2018, U+2019 around last N sexps.

;;;###autoload
(defun nice-single-quote (&optional n)
  "Insert ‘ and ’ around the last N sexps; leave point at end.
N defaults to 1.  Successive interactive calls (that set ‘last-command’)
behave differently, however: In that case, find the left single quote
and transpose it with the character preceding it."
  (interactive "p")
  (if (eq 'nice-single-quote last-command)
      (save-excursion
        (search-backward "‘")
        (transpose-chars 1))
    (save-excursion
      (backward-sexp (or n 1))
      (insert "‘"))
    (insert "’")))

;;;###autoload
(defun query-replace-nice-single-quotes ()
  "Replace ` and ' with ‘ and ’, respectively, using `query-replace'.
When point stops at a backtick, type b to enter a recursive edit.
Type b again to run a small command (bound to a keymap in an
overlay that covers the entire buffer (!)) that replaces the next
apostrophe with a U+2019 (RIGHT SINGLE QUOTATION MARK), arranges for
the backtick to be replaced with U+2018 (LEFT SINGLE QUOTATION MARK)
in the `query-replace' loop, and continues replacing."
  (interactive)
  (let ((ov (make-overlay (point) (point-max)))
        (m (make-sparse-keymap))
        (query-replace-map query-replace-map))
    (define-key query-replace-map "b" 'edit)
    (define-key m "b" (lambda () (interactive)
                        (search-forward "'")
                        (delete-char -1)
                        (insert "’")
                        (push ?y unread-command-events)
                        (exit-recursive-edit)))
    (overlay-put ov 'keymap m)
    (query-replace "`" "‘")
    (delete-overlay ov)))

(provide 'nice-single-quote)

;;; nice-single-quote.el ends here
