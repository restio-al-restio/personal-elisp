;;; no-thanks.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2011, 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Turn off various things for certain filenames.

(require 'ws)

(defvar no-thanks-trailing-ws-ok-root-dirs nil
  "Don't zonk eol ws when saving files under these directories.
Normally ‘before-save-hook’ includes ‘delete-trailing-whitespace’.
For files in/under dirs in this list, ‘no-thanks’ arranges to
remove ‘delete-trailing-whitespace’ from ‘before-save-hook’ and
in the process make ‘before-save-hook’ buffer-local.")

(defvar no-thanks-font-locking-filenames '("configure.WEIRD"
                                           "configure")
  "Disable font-locking for these filenames.
Compare using the non-directory part of ‘buffer-file-name’.
See ‘no-thanks’.")

;;;###autoload
(defun no-thanks ()
  "Disable various things based on ‘buffer-file-name’.
These variables control what to disable and their trigger conditions:
 ‘no-thanks-trailing-ws-ok-root-dirs’
 ‘no-thanks-font-locking-filenames’
This is intended to be added to a hook, typically ‘find-file-hook’."
  (when buffer-file-name
    (let ((full (expand-file-name buffer-file-name)))
      (when (cl-some
             (lambda (dir)
               (string-prefix-p (expand-file-name
                                 (file-name-as-directory
                                  dir))
                                full))
             no-thanks-trailing-ws-ok-root-dirs)
        (dont-delete-trailing-whitespace-on-save)))
    (let ((base (file-name-nondirectory buffer-file-name)))
      (when (member base no-thanks-font-locking-filenames)
        (font-lock-mode -1)))))

(provide 'no-thanks)

;;; no-thanks.el ends here
