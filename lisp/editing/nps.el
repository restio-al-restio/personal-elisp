;;; nps.el                                          -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Support "no mod possible" (read-only) operation.

(require 'vc-hooks)

;;;###autoload
(defun nps-read-only ()
  "Ensure current buffer and possibly, its file, are read-only.
Note that, unlike ‘nps-writable’, this does not take into account
whether or not the buffer is visiting a file under version control."
  (interactive)
  (setq buffer-read-only t)
  (when buffer-file-name
    (let ((mode (file-modes buffer-file-name)))
      (when (and mode (not (zerop (logand #o200 mode))))
        (set-file-modes buffer-file-name (logand (lognot #o200) mode))))))

;;;###autoload
(defun nps-writable ()
  "Ensure current buffer and possibly, its file, are writable.
If the buffer is visiting a file under version control and the VCS
checkout model is not ‘implicit’, delegate to ‘vc-next-action’."
  (interactive)
  (if (let ((backend (vc-backend buffer-file-name)))
        (and backend
             (not (eq 'implicit (vc-checkout-model
                                 backend buffer-file-name)))))
      ;; delegate
      (vc-next-action (not 'verbose))
    ;; do it
    (setq buffer-read-only nil)
    (when buffer-file-name
      (let ((mode (file-modes buffer-file-name)))
        (when (and mode (zerop (logand #o200 mode)))
          (set-file-modes buffer-file-name (logior #o200 mode)))))))

(provide 'nps)

;;; nps.el ends here
