;;; org-dblock.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2020 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Some org-dblock-write:* funcs.

;;;###autoload
(defun org-dblock-write:shell (plist)
  "Insert ‘shell-command-to-string’ result.
Plist keys (* means required):
   :dir DIRECTORY
 * :do COMMAND
All values strings."
  (let* ((dir (plist-get plist :dir))
         (default-directory (if dir
                                (file-name-as-directory
                                 (expand-file-name dir))
                              default-directory)))
    (insert (shell-command-to-string (plist-get plist :do)))))

;;;###autoload
(defun org-dblock-write:insert-file (plist)
  "Insert file contents.
Plist keys (* means required):
 * :filename FILENAME
All values strings."
  (insert-file-contents (plist-get plist :filename)))

(provide 'org-dblock)

;;; org-dblock.el ends here
