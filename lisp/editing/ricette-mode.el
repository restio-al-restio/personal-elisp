;;; ricette-mode.el --- vedere ricette              -*- lexical-binding: t -*-

;;; Copyright (C) 2002, 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Major mode for editing recipes.

;;; Versione: 1.5

;;; Commentary:

;; Uso: Mettere la formula:
;;
;;   (add-to-list 'auto-mode-alist
;;                '("^/home/ale/etc/ricette/" . ricette-mode))
;;
;; in ~/.emacs e garantire che questo file (ricette-mode.el) sia in
;; una cartella che è presente in `load-path'.  (Variare come vuoi.)
;;
;; Il comando `ricetta-nuova' ti chiede il nome della ricetta e
;; prepara un buffer vuoto con lo stile desiderato (titoli ecc.);
;; si può invocare automaticamente aggiungendo la formula:
;;
;;   (add-hook 'ricette-mode-hook 'ricetta-nuova)
;;
;; Questo programma è dedicato alla mia maestra -- Alessandra Bertoni
;; (e il suo cervello bello 8-).

;;; Code:

(defmacro ricette-viso-nuovo (nome col spieg)
  (let ((proprio (intern (concat "ricette-" (symbol-name nome) "-viso"))))
    `(progn
       (defvar  ,proprio ',proprio)
       (defface ,proprio '((t (:foreground ,(symbol-name col))))
         ,spieg :group 'ricette-mode))))

(ricette-viso-nuovo  testo  cyan2 "Viso per il testo delle ricette.")
(ricette-viso-nuovo titoli violet "Viso per i titoli delle ricette.")
(ricette-viso-nuovo quanto yellow "Viso per \"g NNN\" e le altre quantità.")
(defvar ricette-font-lock-keywords
  '(("^[ \t]*\\([A-ZÀÈÌÒÙ\t ,]+\\)[ \t]*$"            1 ricette-titoli-viso)
    ("^\\(Preparazione\\|Ingredienti\\)"              0 ricette-titoli-viso)
    ("^[*] \\([-,./0-9]+\\(\\s-*[mkK]*[gl]\\>\\)*\\)" 1 ricette-quanto-viso)
    ("\\<[kK]*g [-,./0-9]+$"                          0 ricette-quanto-viso)
    ("^\\s-*\\([^\n:]+\\):"                           1 ricette-titoli-viso)
    ("."                                              0 ricette-testo-viso)))

(defvar ricette-font-lock-defaults
  '(ricette-font-lock-keywords t))	; t => solo "keywords"

(defun ricette-aggiungere-ingrediente ()
  (interactive)
  (goto-char (point-max))
  (re-search-backward "^Preparazione")
  (re-search-backward "^[*]")
  (end-of-line)
  (insert "\n* "))

(defun ricette-aggiungere-ingrediente-successivo ()
  (interactive)
  (insert "\n* "))

(defun ricetta-nuova ()
  "Chiedere il nome e preparare i titoli."
  (interactive)
  (when (zerop (buffer-size))
    (let ((nome (read-string "Ricetta nuova! Nome? ")))
      (insert (upcase nome)))
    (center-line)
    (insert "\n\n\n\n\n")
    (insert "Ingredienti (per N persone)\n\n* \n\n")
    (insert "Preparazione\n\n...\n")))

(defun ricette-appoggiare-a-destra ()
  (interactive)
  (let ((beg (progn (beginning-of-line) (delete-horizontal-space) (point)))
        (end (progn (end-of-line) (delete-horizontal-space) (point))))
    (beginning-of-line)
    (insert (make-string (- fill-column (- end beg)) 32))))

(define-derived-mode ricette-mode text-mode "Ricette"
  "Major mode per le ricette.
\\{ricette-mode-map}"
  :syntax-table nil
  :abbrev-table nil
  ;; tasti
  (dolist (paio '(("\C-c\C-i" . ricette-aggiungere-ingrediente)
                  ("\C-j"     . ricette-aggiungere-ingrediente-successivo)
                  ("\M-r"     . ricette-appoggiare-a-destra)))
    (define-key ricette-mode-map (car paio) (cdr paio)))
  ;; font lock
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults ricette-font-lock-defaults)
  ;; ecc
  (setq fill-column 70))

(provide 'ricette-mode)

;;; ricette-mode.el finisce qui
