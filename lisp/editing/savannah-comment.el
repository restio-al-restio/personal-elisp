;;; savannah-comment.el                             -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2020 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Mode for writing comments on savannah.gnu.org.

;;;###autoload
(define-derived-mode savannah-comment-mode text-mode "Savannah Comment"
  "Mode for writing comments on savannah.gnu.org.
Auto-fill, truncate-lines off; visual-line, variable-pitch on.
See also: ~/build/dotemacs/.emacs.d/ttn/play/twab.el"
  (auto-fill-mode -1)
  (setq truncate-lines nil)
  (visual-line-mode 1)
  (variable-pitch-mode 1))

;;;###autoload
(add-to-list 'auto-mode-alist
             '("^/tmp/textern-.*/savannah\\.gnu\\.org.*\\.txt$"
               . savannah-comment-mode))

(provide 'savannah-comment)

;;; savannah-comment.el ends here
