;;; scroll-LR-by-20.el                              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998, 1999,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Horizontal scrolling support used by many modes hooks.

(require 'set-keys)

(defvar-local scroll-LR-by-20-minor-mode nil
  "See function `scroll-LR-by-20-minor-mode'.")

(defvar scroll-LR-by-20-minor-mode-map nil
  "See function `scroll-LR-by-20-minor-mode'.")

;;;###autoload
(defun scroll-left-20 () (interactive)
  (scroll-left 20))

;;;###autoload
(defun scroll-right-20 () (interactive)
  (scroll-right 20))

;;;###autoload
(defun scroll-LR-by-20-minor-mode (&optional arg)
  "Toggle scroll-LR-by-20 minor mode.
With ARG, turn minor mode on if ARG is positive, off otherwise.
When minor mode is on, `[' and `]' scroll the window horizontally
left and right, respectively."
  (interactive "P")
  (setq scroll-LR-by-20-minor-mode
        (if (null arg)
            (not scroll-LR-by-20-minor-mode)
          (cl-plusp (prefix-numeric-value arg))))
  (set (make-local-variable 'auto-hscroll-mode)
       (not scroll-LR-by-20-minor-mode)))

(unless scroll-LR-by-20-minor-mode-map
  (setq scroll-LR-by-20-minor-mode-map (make-sparse-keymap))
  (define-keys scroll-LR-by-20-minor-mode-map
    "[" 'scroll-left-20
    "]" 'scroll-right-20)
  (cl-pushnew (cons 'scroll-LR-by-20-minor-mode scroll-LR-by-20-minor-mode-map)
              minor-mode-map-alist
              :test 'equal))

(provide 'scroll-LR-by-20)

;;; scroll-LR-by-20.el ends here
