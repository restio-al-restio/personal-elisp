;;; set-DejaVu.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Jam default font to DejaVu Sans Mono.

;;;###autoload
(defun set-DejaVu (size)
  "Set the default font to DejaVu Sans Mono SIZE.
This is probably doing it wrong for Emacs 22+, sigh."
  (interactive "nSize: ")
  (set-frame-font
   (format "DejaVu Sans Mono-%s"        ; ‘%s’ handles both 16 and 16.5
           size)))

(provide 'set-DejaVu)

;;; set-DejaVu.el ends here
