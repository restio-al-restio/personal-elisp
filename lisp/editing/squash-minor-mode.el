;;; squash-minor-mode.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2010 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Command to toggle squashing of empty lines.

(defvar squash-minor-mode nil
  "Either nil or a list of overlays to delete when exiting the minor mode.")

;;;###autoload
(defun toggle-squash-minor-mode ()
  "Toggle squashing of empty lines.
Squashing means empty lines are hidden by an overlay
with property `display' having value \"\"."
  (interactive)
  (unless (local-variable-p 'squash-minor-mode)
    (set (make-local-variable 'squash-minor-mode) nil))
  (setq squash-minor-mode
        (if squash-minor-mode
            (not (mapc 'delete-overlay squash-minor-mode))
          (save-excursion
            (goto-char (point-min))
            (let (ovs)
              (while (re-search-forward "^\\(\\s-*\n\\)+" nil t)
                (let ((ov (make-overlay (match-beginning 0) (point))))
                  (overlay-put ov 'display "")
                  (push ov ovs)))
              ovs))))
  (message "squash-minor-mode %s" (if squash-minor-mode 'on 'off)))

;;; Load-time actions.
(add-to-list 'minor-mode-alist '(squash-minor-mode " squash") t)

(provide 'squash-minor-mode)

;;; squash-minor-mode.el ends here
