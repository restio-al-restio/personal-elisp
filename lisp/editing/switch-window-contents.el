;;; switch-window-contents.el                       -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2006, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Switch the contents of two windows.

;;;###autoload
(defun switch-window-contents (&optional cursor-follows-buffer-p)
  "Switch the contents of the selected window with next one.
Prefix arg CURSOR-FOLLOWS-BUFFER-P means to move the cursor
to the other window after the switch (default is to keep it
in the same window, selecting in it the \"other\" buffer).
If there is only one window, display `other-buffer' in a new window."
  (interactive "P")
  (if (one-window-p)
      (display-buffer (other-buffer (current-buffer)))
    (let* ((one (selected-window))
           (two (cadr (window-list nil nil one)))
           (one-buf (with-selected-window one (current-buffer)))
           (two-buf (with-selected-window two (current-buffer))))
      (with-selected-window one
        (switch-to-buffer two-buf))
      (with-selected-window two
        (switch-to-buffer one-buf))
      (pop-to-buffer (if cursor-follows-buffer-p one-buf two-buf)))))

(provide 'switch-window-contents)

;;; switch-window-contents.el ends here
