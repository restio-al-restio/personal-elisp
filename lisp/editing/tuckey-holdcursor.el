;;; tuckey-holdcursor.el                            -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1996, 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: User interface layer for `scroll-in-place'.
;;; Thanks go to Jeff Tuckey for introducing the idea.

(require 'scroll-in-place)

;;;###autoload
(defvar holdcursor-ladder-rung 1
  "the last arg used for down-holdcursor or up-holdcursor")

(unless (boundp 'scroll-in-place)
  (setq scroll-in-place nil))

;;;###autoload
(defun down-holdcursor (&optional arg)
  (interactive "p")
  (if (or (eq last-command 'down-holdcursor)
          (eq last-command 'up-holdcursor))
      (setq arg holdcursor-ladder-rung)
    (setq holdcursor-ladder-rung arg))
  (if scroll-in-place
      (scroll-up-in-place arg)
    (scroll-up arg)
    (forward-line arg)))

;;;###autoload
(defun up-holdcursor (&optional arg)
  (interactive "p")
  (if (or (eq last-command 'down-holdcursor)
          (eq last-command 'up-holdcursor))
      (setq arg holdcursor-ladder-rung)
    (setq holdcursor-ladder-rung arg))
  (if scroll-in-place
      (scroll-down-in-place arg)
    (scroll-down arg)
    (forward-line (- arg))))

(provide 'tuckey-holdcursor)

;;; tuckey-holdcursor.el ends here
