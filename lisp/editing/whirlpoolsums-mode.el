;;; whirlpoolsums-mode.el                           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2009, 2011, 2012, 2017 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Major mode for editing WHIRLPOOLSUMS files.

(require 'set-keys)
(require 'epg)

;;;###autoload
(defun update-WHIRLPOOLSUMS (filename &optional keep-all)
  "Reset buffer, insert whirlpool sum of FILENAME, then GPG sign.
Reset means to delete from point-min to the beginning of the line
where point is, and also from the first blank line to eob.  It's no
problem if the initial buffer is empty, or lacks the cleartext
signature (so feel free to do \\[erase-buffer]).

Prefix arg KEEP-ALL makes reset keep all existing
entries, disregarding current point.

If signing is successful, leave point at new entry.
For example (* represents point), before:

  -----BEGIN PGP SIGNED MESSAGE-----
  Hash: SHA1

  (sum)  abc
  (sum)  def*

  -----BEGIN PGP SIGNATURE-----[...]

After doing \\[update-WHIRLPOOLSUMS] for \"newfile\":

  -----BEGIN PGP SIGNED MESSAGE-----
  Hash: SHA1

  (sum)  def
  (sum)* newfile

  -----BEGIN PGP SIGNATURE-----[...]

If signing is unsuccessful, you need to do GPG sign (cleartext)
until successful (or you give up!)."
  (interactive "fWhirlpool Sum on filename: \nP")
  (when keep-all
    (goto-char (point-min))
    (re-search-forward "Hash:.+\n\n" nil t))
  (beginning-of-line)
  (delete-region (point-min) (point))
  (when (search-forward "\n\n" nil t)
    (delete-region (1- (point)) (point-max)))
  (shell-command (format "whirlpool-sum %S" (file-relative-name filename)) t)
  (forward-line 1)
  (insert "\n")
  ;; Starting with Emacs 23, there is EPA (built on EPG),
  ;; which obsoletes Emacs 22 PGG.
  (if (memq 'autoload (symbol-plist 'pgg-sign))
      (call-interactively 'pgg-sign)
    ;; Call ‘epg-make-context’ first to trigger autoload.
    (let* ((context (epg-make-context 'OpenPGP))
           (new (epg-sign-string context
                                 (encode-coding-string (buffer-string)
                                                       'utf-8)
                                 'cleartext)))
      (erase-buffer)
      (insert new)))
  (search-backward "\n\n-----BEGIN")
  (move-to-column goal-column))

;;;###autoload
(define-derived-mode whirlpoolsums-mode text-mode "WHIRLPOOLSUMS"
  "Major mode for editing WHIRLPOOLSUMS files.
In this `text-mode' derivative, the actual whirlpool
sums (128 hex digits) are displayed as \"(sum)\", and
the `goal-column' is set to 128.

\\{whirlpoolsums-mode-map}"
  :syntax-table nil
  :abbrev-table nil
  (setq goal-column 128)
  (font-lock-add-keywords
   nil '(("[[:xdigit:]]\\{128\\}"
          0 (list 'face font-lock-type-face
                  'display "(sum)")))))

;;;---------------------------------------------------------------------------
;;; load-time actions

(define-keys whirlpoolsums-mode-map
  "n" 'next-line
  "p" 'previous-line
  "\C-c\C-c" 'update-WHIRLPOOLSUMS
  "\C-c\C-e" 'erase-buffer
  "\C-c\C-s" 'pgg-sign)

;;;###autoload
(add-to-list 'auto-mode-alist '("WHIRLPOOLSUMS" . whirlpoolsums-mode))

(provide 'whirlpoolsums-mode)

;;; whirlpoolsums-mode.el ends here
