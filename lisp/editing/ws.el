;;; ws.el                                           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2011 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Commands to futz with whitespace.

;;;###autoload
(defun toggle-show-trailing-whitespace ()
  "Toggle global value of `show-trailing-whitespace'."
  (interactive)
  (message "show-trailing-whitespace now %s"
           (setq show-trailing-whitespace
                 (not show-trailing-whitespace))))

;;;###autoload
(defun dont-delete-trailing-whitespace-on-save ()
  "Remove `delete-trailing-whitespace' from `before-save-hook'.
Make that variable buffer-local in the process."
  (interactive)
  (let ((was (copy-sequence before-save-hook)))

    ;; This no longer works w/ Emacs 27.1, apparently.  :-/
    ;;- (set (make-local-variable 'before-save-hook)
    ;;-      (remq 'delete-trailing-whitespace before-save-hook))

    ;; This is the preferred way, probably.
    (make-local-variable 'before-save-hook)
    (remove-hook 'before-save-hook
                 'delete-trailing-whitespace
                 ;; locally
                 t)
    (message "%s\nwas: %S\nnow: %S" 'before-save-hook
             was before-save-hook)))

;;;###autoload
(defun reindent-buffer ()
  "Reindent the entire buffer (or accessible region, if narrowed)."
  (interactive)
  (save-excursion (indent-region (point-min) (point-max))))

(provide 'ws)

;;; ws.el ends here
