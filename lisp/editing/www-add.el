;;; www-add.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Add to ~/.www-links the URL at point.

;;;###autoload
(defun www-add ()
  "Add to ~/.www-links the URL at point."
  (interactive)
  (let ((url (thing-at-point 'url))
        (filename "~/.www-links"))
    (with-temp-buffer
      (insert-file-contents filename)
      (if (search-forward url (point-max) t)
          (message "Already: %s" url)
        (insert url "\n")
        (write-file filename)))))

(provide 'www-add)

;;; www-add.el ends here
