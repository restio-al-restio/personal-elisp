;;; zap-up-to-char.el                               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2009 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Like `zap-to-char' but stops short.

;;;###autoload
(defun zap-up-to-char (arg char)
  "Kill up to but not including ARG'th occurrence of CHAR.
Internally, this uses `zap-to-char', then inserts CHAR again."
  (interactive "p\ncZap up to char: ")
  (zap-to-char arg char)
  (insert char)
  (unless (cl-minusp arg)
    (forward-char -1)))

(provide 'zap-up-to-char)

;;; zap-up-to-char.el ends here
