;;; alive.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2012 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Run alive(1) in a buffer.

(defvar alive-mode-map (copy-sequence
                        (eval-when-compile
                          (let ((m (make-sparse-keymap)))
                            (suppress-keymap m 'nodigits)
                            (define-key m "?" 'describe-mode)
                            (define-key m "g" 'revert-buffer)
                            (define-key m "i" 'alive-info)
                            m)))
  "Keymap for Alive mode.  See ‘alive’.")

(defun alive-info ()
  "Visit the alive(1) documentation."
  (interactive)
  (info "(alive)"))

(defun alive--hey (sig)
  (let ((proc (when (eq 'alive-mode major-mode)
                (get-buffer-process (current-buffer)))))
    (cond (proc (signal-process proc sig))
          ((eq 'HUP sig) (alive))
          (t (error "No process")))))

(eval-when-compile
  (let ((form (cl-loop
               with fn
               for (key  sig action)
               in '((?r  HUP "restart")
                    (?w ALRM "wake from sleep and ping the next host")
                    (?q  INT "quit"))
               do (setq fn (intern (format "alive-hey-%s"
                                           (car (split-string
                                                 action)))))
               ;; TODO: Use ‘apply-partially’ and ‘fset’ here, once
               ;; we figure out how to inject the ‘interactive’ form.
               collect `(defun ,fn ()
                          ,(format
                            (concat "Send SIG%s to the ‘alive’ subprocess."
                                    "\nThis causes it to %s.")
                            sig action)
                          (interactive)
                          (alive--hey ',sig))
               into ls
               collect `(define-key alive-mode-map
                          ,(char-to-string key)
                          ',fn)
               into ls
               finally return (cons 'progn
                                    ls))))
    (if (and (boundp 'byte-compile-current-buffer)
             byte-compile-current-buffer)
        (save-excursion
          (let ((print-escape-newlines t))
            ;; Prefer ‘prin1’ to ‘print’ to preserve accuracy of line
            ;; numbers in downstream diagnostic messages (if any :-D).
            (prin1 form (current-buffer))))
      (eval form))))

(define-derived-mode alive-mode nil "Alive"
  "Run alive(1) in a buffer.
In this buffer, keys do not insert themselves.
Instead, there are these commands:
\\{alive-mode-map}"
  :syntax-table nil
  :abbrev-table nil
  (add-hook 'kill-buffer-query-functions
            (lambda ()
              (let ((proc (get-buffer-process (current-buffer))))
                (when proc
                  (delete-process proc))
                t))
            nil t)
  (set (make-local-variable 'revert-buffer-function)
       (lambda (&rest ignored)
         (alive-hey-wake))))

;;;###autoload
(defun alive ()
  "Switch to a buffer where alive(1) runs as a subprocess.
This buffer is in ‘alive-mode’.  What is alive(1)?  Try ‘alive-info’.
Display the first line of the buffer in the echo area."
  (interactive)
  (let ((buf (get-buffer-create "+ alive")))
    (switch-to-buffer buf)
    (unless (get-buffer-process buf)
      (erase-buffer)
      (alive-mode)
      (accept-process-output
       (start-process "alive" buf "alive"))
      (set-window-start nil (point-min))))
  (message "%s" (save-excursion
                  (goto-char (point-min))
                  (buffer-substring (line-end-position)
                                    (point)))))

;;;---------------------------------------------------------------------------
;;; that's it

(provide 'alive)

;;; alive.el ends here
