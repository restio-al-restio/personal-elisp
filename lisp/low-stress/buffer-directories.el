;;; buffer-directories.el                           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2012 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: List buffers and their ‘default-directory’ values.

;;;###autoload
(defun directory-abbrev-alist-from-env-vars ()
  "Return an alist suitable for `directory-abbrev-alist'.
This formats `process-environment' elements whose expansion
are ‘file-directory-p’ so that the expansion (fully expanded)
is the key and the env variable is prefixed with \"$\".

For example, `process-environment' element \"HACK=~/hack\"
yields alist element: (\"/home/ttn/hack\" . \"$HACK\").

See also `abbreviate-file-name', the intended beneficiary
of (perhaps dynamically) binding `directory-abbrev-alist'."
  (let* ((all (mapcar (lambda (s)
                        (split-string s "="))
                      process-environment))
         (just-dirs (cl-remove-if-not (lambda (two)
                                        (let ((expansion (cadr two)))
                                          (and expansion
                                               (file-directory-p expansion))))
                                      all)))
    (mapcar (lambda (two)
              (cons (expand-file-name (cadr two))
                    (concat "$" (car two))))
            just-dirs)))

;;;###autoload
(defun buffer-directories ()
  "List buffers and their `default-directory' values in another buffer.
The directory names have ‘~’ and directory env var substitutions."
  (interactive)
  (let* ((dirs (mapcar (lambda (buf)
                         (with-current-buffer buf
                           (cons (buffer-name buf)
                                 (expand-file-name
                                  (directory-file-name
                                   default-directory)))))
                       (buffer-list)))
         (max-width (apply 'max (mapcar 'length (mapcar 'car dirs))))
         (fstr (concat "  %s"
                       (propertize " " 'display
                                   (list 'space
                                         :align-to
                                         (+ 5 max-width)))
                       "%s")))
    (switch-to-buffer (get-buffer-create "*buffer directories*"))
    (erase-buffer)
    (insert "\n"
            (let ((directory-abbrev-alist
                   (cl-remove "$HOME"
                              (directory-abbrev-alist-from-env-vars)
                              :test 'string= :key 'cdr)))
              (mapconcat (lambda (x)
                           (format fstr (car x)
                                   (abbreviate-file-name (cdr x))))
                         (sort dirs (lambda (a b)
                                      (> (length (cdr a))
                                         (length (cdr b)))))
                         "\n"))
            "\n")
    (goto-char (point-min))
    (insert "buffer directories\n")))

(provide 'buffer-directories)

;;; buffer-directories.el ends here
