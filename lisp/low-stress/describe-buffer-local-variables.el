;;; describe-buffer-local-variables.el              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 1999, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Describe buffer-local variables and count them, too.

;; NOTE: This command/file used to be called "print-buffer-local-variables"
;;       but that is easy to confuse with "print-buffer", etc.  File renaming
;;       was done in the repository (CVS is notably deficient in this area).

(require 'pp)
(require 'outline)

;;;###autoload
(defun describe-buffer-local-variables ()
  "Display buffer-local variable names and values in another buffer.
If called interactively, display var count in echo area.  Return var count.
The variables listed are taken from the current buffer."
  (interactive)
  (let* ((blv (sort (buffer-local-variables)
                    (lambda (a b)
                      (string< (symbol-name (car a))
                               (symbol-name (car b))))))
         (count (length blv))
         (temp-buffer-show-function (lambda (buf)
                                      (switch-to-buffer buf)
                                      (outline-mode)
                                      (outline-hide-body)
                                      (buffer-enable-undo))))
    (with-output-to-temp-buffer (format "*buffer-local vars for %s*"
                                        (buffer-name))
      (princ (format "%d buffer-local variables\n\n" count))
      (dolist (pair blv)
        (let* ((var (car pair))
               (len (+ 2 (length (symbol-name var))))
               (val (cdr pair)))
          (princ (format "* %s\n" var))
          (princ (make-string len ?=))
          (princ "\n")
          (pp val)
          (if (not val)
              (princ "\n")
            (or (listp val) (vectorp val) (princ "\n")))
          (princ ".\n"))))
    (when (called-interactively-p 'interactive)
      (message "%d buffer-local variables" count))
    count))

(provide 'describe-buffer-local-variables)

;;; describe-buffer-local-variables.el ends here
