;;; dired-especially.el                             -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2006, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Dired followed by repositioning.

(require 'dired)

;;;###autoload
(defun dired-especially-this-file ()
  "Do dired on current directory, repositioning point on current file.
If the current buffer is already Dired, go up one level before repositioning.
If the buffer is not visiting a file, this is like M-x dired RET RET."
  (interactive)
  (let* ((diredp (eq 'dired-mode major-mode))
         (f (cond (diredp (file-name-as-directory
                           (file-name-nondirectory
                            (directory-file-name default-directory))))
                  (buffer-file-name (file-name-nondirectory buffer-file-name))
                  (t nil))))
    (dired ".")
    (when diredp (dired ".."))
    (if (not f)
        (message "buffer-file-name nil")
      (goto-char (point-min))
      (cl-flet
          ((try () (when (re-search-forward
                          (concat " " (regexp-quote f)
                                  "\\("
                                  ;; symlink
                                  " -> .*"
                                  "\\|"
                                  ;; single char marker
                                  "."
                                  "\\)*$")
                          nil t)
                     (dired-move-to-filename))))
        (unless (try)
          (revert-buffer)
          (unless (try)
            (dired-goto-next-nontrivial-file)
            (message "(Could not find %S)" f)))))))

(provide 'dired-especially)

;;; dired-especially.el ends here
