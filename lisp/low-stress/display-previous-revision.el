;;; display-previous-revision.el                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Display previous revision of a version-controlled file.

(require 'vc)

;;;###autoload
(defun display-previous-revision (file rev checkout-switches)
  "Display FILE revision REV w/ CHECKOUT-SWITCHES in a new buffer."
  (interactive "fFile: \nsRevision (symbolic ok): \nsCheckout switches: ")
  (let ((vc-checkout-switches (unless (string= "" checkout-switches)
                                checkout-switches)))
    (switch-to-buffer (vc-find-revision file (if (string= "" rev)
                                                 (vc-working-revision file)
                                               rev))))
  (delete-file buffer-file-name)
  (string-match "~\\(.+\\)~$" buffer-file-name)
  (setq rev (match-string 1 buffer-file-name)
        buffer-file-name nil)
  (rename-buffer (format "%s (%s)" (file-name-nondirectory file) rev)))

(provide 'display-previous-revision)

;;; display-previous-revision.el ends here
