;;; drop-file-and-its-buffers.el                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Call delete-file and kill-buffer together.

;;;###autoload
(defun drop-file-and-its-buffers (filename &optional replace)
  "Delete FILENAME and kill any buffers visiting it.
Also delete FILENAME~ (with trailing squiggle) if that exists.
Prefix arg means replace the contents of the current
buffer w/ that of FILENAME, first."
  (interactive "fDrop file (and buffers on it): \nP")
  (when replace
    (erase-buffer)
    (insert-file-contents filename))
  (delete-file filename)
  (let ((squig (concat filename "~")))
    (when (file-exists-p squig)
      (delete-file squig)))
  (let (buf)
    (while (setq buf (find-buffer-visiting filename))
      (kill-buffer buf))))

(provide 'drop-file-and-its-buffers)

;;; drop-file-and-its-buffers.el ends here
