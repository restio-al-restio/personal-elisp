;;; invisible-cursor-mode.el                        -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2005, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Toggle cursor visibility.

(defvar invisible-cursor-mode nil)

;;;###autoload
(defun invisible-cursor-mode (&optional count)
  "Toggle cursor visibility.
Optional arg COUNT specifies how many times to toggle, default 1.
When toggling more than once, sit for 0.1 seconds between each toggle."
  (interactive "p")
  (setq invisible-cursor-mode (not invisible-cursor-mode))
  (set-frame-parameter (selected-frame) 'cursor-type
                       (if invisible-cursor-mode
                           '(bar . 0)
                         'box))
  (when (< 1 (or count 1))
    (sit-for 0.1)
    (invisible-cursor-mode (1- count))))

(provide 'invisible-cursor-mode)

;;; invisible-cursor-mode.el ends here
