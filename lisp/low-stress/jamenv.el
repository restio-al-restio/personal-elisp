;;; jamenv.el                                       -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2002, 2003, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Function to clear and set env vars.

(defun jamenv--grok ()
  (save-excursion
    (goto-char (point-min))
    (let (clear set)
      (ignore-errors
        (while t
          (let ((a (read (current-buffer)))
                (b (read (current-buffer))))
            (if (eq '- a)
                (setq clear (cons b clear))
              (setq set (cons b (cons a set)))))))
      (cl-values clear
                 set))))

;;;###autoload
(defun jamenv (clear set)
  "Clear env vars in CLEAR and set env vars in SET.
CLEAR is a list of symbols.  SET is a list of symbols,
alternating name and value, name first, similar to a plist."
  (mapc 'setenv (mapcar 'symbol-name clear))
  (cl-loop for (var dir)
           on (mapcar #'symbol-name set)
           by #'cddr
           do (setenv var dir)))

;;;###autoload
(defun jamenv-buffer ()
  "Run `jamenv' on current-buffer."
  (interactive)
  (cl-multiple-value-bind
      (clear set)
      (jamenv--grok)
    (jamenv (reverse clear) (reverse set))
    (message "%s cleared, %s set" (length clear) (length set))))

;;;###autoload
(defun jamenv-from-file (file)
  "Read FILE and call `jamenv' on its contents.
Lines beginning with \";\" are comments.
Lines of the form \"- VAR\" mean to clear env var VAR.
Lines of the form \"VAR VAL\" mean to set env var VAR to value VAL."
  (interactive "fDo `jamenv' on file: ")
  (with-temp-buffer
    (insert-file-contents file)
    (jamenv-buffer)))

;;;###autoload
(defun jamenv-positive-entries (filename)
  (with-temp-buffer
    (insert-file-contents filename)
    (cl-multiple-value-bind
        (_ set)
        (jamenv--grok)
      (setq set (nreverse set))
      (let (acc)
        (while set
          (push (cons (expand-file-name (symbol-name (cadr set)))
                      (format "$%s" (car set)))
                acc)
          (setq set (cddr set)))
        (cl-sort acc (function >)
                 :key (lambda (pair)
                        (length (car pair))))))))

(provide 'jamenv)

;;;---------------------------------------------------------------------------
;;; jamenv.el ends here
