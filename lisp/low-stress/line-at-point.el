;;; line-at-point.el                                -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Return current line as a string (w/o terminating newline).

;;;###autoload
(defun line-at-point ()
  "Return current line, w/o terminating newline, as a string."
  (interactive)                         ; TODO: ZONK
  (buffer-substring (line-beginning-position)
                    (line-end-position)))

(provide 'line-at-point)

;;; line-at-point.el ends here
