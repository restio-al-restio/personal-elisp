;;; message-face-name-under-point-mode.el           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2006, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Display (in echo area) the name of face under point.

(defun message-face-name-under-point ()
  (let ((name (get-text-property (point) 'face)))
    (when (and name (symbolp name))
      (message "%s" name))))

;;;###autoload
(defun message-face-name-under-point-mode ()
  "Toggle display (in echo area) of the name of face under point.
This doesn't seem to work with Diff mode for some uninvestigated reason."
  (interactive)
  (if (memq 'message-face-name-under-point post-command-hook)
      (remove-hook 'post-command-hook 'message-face-name-under-point)
    (add-hook 'post-command-hook 'message-face-name-under-point))
  (message "Message Face Name Under Point mode %sactivated"
           (if (memq 'message-face-name-under-point post-command-hook)
               "" "de"))
  (sit-for 2))

(provide 'message-face-name-under-point-mode)

;;; message-face-name-under-point-mode.el ends here
