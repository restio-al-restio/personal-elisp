;;; multi-shell.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Modification of ``shell'' allows multiple shells w/ prefix.

(require 'comint)
(require 'shell)

(defvar multi-shell-hist-d (expand-file-name "multi-shell-history"
                                             user-emacs-directory)
  "Directory where `multi-shell' maintains shell history files.")

;;;###autoload
(defun multi-shell (n)
  "Do `shell', naming buffer based on N, a small integer (prefix arg).
The shell's history is taken from the file named N in `multi-shell-hist-d',
which will be created (along with parent dirs) if necessary.
See documention for `shell'."
  (interactive "p")
  (let* ((shell-name (format "shell %s" n))
         (buf-name (concat "*" shell-name "*")))
    (unless (comint-check-proc buf-name)
      (let* ((prog (or (getenv "ESHELL")
                       (getenv "SHELL")
                       "/bin/sh"))
             (histfile (expand-file-name (number-to-string n)
                                         multi-shell-hist-d))
             (process-environment (cons (format "HISTFILE=%s"
                                                (expand-file-name histfile))
                                        process-environment))
             (name (file-name-nondirectory prog))
             (startfile (concat "~/.emacs_" name))
             (xargs-name (intern-soft (concat "explicit-" name "-args"))))
        (make-directory (file-name-directory histfile) t)
        (set-buffer (apply 'make-comint shell-name prog
                           (when (file-exists-p startfile) startfile)
                           (if (and xargs-name (boundp xargs-name))
                               (symbol-value xargs-name)
                             '("-i"))))
        (shell-mode)))
    (switch-to-buffer buf-name)))

(defun multi-shell-grep-history (regexp)
  "In `multi-shell-hist-d', do `grep' on REGEXP."
  (interactive "sRegexp: ")
  (let ((default-directory (file-name-as-directory multi-shell-hist-d)))
    (grep (format "grep -nH -e %s  *"
                  (shell-quote-argument regexp)))))

(provide 'multi-shell)

;;; multi-shell.el ends here
