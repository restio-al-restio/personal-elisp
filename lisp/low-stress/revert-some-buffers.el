;;; revert-some-buffers.el                          -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: For each file-related buffer, do `revert-buffer'.

;;;###autoload
(defun revert-some-buffers (&optional skip-read-only)
  "Revert each file-related buffer.
Prefix arg means don't bother with read-only buffers."
  (interactive "P")
  (save-some-buffers)
  (let ((ready (cl-remove-if-not (lambda (buf)
                                   (with-current-buffer buf
                                     (and buffer-file-name
                                          (or (not skip-read-only)
                                              (not buffer-read-only)))))
                                 (buffer-list))))
    (save-excursion
      (dolist (buf ready)
        (set-buffer buf)
        (revert-buffer t t)))
    (message "Buffers reverted: %d %s" (length ready) ready)))

(provide 'revert-some-buffers)

;;; revert-some-buffers.el ends here
