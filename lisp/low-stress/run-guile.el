;;; run-guile.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 1999, 2000, 2001, 2004, 2006,
;;;   2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Run a Guile interpreter repl in a buffer.

(require 'cmuscheme)
(require 'comint)
(require 'square-braces-as-parens)
(require 'set-keys)
(require 'button)

(define-button-type 'hidden-region
  'face 'variable-pitch
  'follow-link t
  'action (lambda (button)
            (let ((full (button-get button 'full))
                  (col (button-get button 'col))
                  (p (button-start button)))
              (save-excursion
                (goto-char (field-beginning (- p 2)))
                (insert "\n" (make-string col ?\s)))
              (delete-region p (line-end-position))
              (insert full)
              (goto-char p))))

;;;###autoload
(defun run-guile-echo-region (start end)
  "Insert the region (up to the first newline).
If the region spans more than one line, insert only the first
line, ending with \" ...\" and a number of closing parentheses.
Finally, insert a newline.  This simulates user input, somewhat.
The ellipses is a button whose action is to replace itself with
the region it represents, inserting a newline at the input start,
as well (to maintain visually-consistent column alignment)."
  (let* ((b start)
         (e-orig end)
         (e (save-excursion
              (goto-char e-orig)
              (if (zerop (skip-chars-backward "[:space:]"))
                  e-orig
                (1+ (point)))))
         (col (save-excursion
                (goto-char b)
                (current-column)))
         (line-e (save-excursion
                   (goto-char b)
                   (line-end-position)))
         (s (buffer-substring b (min e line-e)))
         (full (when (< line-e e)
                 (buffer-substring line-e e)))
         (open (cl-count ?\( s))
         (close (cl-count ?\) s))
         (proc (scheme-proc)))
    (with-current-buffer (process-buffer proc)
      (goto-char (point-max))
      (insert (propertize s 'field 'fake))
      (when (< line-e e)
        (insert " ")
        (insert-text-button "..." :type 'hidden-region
                            'col col 'full full)
        (insert (make-string (- open close) ?\))))
      (insert "\n")
      (set-marker (process-mark proc) (point)))))

(defun run-guile-help (topic)
  "Request Guile help for TOPIC.
This works by constructing a ‘help’ command to send to the
interpreter (see ‘comint-send-input’ and ‘scheme-buffer’).
If TOPIC begins w/ a SPC char, the command looks like:

  (help \"REST\")
  ;; e.g.: \" process\" => (help \"process\")

where REST is TOPIC w/ the leading SPC removed.
Otherwise, the command looks like:

  (help TOPIC)
  ;; e.g.: \"cons\" => (help cons)

NB: No double-quotes."
  (interactive "sGuile help on topic: ")
  (if (zerop (length topic))
      (message "(No topic, no help)")
    (when (= ?\s (aref topic 0))
      (setq topic (format "%S" (substring topic 1))))
    (insert "(help " topic ")")
    (comint-send-input)))

(defun run-guile-apropos (topic)
  "Request Guile apropos for TOPIC.
This works by constructing an ‘apropos’ command to send to
the interpreter (see ‘comint-send-input’ and ‘scheme-buffer’)."
  (interactive "sGuile apropos on topic: ")
  (if (zerop (length topic))
      (message "(No topic, no apropos)")
    (insert (format "%S" (list 'apropos topic)))
    (comint-send-input)))

;;;###autoload
(defun run-guile (program &optional init-file)
  "Run a Guile interpreter PROGRAM repl in a buffer.
Optional arg INIT-FILE means run: PROGRAM -l INIT-FILE
instead of simply PROGRAM.

The variable `scheme-buffer' is set to the new buffer, and the
keys `[' and `]' insert left- and right-parens, respectively.
Additionally, `C-h C-g' runs a \"Guile help\" command, and
`C-h C-a' runs a \"Guile apropos\" command."
  (interactive (list (read-string "Program: " "guile")
                     (read-file-name "Init file: " "~/.run-guile/" "")))
  (run-scheme (if (and (cl-plusp (length init-file))
                       (file-exists-p init-file))
                  (format "%s -l %s" program (expand-file-name init-file))
                program))
  (setq scheme-buffer (rename-buffer (format "*%s*" program) t))
  (square-braces-as-parens-mode 1))

(define-keys inferior-scheme-mode-map
  "\C-h\C-g" 'run-guile-help
  "\C-h\C-a" 'run-guile-apropos)

(provide 'run-guile)

;;; run-guile.el ends here
