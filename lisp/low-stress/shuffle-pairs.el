;;; shuffle-pairs.el                                -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1999, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Insert randomly ordered formatted pairs.

;;;###autoload
(defun shuffle-pairs (fmt elems)
  (while elems
    (let ((idx (* 2 (/ (cl-random (length elems)) 2))))
      (insert (format fmt (nth idx elems) (nth (1+ idx) elems)))
      (setq elems (append (cl-subseq elems 0 idx)
                          (cl-subseq elems (+ 2 idx)))))))

(provide 'shuffle-pairs)

;;; shuffle-pairs.el ends here
