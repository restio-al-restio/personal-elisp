;;; split-into-3-windows.el                         -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Split frame into three windows.  Prefix arg changes axis.

;;;###autoload
(defun split-into-3-windows (&optional arg)
  "Splits one window into three, highest first.  Optional ARG inverts axis.
Currently, doesn't work while minibuffer is busy."
  (interactive "P")
  (let* ((old (if arg (window-width) (window-height)))
         (v (/ (- old 2) 3)))
    (if arg
        (progn
          (split-window-horizontally)
          (while (> (window-width) v)
            (shrink-window-horizontally 1))
          (other-window 1)
          (split-window-horizontally)
          (other-window -1))
      (split-window-vertically)
      (while (> (window-height) v) (shrink-window 1))
      (other-window 1)
      (split-window-vertically)
      (other-window -1))))

(provide 'split-into-3-windows)

;;; split-into-3-windows.el ends here
