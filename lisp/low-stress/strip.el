;;; strip.el                                        -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1996, 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Strip whitespace.  Inspired by GNU make func w/ same name.

;;;###autoload
(defun strip (s)
  "Return a string derived by stripping whitespace surrounding S.
If there is no whitespace (character class ‘[:space:]’), return S."
  (interactive "sString: ")
  (let ((r s))
    (when (string-match "^[[:space:]]+" r)
      (setq r (substring r (match-end 0))))
    (when (string-match "[[:space:]]+$" r)
      (setq r (substring r 0 (match-beginning 0))))
    r))

(provide 'strip)

;;; strip.el ends here
