;;; sum-column.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2002, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Sum contiguous column of numbers that share a decimal point.

;;;###autoload
(defun sum-column (&optional decimal-point-char)
  "Sum contiguous column of numbers that share a decimal point.
Prefix arg means use character at point as the \"decimal point\".
Numbers are `read' starting from [-0-9]+ prior to the decimal point.
Display the sum in the echo area and return it as well."
  (interactive "P")
  (setq decimal-point-char
        (if decimal-point-char
            (char-after)
          ?.))
  (unless (= decimal-point-char (char-after (point)))
    (error "Not on decimal point"))
  (let ((col (current-column)))
    (save-excursion
      (let ((sum 0))
        (while (and (= col (move-to-column col))
                    (= decimal-point-char (char-after (point))))
          (forward-line -1))
        (forward-line 1)
        (while (and (= col (move-to-column col))
                    (= decimal-point-char (char-after (point))))
          (save-excursion
            (while (progn (forward-char -1)
                          (looking-at "[-0-9]")))
            (forward-char 1)
            (cl-incf sum (read (current-buffer))))
          (forward-line 1))
        (message "Sum: %s" sum)
        sum))))

(provide 'sum-column)

;;; sum-column.el ends here
