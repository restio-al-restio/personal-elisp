;;; zonk-buffers.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1999, 2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Delete certain buffers.

(require 'hashutils)

(defvar zonkable-buffer-regexps
  '("\\*mail"
    "\\*ftp"
    "\\*emerge"
    "\\*Completions"
    "^sent "
    "\\*vc"
    "\\*Man"
    "\\*Locate"
    "\\*grep"
    "\\*Occur"
    " info dir"
    " \\*info tag table\\*"
    "\\s-Log\\*$"
    "\\*MailCrypt\\*"
    "\\*\\(Database\\|Compile\\)-Log\\*"
    "\\*Apropos\\*"
    "\\*Bookmark List\\*"
    "^ .code-conver.+-work"
    ;; Add new regexps here.
    )
  "List of regular expressions for `zonk-buffers'.")

(defun zonk-buffers--do-it! (msg ls)
  (let ((modes (when msg
                 (cl-loop
                  with ht = (make-hash-table)
                  for buf in ls
                  do (cl-incf (gethash (buffer-local-value 'major-mode buf)
                                       ht 0))
                  finally return (ht-collect :cons ht))))
        (count (length (mapc #'kill-buffer ls))))
    (when msg
      (if (zerop count)
          (message "None zonked")
        (message "%3d (zonked)\n%s"
                 count (mapconcat
                        (lambda (pair)
                          (format "%3d %s"
                                  (cdr pair)
                                  (let* ((symbol (car pair))
                                         (s (symbol-name symbol)))
                                    (if (string-match "-mode$" s)
                                        (substring s 0 -5)
                                      symbol))))
                        modes
                        "\n"))))))

;;;###autoload
(defun zonk-buffers ()
  "Delete buffers matching regexps in `zonkable-buffer-regexps'."
  (interactive)
  (zonk-buffers--do-it!
   (called-interactively-p 'interactive)
   (cl-loop
    for buffer in (buffer-list)
    when (let ((name (buffer-name buffer)))
           (cl-some (lambda (re)
                      (string-match re name))
                    zonkable-buffer-regexps))
    collect buffer)))

;;;###autoload
(defun zonk-buffers-under (dir)
  "Delete buffers with `default-directory' at or under DIR."
  (interactive "DDirectory: ")
  (zonk-buffers--do-it!
   (called-interactively-p 'interactive)
   (cl-loop
    for buffer in (buffer-list)
    unless (string-match "^\\.\\./" (file-relative-name
                                     (buffer-local-value 'default-directory
                                                         buffer)
                                     dir))
    collect buffer)))

(provide 'zonk-buffers)

;;; zonk-buffers.el ends here
