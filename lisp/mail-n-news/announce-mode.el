;;; announce-mode.el                                -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2009 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Message mode specialized for software release announcements.

(eval-when-compile
  (require 'message)
  (require 'more-message)
  (require 'outline))

(defun announce-setup (project-name mailing-list-address url)
  "Replace buffer contents with a skeleton announcement mail.
The PROJECT-NAME, MAILING-LIST-ADDRESS and URL, as well as the
values of `user-full-name' and `user-mail-address' are spliced
into a form that ends up something looking like:

  From: USER-FULL-NAME <USER-MAIL-ADDRESS>
  To: MAILING-LIST-ADDRESS
  Subject: PROJECT-NAME <VERSION> available
  --text follows this line--
  release notes:

  README excerpt:

  NEWS excerpt:

  tarball, expanded dist, etc, in dir:

    URL

You can use \\[announce-update-NEWS] to update the
NEWS excerpt section and the <VERSION> placeholder."
  (interactive "sProject name: \nsMailing-List address: \nsURL: ")
  (erase-buffer)
  (dolist (x '("release notes"
               "README excerpt"
               "NEWS excerpt"
               "tarball, expanded dist, etc, in dir"))
    (insert x ":\n\n"))
  (insert "  " url)
  (message-setup
   `(("Subject" . ,(format "%s <VERSION> available"
                           project-name))
     ("To" . ,mailing-list-address)
     ("From" . ,(format "%s <%s>"
                        user-full-name
                        user-mail-address)))))

(defun announce-update-NEWS (news)
  "Replace \"NEWS excerpt\" section with excerpt from NEWS.
More precisely:
- scan file NEWS to extract its first top-level tree;
- delete the current buffer's \"NEWS excerpt\" section;
- insert the excerpt;
- format it nicely;
- replace the second-to-last word on the Subject line
  with the version number extracted from the NEWS excerpt."
  (interactive (list (read-file-name "News (default NEWS): " nil "NEWS")))
  (let ((s (with-temp-buffer
             (insert-file-contents news)
             (hack-local-variables)
             (goto-char (point-min))
             (outline-next-heading)
             (let ((p (point)))
               (outline-forward-same-level 1)
               (skip-chars-backward "\n")
               (buffer-substring p (point))))))
    (setq news (file-name-nondirectory news))
    (message-goto-body)
    (search-forward (format "\n%s " news))
    (let ((p (line-beginning-position))
          (q (save-excursion
               (re-search-forward "^\\S-")
               (line-beginning-position))))
      (delete-region p q)
      (insert s "\n\n")
      (untabify p (point))
      (goto-char p)
      (save-excursion
        (looking-at "^- \\(\\S-+\\) | \\(.+\\)$")
        (setq q (match-string 1))
        (replace-match (concat news " for \\1 (\\2):"))
        (message-goto-subject)
        (end-of-line)
        (search-backward " ")
        (zap-to-char -1 ?\s)
        (insert " " q)))))

;;;###autoload
(define-derived-mode announce-mode message-mode "Announce"
  "Message mode, specialized for software release announcements.
Normally, files whose names end in .sent.ANNOUNCE or .wait.ANNOUNCE
are placed into this mode automatically (see `auto-mode-alist').

In this mode there are two special commands:
`announce-setup' (\\[announce-setup]); and
`announce-update-NEWS' (\\[announce-update-NEWS])."
  :syntax-table nil
  :abbrev-table nil
  (with-silent-modifications
    (remove-hook 'before-save-hook 'delete-trailing-whitespace)
    (more-message-protect-signature-separator))
  (setq buffer-read-only nil))

;;;###autoload
(add-to-list 'auto-mode-alist
             '("\\.\\(sent\\|wait\\)\\.ANNOUNCE\\'" . announce-mode))

(define-key announce-mode-map "\C-c\C-a" 'announce-setup)
(define-key announce-mode-map "\C-c\C-n" 'announce-update-NEWS)

(add-hook 'announce-mode-hook 'inhibit-backup-locally)

(provide 'announce-mode)

;;; announce-mode.el ends here
