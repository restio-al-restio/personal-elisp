;;; gnus-extras.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2016 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Extra commands for Gnus felicity.

(require 'nnml)
(require 'hashutils)

;;;###autoload
(defun summarize-recent-gnus-respool-destinations ()
  "Display a summary of respool destinations in the echo area.
For example:

  Destinations:
    2     gmane.lisp.guile.user
    1     mail.misc
    7     mail.sysadmin
    3     ttn.gnu-prog-discuss
    3     ttn.info-gnu
   10     ttn.srfi

This works by counting the last (latter) lines in the *Messages*
buffer that match a regexp based on \"Wrote ROOT/DIR/NNNNN\".
ROOT is taken from `nnml-directory' and DIR is transformed in
the usual way (substituting slash with dot)."
  (interactive)
  (let ((ht (make-hash-table :test 'equal)))
    (with-current-buffer "*Messages*"
      (save-excursion
        (goto-char (point-max))
        (let ((rx (concat "^Wrote "
                          (expand-file-name nnml-directory)
                          "\\(.*\\)/[0-9]+$")))
          (re-search-backward rx)
          (while (looking-at rx)
            (cl-incf (gethash (match-string 1) ht 0))
            (forward-line -1)))))
    (message "Destinations:\n%s"
             (mapconcat (lambda (pair)
                          (format "%3d\t%s"
                                  (cdr pair)
                                  (cl-substitute ?. ?/ (car pair))))
                        (sort (ht-collect :cons ht)
                              (lambda (a b)
                                (string< (car a) (car b))))
                        "\n"))))

(provide 'gnus-extras)

;;; gnus-extras.el ends here
