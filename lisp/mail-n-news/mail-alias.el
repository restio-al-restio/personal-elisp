;;; mail-alias.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2003, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Maintain ~/.mailrc aliases.

(require 'mailalias)

;;;###autoload
(defun mail-alias (a)
  (interactive "sAlias: ")
  (require 'sendmail)
  (when (eq mail-aliases t)
    (build-mail-aliases))
  (let ((lookup (assoc a mail-aliases)))
    (if lookup
        (message "%s" lookup)
      (let ((tag (concat "alias " a))
            (def (read-string "Not found, add definition: "))
            (filename "~/.mailrc"))
        (if (string= "" def)
            (message "(no change)")
          (with-temp-buffer
            (insert-file-contents filename)
            (search-forward "# individuals\n#\n")
            (while (and (looking-at "alias [^\t]+")
                        (string< (match-string 0) tag))
              (end-of-line)
              (forward-char 1))
            (insert tag)
            (while (/= 24 (current-column))
              (insert "\t"))
            (insert def "\n")
            (write-file filename))
          (build-mail-aliases))))))

(provide 'mail-alias)

;;; mail-alias.el ends here
