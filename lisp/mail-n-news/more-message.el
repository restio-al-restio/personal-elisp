;;; more-message.el                                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2016 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Extra goodies for Message mode.

(require 'message)

;;;###autoload
(defun more-message-protect-signature-separator ()
  "Make the signature separator chars read-only and rear-nonsticky.
This uses ‘message-signature-separator’ to find the chars."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (when (re-search-forward message-signature-separator nil t)
      (let ((message-forbidden-properties nil)
            (inhibit-read-only t))
        (add-text-properties
         (match-beginning 0)
         (match-end 0)
         (list 'read-only t
               'rear-nonsticky t)))
      (message "NB: message signature separator protected"))))

(provide 'more-message)

;;; more-message.el ends here
