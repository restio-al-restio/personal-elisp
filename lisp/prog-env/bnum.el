;;; bnum.el                                         -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Convert a number to a binary string.

;;;###autoload
(defun bnum (n)
  "Return non-negative integer N represented in boolean, as a string."
  (when (cl-minusp n)
    (error "Negative: %d" n))
  (cl-loop
   with s
   do (push (if (cl-evenp n)
                ?0
              ?1)
            s)
   do (setq n (ash n -1))
   until (zerop n)
   finally return (apply #'string s)))

(provide 'bnum)

;;; bnum.el ends here
