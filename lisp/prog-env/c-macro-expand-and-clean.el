;;; c-macro-expand-and-clean.el                     -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2000, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Expand a region of C code into another buffer, then clean it.

(require 'cc-mode)

;;;###autoload
(defvar c-macro-expand-and-clean-substitutions
  '("/\\*\\([^*]\\|\\(\\*[^/]\\)\\)*\\*/" ""
    "^\\s-+"                              ""
    "\\`\n+"                              ""
    "[ \t]*[ \t]"                         " "
    "(\\s-+"                              "("
    "\\s-+)"                              ")"
    "{\\|}"                               "\n\\&\n"
    "\\s-*;"                              ";\n"
    "\n\\s-*\n"                           "\n"
    "\\s-*,"                              ","
    ",\\([^\n]\\)"                        ",\n\\1"
    " \\?"                                "\n?"
    " :"                                  "\n:"
    ")\\s-+do\\s-"                        ")\ndo "
    "\\s-+$"                              ""
    "\n\n+\\(\\s-*}\\)"                   "\n\\1"
    "\n\n\n*"                             "\n\n"
    ;; Add regexp / string pairs here.
    )
  "List of alternating strings for `c-macro-expand-and-clean'.
In *Macroexpansion*, each CAR (regexp) is substituted with the CADR.")

(defvar c-macro-expand-and-clean-info nil
  "Information required for redoing `c-macro-expand-and-clean'.")

;;;###autoload
(defun c-macro-expand-and-clean (beg end)
  "Call `c-macro-expand', then do additional cleaning.
See variable `c-macro-expand-and-clean-substitutions'."
  (interactive "r")
  (message "Calling c-macro-expand ...")
  (let ((cb (current-buffer))
        c-mode-common-hook)
    (c-macro-expand beg end nil)
    (set-buffer (get-buffer "*Macroexpansion*"))
    (setq buffer-read-only nil)
    (buffer-disable-undo)
    (set (make-local-variable 'c-macro-expand-and-clean-info)
         (list cb beg end))
    (set (make-local-variable 'revert-buffer-function)
         'redo-c-macro-expand-and-clean)
    (cl-loop for (from to)
             on c-macro-expand-and-clean-substitutions
             by #'cddr
             do (progn
                  (message "Cleaning...")
                  (goto-char (point-min))
                  (while (re-search-forward from nil t)
                    (replace-match to nil nil))))
    (setq indent-tabs-mode nil)
    (indent-region 1 (point-max) nil)
    (message "Cleaning...done")
    (set-buffer cb)))

(defun redo-c-macro-expand-and-clean (&rest ignored)
  (cl-destructuring-bind
      (cb beg end)
      (symbol-value 'c-macro-expand-and-clean-info)
    (set-buffer cb)
    (c-macro-expand-and-clean beg end)))

(provide 'c-macro-expand-and-clean)

;;; c-macro-expand-and-clean.el ends here
