;;; file-contents-to-sexp.el                        -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Read file contents as a sexp.

;;;###autoload
(defun file-contents-to-sexp (filename &optional surroundp literalp)
  "Return contents of FILENAME as a sexp, as from `read'.
Optional second arg SURROUNDP non-nil means to surround the
contents first with parens before doing the `read'.  This is
useful for multi-entry file formats.  Optional third arg LITERALP
non-nil means to use `insert-file-contents-literally' instead of
the default `insert-file-contents'."
  (with-temp-buffer
    (when surroundp
      (insert "()")
      (forward-char -1))
    (funcall (if literalp
                 'insert-file-contents-literally
               'insert-file-contents)
             filename)
    (goto-char (point-min))
    (read (current-buffer))))

(provide 'file-contents-to-sexp)

;;; file-contents-to-sexp.el ends here
