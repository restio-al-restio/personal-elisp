;;; find-files.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1998, 2000, 2003, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Find all files in/under cwd matching a glob.

;;;###autoload
(defun find-files (glob)
  "Selectively do `find-file' on find(1) output using GLOB.
Find(1) is invoked from the current working directory.  Selection
is interactive: `y' or `SPC' accepts, `n' or `DEL' rejects,
`.' accepts then stops, `!' accepts the rest, `q' just stops."
  (interactive "sCall find(1) with glob: ")
  (cl-loop
   with decided
   with cwd = default-directory
   with maybe = (split-string
                 (shell-command-to-string
                  (format "find . -name %s -print0"
                          (shell-quote-argument glob)))
                 "\0" t)
   with len = (length maybe)
   for idx from 1 to len
   for file in maybe
   do (cl-flet
          ((yes () (find-file (expand-file-name file cwd)))
           (too (type) (setq decided type)))
        (cl-case (or decided
                     (read-char
                      (format "[%d/%d] Find %s ? [yn.!q] "
                              idx len
                              (file-relative-name file cwd))
                      t))
          ((?n ?\C-?))
          ((?y ?\s)   (yes))
          (?.         (yes) (too ?n))
          (?!         (yes) (too ?y))
          (?q               (too ?n))
          (t (error "Bad input"))))))

(provide 'find-files)

;;; find-files.el ends here
