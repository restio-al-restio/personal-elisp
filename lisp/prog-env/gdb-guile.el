;;; gdb-guile.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2003, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Utilities for working with guile under GNU gdb.

(defvar gdb-guile-backend-method 'internal
  "Symbol that controls how `gdb-guile-display-scm' talks to gdb.
It can be either `gp' or `internal' (the default).  Unrecognized
values are treated the same as `internal'.")

;;;###autoload
(defun gdb-guile-display-scm ()
  "Request a gdb subprocess to display the value of the symbol at point.
If variable `gdb-guile-backend-method' is `gp', use a .gdbinit-supplied
macro `gp' to make the request, otherwise, use the `internal' method."
  (interactive)
  (save-excursion
    (let ((sym (thing-at-point 'symbol))
          (proc (get-buffer-process
                 (cl-find-if (lambda (buf)
                               (string-match "^.gud-." (buffer-name buf)))
                             (buffer-list)))))
      (cl-case gdb-guile-backend-method
        ((gp) (process-send-string proc (format "gp %s\n" sym)))
        (t (mapc (lambda (template)
                   (process-send-string proc (format template sym)))
                 (list
                  "set gdb_print(%s)\n"
                  "printf \"%s: %%s\\n\", gdb_output\n")))))))

(provide 'gdb-guile)

;;; gdb-guile.el ends here
