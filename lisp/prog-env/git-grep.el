;;; git-grep.el                                     -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Wrap git-grep(1) in various ways.

(require 'grep)

(defun git-grep-get-regexp (blurb)
  (list (read-string (format "git grep %s: " blurb)
                     (when current-prefix-arg
                       (symbol-name (symbol-at-point))))))

;;;###autoload
(defun git-grep (regexp)
  "Like `git-grep-from-top' for REGEXP, but limit to `default-directory'."
  (interactive (git-grep-get-regexp "under this dir"))
  (let ((prev grep-command)
        (grep-use-null-device nil))
    (prog1 (grep (concat "git grep -n -H -e "
                         (shell-quote-argument regexp)))
      (setq grep-history (cdr grep-history)
            grep-command prev))))

;;;###autoload
(defun git-grep-from-top (regexp)
  "Use git-grep(1) for REGEXP."
  (interactive (git-grep-get-regexp "from top"))
  (let ((default-directory default-directory))
    (while (not (file-exists-p ".git"))
      (setq default-directory (file-name-directory
                               (directory-file-name
                                default-directory))))
    (git-grep regexp)))

(provide 'git-grep)

;;; git-grep.el ends here
