;;; git-show-branch.el                              -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2011 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Show "git show-branch" output in a buffer.

(require 'view)
(require 'vc-git)
(require 'set-keys)
(require 'compile)
(require 'file-contents-to-sexp)
(require 'git)

(defvar git-show-branch-map (copy-keymap view-mode-map)
  "Keymap for buffer prepared by `git-show-branch'.")

(defvar git-show-branch-all nil)

;;;###autoload
(defun git-show-branch-all ()
  "Toggle variable `git-show-branch-all'.
If the current buffer is named \"*gb* ...\", that is,
presumed to be created by `git-show-branch', revert it."
  (interactive)
  (message "git-show-branch-all: %S"
           (setq git-show-branch-all
                 (not git-show-branch-all)))
  (when (string-match "^\\*gb\\* " (or (buffer-name) ""))
    (revert-buffer t t)))

(defun git-show-branch-read-remote-name ()
  (let ((all (split-string (shell-command-to-string "git remote"))))
    (unless all
      (error "No remote repos found"))
    (if (cdr all)
        ;; many
        (completing-read "Remote: " all)
      ;; just one
      (car all))))

(defun git-show-branch-fetch (remote)
  "Do \"git fetch REMOTE\" in a *gb DIR* buffer.
If `git-show-branch-all' is set, revert the buffer also."
  (interactive (list (git-show-branch-read-remote-name)))
  (let* ((orig (current-buffer))
         (cmd (format "git fetch %s" remote))
         (compilation-buffer-name-function
          `(lambda (&rest ignored)
             ,(format "*%s*" cmd))))
    (with-current-buffer
        ;; kick it!
        (compilation-start cmd)
      (set (make-local-variable 'compilation-finish-functions)
           (list `(lambda (buffer status)
                    (when (buffer-live-p ,orig)
                      (with-current-buffer ,orig
                        (when git-show-branch-all
                          (message "Refreshing %s" (current-buffer))
                          (revert-buffer-t-t))))))))))

;;;###autoload
(defun git-show-branch (dir)
  "Display \"git show-branch\" output for DIR in a buffer.
Actually, use its top directory, the one returned by `vc-git-root'.
Interactively, try `default-directory' first, and query if no
top directory is found.

You can use commands from keymap `git-show-branch-map' in that buffer.

If file \".git/.gsb/specifically\" exists, its contents specify
branches to show, instead of all of them.  The file format is
`read' as a series of Lisp expressions (all atoms are symbols):
  SPEC ...
where each SPEC is the name of a branch, or a list of the form:
  (REMOTE BRANCH...)
where REMOTE names a remote and BRANCH... the desired branches.
As a special case if BRANCH is ‘*’, that means all the branches
of that REMOTE, and the branches will be displayed with the prefix
\"refs/remotes/\"."
  (interactive (list (or (vc-git-root default-directory)
                         (read-directory-name "Directory: "))))
  (setq dir (file-name-as-directory dir))
  (switch-to-buffer (get-buffer-create (format "*gb* %s" dir)))
  (setq default-directory dir)
  (erase-buffer)
  (buffer-disable-undo)
  (set (make-local-variable 'revert-buffer-function)
       (lambda (&rest _)
         (git-show-branch default-directory)))
  (let* ((filename ".git/.gsb/specifically")
         (specifically (when (file-exists-p filename)
                         (file-contents-to-sexp filename t))))
    (cl-flet*
        ((rem (x) (if git-show-branch-all
                      (let ((remote (symbol-name (car x))))
                        (mapconcat (lambda (b)
                                     (concat remote "/" b))
                                   (mapcar 'symbol-name (cdr x))
                                   " "))
                    ""))
         (exp (x) (funcall (if (symbolp x)
                               'symbol-name
                             #'rem)
                           x)))
      (shell-command
       (format "git show-branch %s"
               (cond (specifically (mapconcat #'exp specifically " "))
                     (git-show-branch-all "-a")
                     (t "")))
       (current-buffer))))
  (insert "Directory: " dir
          " -- " (shell-command-to-string
                  "git describe --long")
          "\n")
  (use-local-map git-show-branch-map))

(defun git-show-branch-reveal-tags (&optional pattern)
  "Reveal tags associated w/ the refs in the current *gb* buffer.
Tags are shown next to the ref, in ‘font-lock-constant-face’.
For example, the original buffer might look like:

 ! [aack] Initial commit
  ! [ack] [v int] factor ‘wd-do’
   * [p] [v] include emacs version in log
 ---
   * [p] [v] include emacs version in log
     ...
   * [p~4] [build] add autotooling

After typing ‘\\[git-show-branch-reveal-tags]’, it looks like:

 ! [aack] Initial commit
  ! [ack] [v int] factor ‘wd-do’
   * [p] [v] include emacs version in log
 ---
   * [p] [v] include emacs version in log
     ...
   * [p~4 - 0.1 - COOL] [build] add autotooling

Note the two tags on p~4, \"0.1\" and \"COOL\".

Optional arg PATTERN specifies a glob to filter tag names.
Using the above example, specifying \"*.*\" for PATTERN
would omit ‘COOL’, leaving only ‘0.1’.

In addition to revealing the tags, display in the echo area
the count of revealed tags, the total number of tags, and
a list of the revealed tags."
  (interactive "sSelect tags matching glob (default: match all tags): ")
  (cl-flet*
      ((list<-sh (fmt &rest args)
                 (split-string (shell-command-to-string
                                (if (not args)
                                    fmt
                                  (apply #'format fmt args)))))
       (commits (ls &optional suffix)
                (list<-sh "git rev-parse %s"
                          (mapconcat (if suffix
                                         (lambda (s)
                                           (concat s suffix))
                                       #'identity)
                                     ls
                                     " ")))
       (dash-sep (ls)
                 (mapconcat #'identity
                            ls
                            " - ")))
    (let ((hash (make-hash-table :test 'equal))
          (total 0))
      ;; multiple tags can point to the same commit
      (cl-loop
       with tags = (or (list<-sh "git tag%s"
                                 (if (zerop (length pattern))
                                     ""
                                   (format " -l %s" (shell-quote-argument
                                                     pattern))))
                       (user-error "No tags"))
       for tag in tags
       for commit in (commits tags (shell-quote-argument "^{}"))
       do (cl-incf total)
       do (push tag (gethash commit hash)))
      (cl-loop
       with refs = (save-excursion
                     (goto-char (point-min))
                     (re-search-forward "^-+$")
                     (cl-loop
                      with col = (current-column)
                      while (and (zerop (forward-line 1))
                                 (= col (move-to-column col))
                                 (looking-at " \\[\\([^][]+\\)\\] "))
                      collect (match-string    1) into acc
                      collect (match-beginning 1) into beg
                      collect (match-end       1) into end
                      finally return (cl-mapcar #'cl-list*
                                                (commits acc)
                                                beg end)))
       with hit
       for (commit beg . end) in refs
       when (setq hit (gethash commit hash))
       nconc (let ((inhibit-read-only t))
               (add-text-properties
                beg end (list
                         'face 'font-lock-constant-face
                         'display (dash-sep
                                   (cons (buffer-substring-no-properties
                                          beg end)
                                         hit))))
               hit)
       into hits
       finally do (let ((count (length hits)))
                    (message "Revealed %d tag%s (of %d)%s"
                             count (if (= 1 count)
                                       ""
                                     "s")
                             total
                             (if hits
                                 (concat ": " (dash-sep hits))
                               "")))))))

(defsubst git-show-branch--branch-exists-p (branch)
  ;; snarfed from git.el ‘git-branch’
  (git-rev-parse (concat "refs/heads/" branch)))

(defun git-show-branch-opportune-oust (branch)
  "Switch to BRANCH, creating it first if necessary.
In the process, update the *git-status* buffer.
This uses ‘git-checkout’ or ‘git-branch’ to do the work."
  (interactive (list (git-read-commit-name "Switch to branch: ")))
  (git-status default-directory)
  (funcall (if (git-show-branch--branch-exists-p branch)
               #'git-checkout
             #'git-branch)
           branch))

(defun git-show-branch-revert-buffer-maybe ()
  "If buffer \"*gb* DIR\" exists, revert it.
DIR is the result of `vc-git-root' on `default-directory',
not coincidentally what `git-show-branch' does."
  (let ((buf (get-buffer (concat "*gb* " (vc-git-root default-directory)))))
    (when buf
      (with-current-buffer buf
        (revert-buffer t t)))))

;;;---------------------------------------------------------------------------
;;; load-time actions

(define-keys git-show-branch-map
  "\C-c\C-a" 'git-show-branch-all
  "\C-c\C-f" 'git-show-branch-fetch
  "T" 'git-show-branch-reveal-tags
  "\C-c\C-o" 'git-show-branch-opportune-oust
  [remap View-quit] 'bury-buffer)

(add-hook 'git-do-commit-hook 'git-show-branch-revert-buffer-maybe)
(add-hook 'git-checkout-hook 'git-show-branch-revert-buffer-maybe)
(add-hook 'git-branch-hook 'git-show-branch-revert-buffer-maybe)

;;;---------------------------------------------------------------------------
;;; that's it!

(provide 'git-show-branch)

;;; git-show-branch.el ends here
