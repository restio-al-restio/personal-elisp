;;; give-me-a-scratch-buffer-now.el                 -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Find *scratch* buffer; put it in lisp-interaction-mode.
;;; Prefix arg guarantees new buffer.
;;;
;;; I leave the mode to the default hook, in this case text-mode-hook (qv).

;;;###autoload
(defun give-me-a-scratch-buffer-now (want-new)
  "Bring up *scratch* or younger siblings if prefixed."
  (interactive "P")

  (switch-to-buffer
   (if want-new
       (generate-new-buffer "*scratch*")
     "*scratch*"))
  (lisp-interaction-mode))

(provide 'give-me-a-scratch-buffer-now)

;;; give-me-a-scratch-buffer-now.el ends here
