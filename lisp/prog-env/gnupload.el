;;; gnupload.el                                     -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2013 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: GNUishly sign files and upload them to the right place.

(require 'dired)
(require 'epg)
(require 'epa)
(require 'net-utils)

(cl-eval-when
    ;; This WHEN list produces no warnings under GNU Emacs 25 and 24.3.
    ;; It was found by trial and error; i confess i have not taken the
    ;; time to grok things in full.  Other combinations tried:
    ;;  - ‘eval-when-compile’ (one, top-level, surrounding)
    ;;  - ‘eval-when-compile’ (one, condition)
    ;;  - ‘eval-when-compile’ (two, condition and body -- only
    ;;    ‘when’ remains outside) -- unabashed flailing, ugh :-/
    ;;  - ‘(eval load compile)’
    ;;  - ‘(eval load)’
    ;;  - ‘(eval compile)’ -- this also produces no warnings
    ;; Probably the gist is: Avoid ‘load’ as a WHEN element in ‘eval-when’.
    ;; Also, ‘eval-when-compile’ ≢ ‘(eval-when (compile) ...)’, evidently.
    ;; For future study:
    ;;  - (info "(cl) Time of Evaluation")
    ;;  - (info "(elisp) Eval During Compile")
    (compile)
  (when (and (fboundp 'epg-context-set-armor)
             (cl-flet
                 ((no (prop func)
                      (not (plist-get (symbol-plist func)
                                      prop))))
               (and (no 'compiler-macro     'epg-context-armor)
                    (no 'byte-obsolete-info 'epg-context-set-armor))))
    (require 'gv)
    (gv-define-simple-setter epg-context-armor    epg-context-set-armor)
    (gv-define-simple-setter epg-context-textmode epg-context-set-textmode)
    (gv-define-simple-setter epg-context-signers  epg-context-set-signers)))

(defvar gnupload-fool nil
  "The name (string) of the signer, or nil.
This is passed to `epg-list-keys' as the NAME argument.")

;;;###autoload
(defun gnupload-sign-files-from-dired (&optional dir)
  "Prompt for GPG key passphrase and sign marked files.
The signatures are binary and detached, named FILENAME.sig.

Optional arg DIR a non-empty string means also prepare the
related directive files, with version 1.2 and directory DIR.
The directive files are text, named FILENAME.directive.asc.

See variable `gnupload-fool'."
  (interactive "sDirectory (RET to skip xfer prep): ")
  (let* ((all (or (cl-remove-if 'file-directory-p (dired-get-marked-files))
                  (error "No files selected")))
         (count (length all))
         (signers (epg-list-keys (epg-make-context 'OpenPGP)
                                 gnupload-fool
                                 t))
         (cache (list nil))
         auth xfer)
    (cl-flet
        ((mkc (purpose)
              (let ((context (epg-make-context 'OpenPGP))
                    ;; unnecessary at the moment
                    ;;- (auth (eq 'auth purpose))
                    (xfer (eq 'xfer purpose)))
                (setf (epg-context-armor    context) xfer
                      (epg-context-textmode context) xfer
                      (epg-context-signers  context) signers)
                (epg-context-set-passphrase-callback
                 context (cons
                          (lambda (context key-id cache)
                            (or (copy-sequence (car cache))
                                (let ((rv (epa-passphrase-callback-function
                                           context key-id nil)))
                                  (when (consp cache)
                                    (setcar cache (copy-sequence rv)))
                                  rv)))
                          cache))
                context)))
      ;; First, sign the tarball.
      (setq auth (mkc 'auth))
      (dolist (filename all)
        (let ((action (format "Signing %s..." (file-name-nondirectory filename)))
              (signature (concat filename ".sig")))
          (epg-context-set-progress-callback
           auth (cons #'epa-progress-callback-function action))
          (message "%s" action)
          (epg-sign-file auth filename signature 'detached)
          (message "%swrote %s" action (file-name-nondirectory signature))))
      ;; Next (maybe), create the directive files.
      (when (setq xfer (and dir (not (zerop (length dir)))
                            (mkc 'xfer)))
        (dolist (filename all)
          (with-temp-file (concat filename ".directive.asc")
            (insert
             (epg-sign-string
              xfer (format "version: 1.2\ndirectory: %s\nfilename: %s\n"
                           dir (file-name-nondirectory filename))
              'cleartext)))))
      ;; Lastly, clean up.
      (when (car cache)
        (clear-string (car cache))
        (setcar cache nil))
      (message "Files written: %d auth, %d xfer"
               count (if xfer count 0)))))

;;;###autoload
(defun gnupload-from-dired (&optional alpha)
  "Upload marked files to GNU.
Normally, the upload directory is /incoming/ftp.
Prefix arg means write to /incoming/alpha, instead."
  (interactive "P")
  (let ((all (or (cl-remove-if 'file-directory-p (dired-get-marked-files))
                 (error "No files selected")))
        (proc (progn (ftp "ftp-upload.gnu.org")
                     (get-buffer-process (current-buffer)))))
    (set (make-local-variable 'comint-preoutput-filter-functions)
         (list (lambda (string)
                 (replace-regexp-in-string "ftp> " "(ftp)\n"
                                           string t t))))
    (cl-flet
        ((send-line (s &rest args)
                    (comint-send-string
                     proc (concat (apply 'format s args)
                                  "\n"))
                    (accept-process-output)))
      (mapc #'send-line
            '("anonymous"               ; login
              "passive"                 ; on
              "glob"                    ; off
              "prompt"                  ; off
              ))
      (send-line "cd /incoming/%s" (if alpha "alpha" "ftp"))
      (send-line "mput %s" (mapconcat 'file-name-nondirectory
                                      all " "))
      (send-line "quit"))))


;;; This is another implementation, using the mighty Ange-FTP.
;;; It shares async nature (good) but is too noisy (bad).

';;;DISABLED
(require 'ange-ftp)
';;;DISABLED
(defun gnupload-from-dired (&optional alpha)
  (interactive "P")
  (let ((all (or (cl-remove-if 'file-directory-p (dired-get-marked-files))
                 (error "No files selected")))
        (dir (format "/anonymous@ftp-upload.gnu.org:/incoming/%s/"
                     (if alpha "alpha" "ftp")))
        ;; Don't futz around w/ ASCII.
        (ange-ftp-binary-file-name-regexp "."))
    (ange-ftp-copy-files-async
     ;; required args for internal (ange-ftp) consistency
     t nil
     ;; don't display progress messages (does not affect "Copying...[done]")
     nil
     (mapcar (lambda (filename)
               (list filename
                     (concat dir (file-name-nondirectory filename))
                     'ok-if-already-exists
                     'keep-date))
             all))))

;;;---------------------------------------------------------------------------
;;; that's it!

(provide 'gnupload)

;;; gnupload.el ends here
