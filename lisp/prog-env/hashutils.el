;;; hashutils.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Working with hash tables.

;;;###autoload
(defun ht-collect (what table)
  "Collect WHAT from hash TABLE.
WHAT is one of:  :keys :vals :cons"
  (let (acc)
    (maphash (cl-case what
               (:keys (lambda (k _) (setq acc (cons k acc))))
               (:vals (lambda (_ v) (setq acc (cons v acc))))
               (:cons (lambda (k v) (setq acc (cl-acons k v acc)))))
             table)
    acc))

;;;###autoload
(defun describe-hash-table (ht)
  "Describe hash table HT in a *Help* buffer.
If HT is not a hash table, it is `eval'ed to find one."
  (interactive "SHash table variable name: ")
  (unless (hash-table-p ht)
    (setq ht (eval ht)))
  (unless (hash-table-p ht)
    (error "Not a hash table"))
  (with-help-window (help-buffer)
    (mapc 'princ (list
                  ht
                  "  rehash-size " (hash-table-rehash-size ht)
                  "  -threshold " (hash-table-rehash-threshold ht)
                  "\n\n"))
    (pp (ht-collect :cons ht))))

(provide 'hashutils)

;;; hashutils.el ends here
