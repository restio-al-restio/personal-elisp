;;; more-info-look.el                               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: [(control h) (shift s)] refinements.

(require 'info-look)

(defvar more-info-look-common
  `((scheme-mode
     :regexp "[^()`',\" \t\n]+"
     :ignore-case nil
     :doc-spec ,(mapcar (lambda (s)
                          (list s nil "^ -- .+: " ""))
                        '("(guile) Procedure Index"
                          "(guile) Variable Index"
                          "(guile) R5RS Index"
                          "(ttn-do) Index")))))

;;;###autoload
(defun more-info-look-add-common ()
  (dolist (ent more-info-look-common)
    (apply 'info-lookup-add-help :mode (car ent) (cdr ent))))

;;;###autoload
(defun more-info-look-mode-add (info-name node-name &optional rest)
  (interactive "sInfo name: \nsNode name: ")
  (let* ((mode-ent (assq major-mode (assq 'symbol info-lookup-alist)))
         (new (cons (format "(%s) %s" info-name node-name)
                    (or rest '(nil "^ -- .+: " ""))))
         (old (nth 3 mode-ent)))
    (unless (member new old)
      (info-lookup-add-help
       :mode major-mode
       :regexp (nth 1 mode-ent)
       :ignore-case (nth 2 mode-ent)
       :doc-spec (cons new old)
       :parse-rule (nth 4 mode-ent)
       :other-modes (nth 5 mode-ent))
      ;; blech!
      (info-lookup-reset))))

(provide 'more-info-look)

;;; more-info-look.el ends here
