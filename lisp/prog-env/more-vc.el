;;; more-vc.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Additional version-control functionality.

(require 'scheme)
(require 'vc)
(eval-when-compile (require 'vc-git))
(eval-when-compile (require 'log-view))
(eval-when-compile (require 'diff-mode))
(eval-when-compile (require 'log-edit))

;;; New functionality.

;;;###autoload
(defun more-vc-set-file-modes-based-on-buffer ()
  "If visiting a version controlled file, sync the file's r[wo] modes.
That is, if the buffer is read-only, set the file mode to ‘u-w’.
OTOH, if the buffer is writeable, set the file mode to ‘u+w’ as well.
This function is intended to added to ‘read-only-mode-hook’."
  (interactive)
  (when (vc-backend buffer-file-name)
    (set-file-modes buffer-file-name
                    (file-modes-symbolic-to-number
                     (format "u%cw" (if buffer-read-only ?- ?+))
                     (file-modes buffer-file-name)))))

;;;###autoload
(defun more-vc-set-state (files state)
  "Set \"file state\" to STATE for each file in FILES.
Revert any buffers visiting those files and return them in a list.
The backend to use is determined by the first element in FILES."
  (let* ((backend (or (vc-backend (expand-file-name (car files)))
                      (error "Not under version control: %s" (car files))))
         (handler (intern (format "more-vc-set-state-%s" backend)))
         (_ (or (fboundp handler)
                (error "No %s commands to set state" backend)))
         (ls (mapconcat (lambda (name)
                          (shell-quote-argument name))
                        files " "))
         (commands (funcall handler state ls))
         (bufs (delq nil (mapcar 'find-buffer-visiting files))))
    ;; Each element in COMMANDS has the form (SHELL-COMMAND ARGS...).
    ;; SHELL-COMMAND is a string, and ARGS... are zero or more objects.
    (with-temp-buffer
      (dolist (command commands)
        (shell-command (apply 'format command) t)))
    (dolist (buf bufs)
      (with-current-buffer buf
        (revert-buffer t t)))
    bufs))

(defun more-vc-set-state-RCS (state ls)
  `(("co -l %s" ,ls)
    ("ci -u -s%S -m'State now %s.' %s" ,state ,state ,ls)))

(defun more-vc-set-state-CVS (state ls)
  `(("cvs commit -f -m'State now %s.' %s" ,state ,ls)
    ("cvs admin -s%S %s" ,state ,ls)
    ("rm -f %s" ,ls)
    ("cvs update %s" ,ls)))

;;;###autoload
(defun more-vc-pretty-up-log ()
  (buffer-disable-undo)
  (let* ((backend (vc-backend (or buffer-file-name
                                  (buffer-file-name vc-parent-buffer))))
         (thunk-name (when backend
                       (intern (format "more-vc-pretty-up-log-%s" backend)))))
    (when thunk-name
      (vc-exec-after `(,thunk-name)))))

(defun more-vc-pretty-up-log-RCS ()
  (let ((re (concat "\\(revision \\)\\S-+"
                    "\\(.*\ndate: \\)[^;]+"
                    "\\(;.+author: \\)[^;]+"
                    "\\(;.+state: \\)[^;]+"
                    "\\(\\(;.+lines: \\)[^;\n]+\\)*"
                    "\\(;.*\\)"))
        buffer-read-only)
    (cl-flet
        ((mprop (n p v) (put-text-property (match-beginning n)
                                           (match-end n)
                                           p v)))

      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward "^-+\n" nil t)
          (mprop 0 'display "\n\n")
          (looking-at re)
          (dolist (ent '((0 font-lock-face match)
                         (1 nil "  ")
                         (2) (3) (4) (6)
                         (7 nil "\n")))
            (let ((n (pop ent))
                  (p (or (pop ent) 'display))
                  (v (or (pop ent) "      ")))
              (when (match-end n)
                (mprop n p v)))))))
    (when (get-buffer-window (current-buffer))
      (recenter 0))))

(defun more-vc-pretty-up-log-CVS ()
  (more-vc-pretty-up-log-RCS))

(defun more-vc-pretty-up-log-Git ()
  (let ((re (concat "\\(commit .*\n\\(Merge:.*\n\\)*Author:.*<\\)[^@]+"
                    "\\(@.*\nDate: +\\).*\n"))
        buffer-read-only)
    (cl-flet
        ((mprop (n p v) (put-text-property (match-beginning n)
                                           (match-end n)
                                           p v)))

      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward re nil t)
          (mprop 0 'font-lock-face 'match)
          (mprop 1 'display "  ")
          (mprop 3 'display "             "))))
    (when (get-buffer-window (current-buffer))
      (recenter 0))))

;;;###autoload
(defun more-vc-log-view-copy-id-as-kill ()
  "Copy the current log entry's \"id\" onto the `kill-ring'.
If point is not `looking-at' the `log-view-message-re', do nothing.
Otherwise, take the match's first group to be the entry's id, copy
this string onto the `kill-ring' (with `kill-new') and lastly,
display it in the echo area."
  (interactive)
  (when (looking-at log-view-message-re)
    (kill-new (buffer-substring-no-properties
               (match-beginning 1)
               (match-end 1)))
    (message "%s" (car kill-ring))))

;;;###autoload
(defun more-vc-likewise ()
  "Edit the current like to be \"\\t(TEXT): Likewise.\".
Leave point at eol.  When called, the line may have one of the forms:

  TEXT
  \\tTEXT
  (TEXT)
  \\t(TEXT)
  \\t* FILENAME (TEXT)

Here, \"\\t\" stands for the TAB character.  As a special case,
if unparenthesized TEXT (i.e., the first two cases) ends with
comma and any whitespace, that part is deleted.

This command is intended for use in Change Log mode."
  (interactive)
  (beginning-of-line)
  (delete-horizontal-space)
  (insert "\t")
  (unless (memq (char-after) '(?\( ?\*))
    (insert "(")
    (end-of-line)
    (delete-horizontal-space)
    (insert ")"))
  (end-of-line)
  (delete-horizontal-space)
  (when (= ?, (char-before))
    (delete-char -1) (insert ")"))
  (insert ": Likewise."))

;;;###autoload
(defun more-vc-diff-NEW ()
  "Diff current buffer's file with its \".NEW\" file.
For instance, if the current buffer is visiting \"foo\",
this command diffs \"foo\" and \"foo.NEW\"."
  (interactive)
  (let ((same-window-buffer-names (cons "*Diff*" same-window-buffer-names))
        (diff-default-read-only t)
        (short (file-name-nondirectory (buffer-file-name))))
    (diff short (concat short ".NEW") nil t)))

;;;###autoload
(defun more-vc-toggle-git-post-commit-hook ()
  "In .../.git/hooks/, toggle filename of post-commit hook.
Set to \"post-commit\" for enabled, \"post-commit.OFF\" for disabled."
  (interactive)
  (let (dir fn off)
    (if (not (setq dir (vc-git-root default-directory)))
        (message "Not under Git: %s" (directory-file-name default-directory))
      (setq fn (expand-file-name ".git/hooks/post-commit" dir)
            off (concat fn ".OFF"))
      (cond ((file-exists-p fn)
             (rename-file fn off)
             (message "%s DISABLED" fn))
            (t
             (rename-file off fn)
             (message "%s ENABLED" fn))))))

(defun more-vc-ref-at-point ()
  "Return the reference at point as a string.
This is essentially the symbol at point under Emacs Lisp syntax.
As a special case, if after the symbol there is \"@{...}\", (i.e.,
at-sign, open-curly-brace, ..., close-curly-brace), then that is
also included in the returned value.  See gitrevisions(7)."
  (save-excursion
    (with-syntax-table scheme-mode-syntax-table
      (forward-symbol 1)
      (let ((end (point)))
        (when (looking-at "@{[^{}]+}")
          (setq end (match-end 0)))
        (forward-symbol -1)
        (buffer-substring-no-properties (point) end)))))

;;;###autoload
(defun more-vc-copy-ref-as-kill ()
  "Copy the ref (see `more-vc-ref-at-point') to the kill ring."
  (interactive)
  (kill-new (more-vc-ref-at-point)))

;;;###autoload
(defun more-vc-git-rebase ()
  "Arrange to invoke `compile' interactively with a \"git rebase\" command.
The full command uses the REF (from `more-vc-ref-at-point') like so:
  git rebase -i REF^
Note the ^ (hat).  You can modify the command further prior to dispatch."
  (interactive)
  (let ((compile-command (format "git rebase -i --autosquash %s^"
                                 (more-vc-ref-at-point))))
    (call-interactively 'compile)))

;;;###autoload
(defun more-vc-git-show (ref)
  "Do \"git show --stat -p REF\".
Interactively, prefix arg means query for REF.
Otherwise, obtain it w/ ‘more-vc-ref-at-point’.
Display output in a read-only buffer in Diff mode,
with Diff Auto-Refine mode enabled."
  (interactive (list (if current-prefix-arg
                         (read-string "Ref: ")
                       (more-vc-ref-at-point))))
  (unless (and ref (not (string= "" ref)))
    (user-error "Invalid ref: %S" ref))
  (let* ((cmd (format "git %s --stat -p %s"
                      (if (string-match "^stash@{.+}$" ref)
                          "stash show"
                        "show")
                      ref))
         (buf (get-buffer-create (format "*(%s) %s*"
                                         (file-name-nondirectory
                                          (directory-file-name
                                           (vc-git-root default-directory)))
                                         ref)))
         (resize-mini-windows nil)
         (pop-up-windows nil))
    (shell-command cmd buf)
    (with-current-buffer buf
      (diff-mode)
      (ignore-errors (diff-auto-refine-mode 1))
      (setq buffer-read-only t))
    (switch-to-buffer buf)))

;;;###autoload
(defun more-vc-register-dwim (&optional fname)
  "Like `vc-register' but if FNAME is already under Git, do a \"git add\"."
  (interactive)
  (unless fname (setq fname buffer-file-name))
  (if (and (eq 'Git (vc-backend fname))
           (vc-registered fname))
      (vc-git-command nil 0 fname "add")
    (call-interactively 'vc-register)))

;;;###autoload
(defun more-vc-set-default-directory-to-project-root ()
  "Set ‘default-directory’ to the project root.
This is intended to be added to ‘log-edit-mode-hook’ and tries
to use ‘log-edit-vc-backend’ to determine the backend."
  (let ((root (ignore-errors
                (vc-call-backend
                 (or log-edit-vc-backend
                     (vc-backend (buffer-file-name
                                  vc-parent-buffer)))
                 'root default-directory))))
    (when root
      (setq default-directory
            (expand-file-name root)))))

(provide 'more-vc)

;;; more-vc.el ends here
