;;; my-prog-env.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001,
;;;   2004, 2005, 2006, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: Set up programming env the way I like it.

(require 'hideshow)
(require 'set-keys)
(require 'square-braces-as-parens)

(defvar C-l-gravity)

(defun delete-indentation-dwim ()
  "Like `delete-indentation', but forward if rest of line is whitespace."
  (interactive)
  (delete-indentation (looking-at "\\s-*\n")))

;;;###autoload
(defvar my-prog-env-makefilish-major-modes '(makefile-mode
                                             makefile-gmake-mode
                                             makefile-automake-mode)
  "Major modes that `my-prog-env' treats like Makefile mode.
Specifically, non-nil value enables `indent-tabs-mode', and
inhibits enabling of `hs-minor-mode'.")

;;;###autoload
(defvar my-prog-env-keybinding-preferences
  (list
   "\C-a"  'beginning-of-code-line
   "\C-e"  'end-of-code-line
   "\C-\?" 'delete-backward-char
   "\C-x[" 'backward-page-ignore-narrow
   "\C-x]" 'forward-page-ignore-narrow
   "\C-c-" 'insert-separator
   "\M-j"  (lambda ()
             (interactive)
             (end-of-line)
             (newline-and-indent))
   "\M-?"  'where-am-i [(shift mouse-3)] 'mouse-where-am-i
   "\M-#"  'delete-indentation-dwim

   "\M-\C-g" 'go

   ;; 1998.0116.02:56:54-0800.  kfjc techno.  very hypnotic.
   ;;
   ;; search for a standard source code string.

   [?\C-c ?\M-\C-h]    'next-here-tag

   ;; 1998.0707.02:31:16-0700.  silence.
   ;;
   ;; must be getting old.  :->

   [f10]               'next-here-tag

   ;; 2001/02/13 01:14:00.  silence.
   ;;
   ;; ugh, hideshow keybindings are a mess, so we toe the line in
   ;; official source but jam it optimally here -- woo hoo!

   "\C-c\C-h"          'hs-hide-block
   "\C-c\C-s"          'hs-show-block
   "\C-c\C-\M-h"       'hs-hide-all
   "\C-c\C-\M-s"       'hs-show-all
   "\C-c\C-l"          'hs-hide-level
   "\C-c\C-c"          'hs-toggle-hiding

   [?\H-!]             'infer/run-compile-command

   ;; Add new interesting keybindings here!
   )
  "Plist of KEY COMMAND [KEY COMMAND...] preferences.
KEY can be a string or vector.  This is used in `my-prog-env'.")

;;;###autoload
(defun my-prog-env ()
  "Set up programming env the way i like it.
See vars: `my-prog-env-makefilish-major-modes',
          `my-prog-env-keybinding-preferences'."

  (interactive)
  (setq comment-style 'indent
        comment-column (if (eq major-mode 'asm-mode) 32 40)
        C-l-gravity (if (or window-system (> (window-height) 30)) -15 -4)
        hs-hide-comments-when-hiding-all nil
        indent-tabs-mode (memq major-mode my-prog-env-makefilish-major-modes)
        ;; Add other vars here.
        )
  (when (memq major-mode '(lisp-mode lisp-interaction-mode scheme-mode))
    (setq comment-add 1))

  (local-set-keys my-prog-env-keybinding-preferences)

  (turn-on-auto-fill)
  (unless (memq major-mode (append '(sh-mode html-mode sgml-mode)
                                   my-prog-env-makefilish-major-modes))
    (save-excursion
      (hs-minor-mode 1)
      (or (memq 'hs-headline mode-line-format)
          (setq mode-line-format
                (append '("-" hs-headline)
                        (cdr mode-line-format))))
      (unless (and this-command
                   (symbolp this-command)
                   (or (memq this-command '(elm lim))
                       (and (eq 'exit-minibuffer this-command)
                            (eq 'revert-buffer
                                (when extended-command-history
                                  (intern (car extended-command-history)))))
                       (string= "-mode"
                                (substring (symbol-name this-command)
                                           -5))))
        (hs-hide-all)
        (hs-hide-initial-comment-block)))
    (when (invisible-p (point))
      (save-excursion (hs-show-block))))
  (when (memq major-mode '(emacs-lisp-mode
                           lisp-interaction-mode
                           scheme-mode))
    (square-braces-as-parens-mode 1)))

;;;###autoload (defalias 'elm 'emacs-lisp-mode)
;;;###autoload (defalias 'lim 'lisp-interaction-mode)

(provide 'my-prog-env)

;;; my-prog-env.el ends here
