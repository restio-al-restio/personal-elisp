;;; narrowed-to-page-p.el                           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Return nil if not narrowed to page boundaries.

;;;###autoload
(defun narrowed-to-page-p ()
  "Returns nil if not narrowed to page boundaries."
  (or (/= (point-min) 1)
      (/= (point-max) (1+ (buffer-size)))))

(provide 'narrowed-to-page-p) 

;;; narrowed-to-page-p.el ends here
