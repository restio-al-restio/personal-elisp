;;; next-here-tag.el                                -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2001, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Search for a standard source code re: "here.$".

;;;###autoload
(defun next-here-tag ()
  (interactive)
  (let ((here-re "here[.!]$"))
    (or (re-search-forward here-re (point-max) t)
        (progn
          (goto-char (point-min))
          (or (progn
                (message "(Wrapped)")
                (re-search-forward here-re (point-max) t))
              (message "No `%s' tags found." here-re))))))

(provide 'next-here-tag)

;;; next-here-tag.el ends here
