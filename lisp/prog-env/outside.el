;;; outside.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Interact w/ the world outside Emacs.

;;;###autoload
(defun rewrite-shell-command-w/cd (string)
  "If STRING begins w/ \"$foo\", return a new string.
In the new string, the \"$foo\" is replaced with \"cd $foo\", and
furthermore \"$foo\" is replaced with the value of `(getenv \"foo\")'.

For example, if (getenv \"hack\") => \"~/build/hack\", then
  (rewrite-shell-command-w/cd \"$hack.b ; echo $hack\")
  => \"cd ~/build/hack.b ; echo $hack\"

For other values of STRING, simply return it."
  (save-match-data
    (if (string-match "^[$]\\([a-zA-Z_]+\\)" string)
        (replace-match (concat "cd " (getenv (match-string 1 string)))
                       t t string)
      string)))

;;;###autoload
(defun bg-shell-command (cmd &optional watch)
  "Do shell CMD in background, renaming controlling buffer.
CMD is passed through `rewrite-shell-command-w/cd'.
Prefix arg WATCH non-nil means switch to that buffer,
wait 0.1s for output and move to the beginning of the buffer."
  (interactive "sShell command (to be backgrounded): \nP")
  (let (buf)
    (save-window-excursion
      (shell-command (concat (rewrite-shell-command-w/cd cmd) "&"))
      (set-buffer "*Async Shell Command*")
      (setq buf (rename-buffer (concat "*bg job* " cmd) t)))
    (when watch
      (switch-to-buffer buf)
      (sit-for 0)
      (accept-process-output (get-buffer-process buf) 0 100 t)
      (goto-char (point-min)))))

;;;###autoload
(defun saved-shell-command (command)
  "Do shell command COMMAND, handling output (if any) interactively.
Display output in a buffer named \"*shell* COMMAND\" in another
window and loop reading a character from the keyboard.  Handling:
 TAB -- scroll the buffer
 SPC -- return to top-level, in the original buffer/window
 i   -- likewise, and insert COMMAND's output after point, as well
        (however, if the region is active, replace it w/ COMMAND's output)
 (any other key) -- like SPC, but discard COMMAND's output

COMMAND is passed through `rewrite-shell-command-w/cd'."
  ;; Thanks go to gsri for suggesting scrolling.
  (interactive "sShell command: ")
  (let ((buf (get-buffer-create (generate-new-buffer-name
                                 (concat "*shell* " command))))
        (oldw (selected-window))
        (browsingp t)
        (ra-p (region-active-p))
        (window-min-height 1))          ; override Emacs
    (shell-command (rewrite-shell-command-w/cd command) buf)
    (if (zerop (buffer-size buf))
        (kill-buffer buf)
      (switch-to-buffer-other-window buf)
      (shrink-window-if-larger-than-buffer)
      (cl-flet
          ((zonk () (kill-buffer buf) (delete-window) (setq browsingp nil)))
        (while browsingp
          (cl-case (read-char (format "TAB scrolls, SPC keeps, i %s"
                                      (if ra-p
                                          "replaces region"
                                        "inserts (after point)")))
            (?\C-i (if (pos-visible-in-window-p (point-max))
                       (goto-char (point-min))
                     (scroll-up)))
            (?\s (select-window oldw)
                 (setq browsingp nil))
            (?i (let ((text (buffer-string)))
                  (zonk)
                  (when ra-p (delete-region (point) (mark)))
                  (save-excursion (insert text))))
            (t (zonk))))))))

(provide 'outside)

;;; outside.el ends here
