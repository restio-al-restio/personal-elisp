;;; ppq.el                                          -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Pretty-print quietly.

;;;###autoload
(defun ppq (&optional replace)
  (interactive "P")
  (when replace
    (delete-region (point) (progn (forward-sexp 1) (point))))
  (save-excursion
    (insert "\n")
    (pp-eval-last-sexp t)
    (when (bolp)
      (delete-char -1))))

(provide 'ppq)

;;; ppq.el ends here
