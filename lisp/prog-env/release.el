;;; release.el                                      -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2000, 2001, 2002, 2003, 2004, 2005,
;;;   2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Context-dependent release!

(require 'dired)
(require 'more-vc)

;;;###autoload
(fset 'run-compile-command-on-region
      [?\M-w ?\M-m ?\C-a ?\C-y ?\C-k return])

;;;###autoload
(defun infer/run-compile-command ()
  (interactive)
  (compile (cond ((looking-at "(system")
                  (save-excursion
                    (cadr (read (current-buffer)))))
                 ((looking-at ";; do: \\(.+\\)$")
                  (match-string 1))
                 (t (error "Could not infer command"))))
  (forward-line 1)
  (skip-chars-forward " \n\t"))

;;;###autoload
(defun do-dot-release-el ()
  "From dired, run the batch file .release.el (note leading dot)."
  (interactive)
  (when (and (eq major-mode 'dired-mode)
             (file-exists-p ".release.el"))
    (compile (concat "emacs -batch -l .release.el"))))

;;;###autoload
(defun display-state-summary (dir &optional outbuf)
  "Do \"ttn-do display-state-summary\" on directory DIR.
Prefix arg means to insert output in the current buffer,
otherwise it goes to *Shell Command Output*."
  (interactive "DDirectory: \nP")
  (let ((max-mini-window-height 0))
    (shell-command (format "ttn-do display-state-summary %s"
                           (shell-quote-argument
                            (expand-file-name dir)))
                   outbuf)))

;;;###autoload
(defun release-dired (dir)
  "Run Dired for files with state \"Exp\" under directory DIR.
Determine state from `display-state-summary' output.
In the Dired buffer, you can mark files to be \"released\", that is,
to have their state changed to \"Rel\".  Type `C-c C-c' to do it.
Released files are removed from the listing.  After the last file
is removed, the Dired buffer is killed as well.

Note: There is a known bug: If you do not kill the Dired buffer,
      a subsequent `\\[dired]' will erroneously select and reuse it."
  (interactive "DDired for release (directory): ")
  (let (files topdir)
    (with-temp-buffer
      (cd dir)
      (display-state-summary "." t)
      (goto-char (point-min))
      (while (re-search-forward "^Exp.*[ ]\\(.+\\)$" (point-max) t)
        (push (match-string 1) files))
      (setq files (nreverse files)
            topdir (when files
                     (if (file-exists-p (car files))
                         dir
                       (when (save-excursion
                               (goto-char (point-max))
                               (re-search-backward "^cd \\([^ ;]+\\)"
                                                   (point-min) t))
                         (match-string 1))))))
    (if (not topdir)
        (message "No files in state `Exp'")
      (dired (cons topdir files) "-l --time-style=long-iso")
      (goto-char (point-min))
      (let ((inhibit-read-only t))
        (while (progn (forward-line 1)
                      (when (< (point) (point-max))
                        (forward-char 12)
                        t))
          (delete-region (point) (save-excursion
                                   (forward-sexp 4)
                                   (point)))))
      (goto-char (point-min))
      (end-of-line)
      (dired-goto-next-nontrivial-file)
      (use-local-map (copy-keymap (current-local-map)))
      (message "Type C-c C-c to release marked files")
      (local-set-key
       "\C-c\C-c"
       (lambda ()
         (interactive)
         (let* ((cur (cdr dired-directory))
                (sel (dired-get-marked-files t nil nil t))
                upd)
           (setq sel                    ; sigh
                 (cond ((eq t (car sel)) (cdr sel))
                       ((= 1 (length sel)) nil)
                       (t sel)))
           (if (not sel)
               (message "(No files marked)")
             (setq cur (cl-set-difference cur sel :test 'string=))
             (dired-do-kill-lines)
             (setcdr dired-directory cur)
             (message "Setting state...")
             (when (setq upd (more-vc-set-state sel "Rel"))
               (setq upd (length upd)
                     upd (format " (%d buffer%s updated)"
                                 upd (if (= 1 upd) "" "s"))))
             (message "Setting state...done%s" (or upd ""))
             (if (not cur)
                 (kill-buffer nil)
               (goto-char (point-min))
               (dired-goto-next-nontrivial-file)))))))))

(provide 'release)

;;; release.el ends here
