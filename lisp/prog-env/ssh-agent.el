;;; ssh-agent.el                                    -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2011 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Run ssh-agent(1) in a buffer.

(require 'comint)

(defvar ssh-agent-socket nil
  "Socket where ssh-agent(1) can be reached.
The environment variable ‘SSH_AUTH_SOCK’ is set based on this.")

;;;###autoload
(defun ssh-agent (action)
  "Invoke ssh-agent(1) to do ACTION.

If ACTION is the character ‘u’ (i.e., ‘?u’, ASCII 117), bring it \"up\"
with the Unix-domain socket specified by variable `ssh-agent-socket',
and set environment variable ‘SSH_AUTH_SOCK’ for the benefit of future
child processes.  If this command fails with \"address already in use\"
error, you must manually delete this socket and try again.

If ACTION is the character ‘d’ (i.e., ‘?d’, ASCII 100), bring it \"down\",
delete the socket file, and clear those environment variables.

Otherwise, display a message showing current status.

This command maintains a buffer *ssh-agent*;
killing it equivalent to doing ‘(ssh-agent ?d)’."
  (interactive "cBring ssh-agent (u)p or (d)own? (RET for status) ")
  (unless ssh-agent-socket
    (error "Variable `ssh-agent-socket' not set"))
  (switch-to-buffer (get-buffer-create "*ssh-agent*"))
  (comint-mode)
  (let ((proc (get-buffer-process (current-buffer))))
    (cl-case action
      (?u
       (when proc
         (error "ssh-agent already up"))
       (erase-buffer)
       (when (file-exists-p ssh-agent-socket)
         (if (y-or-n-p "Delete (likely to be) stale socket first? ")
             (delete-file ssh-agent-socket)
           (error "Stale socket: %s" ssh-agent-socket)))
       (accept-process-output
        (start-process "ssh-agent" (current-buffer) "ssh-agent"
                       "-d" "-a" (expand-file-name ssh-agent-socket)))
       (goto-char (point-min))
       (unless (looking-at "\\(SSH_AUTH_SOCK\\)=\\([^;]+\\);")
         (error "Sorry, cannot start ssh-agent"))
       (setenv (match-string 1) (match-string 2))
       (redisplay)
       (goto-char (point-max))
       ;; Do it here instead of for ‘?d’ action for the sake of ‘C-x C-k’.
       (remove-hook 'kill-buffer-query-functions
                    'process-kill-buffer-query-function)
       (set (make-local-variable 'kill-buffer-hook)
            (list (lambda ()
                    (let ((proc (get-buffer-process (current-buffer))))
                      (signal-process (process-id proc) 'QUIT)
                      (delete-process proc))
                    (when (file-exists-p ssh-agent-socket)
                      (delete-file ssh-agent-socket))
                    (setenv "SSH_AUTH_SOCK")))))
      (?d
       (unless proc
         (error "ssh-agent already down"))
       (kill-buffer nil))))
  (message "ssh-agent %s" (if (get-buffer-process (current-buffer))
                              'up
                            'down)))

(provide 'ssh-agent)

;;; ssh-agent.el ends here
