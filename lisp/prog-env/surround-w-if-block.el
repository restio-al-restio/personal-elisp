;;; surround-w-if-block.el                          -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996,
;;;   1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;; This file is part of ttn's personal elisp library, released under GNU
;;; GPL with ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

;;; Description: Surround region w/ C preprocessor block w/ luser-chosen tag.

;;;###autoload
(defun surround-w-if-block (begin end condition &optional arg)
  "Surrounds region w/ conditional.  Use prefix arg to specify \"0\",
otherwise the inputted conditional string is automagically upcased and
whitespace replaced w/ underscore."
  (interactive "r\nsCondition: \np")
  (let ((zerop (= 4 arg)))
    (setq condition
          (if zerop
              "0"
            (let ((s "")
                  (b 0))
              (while (string-match "\\S-+" condition b)
                (setq s (concat s (substring condition (match-beginning 0)
                                             (setq b (match-end 0)))
                                "_")))
              (upcase (substring s 0 -1)))))
    (save-excursion
      (goto-char end)
      (unless (bolp) (forward-line 1))
      (insert "#endif /* " condition " */\n")
      (goto-char begin)
      (unless (bolp) (beginning-of-line))
      (insert "#if" (if zerop "" "def") " " condition "\n"))))

(provide 'surround-w-if-block)

;;; surround-w-if-block.el ends here
