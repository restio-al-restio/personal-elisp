;;; update-diff.el                                  -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2010 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Update current buffer using its "git diff" command.

;;;###autoload
(defun update-diff ()
  "Update the current buffer using its \"git diff\" command.
The command should be near the beginning of the file at bol,
and start with \"git diff\" (so \"git diff-index\", etc, is ok).
It should be followed at some point by #### on a line by itself.
For example:

   #git diff-index [...]           ;; unused
   #git diff a q                   ;; unused
   git diff -p --stat q r/hack
   #git diff -p --stat q r/fix     ;; unused
   ####

The updated buffer has a time stamp and the output of the
git diff command.  It is in Diff mode, read-only."
  (interactive)
  (let ((cmd (progn (goto-char (point-min))
                    (re-search-forward "^git diff")
                    (buffer-substring (match-beginning 0)
                                      (line-end-position)))))
    (re-search-forward "^####.*")
    (let ((inhibit-read-only t))
      (delete-region (point) (point-max))
      (save-excursion
        (insert (format-time-string "\n\t\t\t\t\t\t%F %T\n\n")
                (shell-command-to-string cmd)))
      (when (buffer-file-name)
        (let ((backup-inhibited t))
          (save-buffer))))
    (diff-mode)
    (setq buffer-read-only t)))

(provide 'update-diff)

;;; update-diff.el ends here
