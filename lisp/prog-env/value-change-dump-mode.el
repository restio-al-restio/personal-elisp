;;; value-change-dump-mode.el                       -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.
;;;
;;; Description: Major mode for value-change-dump files.

;;;###autoload
(define-generic-mode value-change-dump-mode
  ;; comment-list
  nil
  ;; keyword-list
  nil
  ;; font-lock-list
  '(("^#[0-9]+\n" . font-lock-comment-face))
  ;; auto-mode-list
  '("\\.dump\\'")
  ;; function-list
  '((lambda ()
      (let ((m (make-sparse-keymap)))
        (suppress-keymap m)
        (define-keys m
          "n" (lambda () (interactive)
                (search-forward "\n#")
                (forward-char -1)
                (recenter 1))
          "p" (lambda () (interactive)
                (search-backward "\n#")
                (forward-char 1)
                (recenter 1)))
        (use-local-map m))
      (set-syntax-table (copy-syntax-table (syntax-table)))
      (modify-syntax-entry ?\" "w")
      (modify-syntax-entry ?\' "w")
      (modify-syntax-entry ?\| "w")
      (setq comment-use-syntax nil)))
  "Mode for viewing Value Change Dump files.
Time stamps are in `font-lock-comment-face';
`n' and `p' move to the next and previous entry, respectively.")

(provide 'value-change-dump-mode)

;;; value-change-dump-mode.el ends here
