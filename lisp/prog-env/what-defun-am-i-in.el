;;; what-defun-am-i-in.el                           -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1997, 1998, 2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Determine where cursor is syntactically for several modes.

;;;###autoload
(defun where-am-i ()
  "Momentarily hide non-initial lines of forms surrounding point.
The effect is to corrugate the parent forms so that their initial
lines have more chance of being visible.  Display these lines, some
with trailing \"[+N]\", where N is the count of the hidden lines after
the visible line.  Display the sum of all N in the echo area.

This uses `momentary-string-display' to do the work;
type SPC (swallowed) to continue editing."
  (interactive)
  (let* ((top (save-excursion
                (let ((beginning-of-defun-function nil)
                      (open-paren-in-column-0-is-defun-start t)
                      (defun-prompt-regexp nil))
                  (beginning-of-defun))
                (point)))
         (bol (line-beginning-position))
         (ov (make-overlay top bol))
         (wline (count-lines (window-start) bol))
         (skipped 0)
         lbol vis)
    (overlay-put ov 'invisible t)
    (save-excursion
      (font-lock-fontify-region top (line-end-position))
      (goto-char (setq lbol bol))
      (while (ignore-errors
               (while (memq (get-text-property (point) 'face)
                            '(font-lock-comment-face
                              font-lock-comment-delimiter-face
                              font-lock-string-face))
                 (goto-char (1- (previous-single-property-change
                                 (point) 'face))))
               (backward-up-list)
               (let* ((bol (line-beginning-position))
                      (eol (line-end-position))
                      (bye (count-lines (1+ eol)
                                        lbol)))
                 (push (concat (buffer-substring bol eol)
                               (unless (zerop bye)
                                 (propertize (format " [%+d]" bye)
                                             'face 'font-lock-warning-face))
                               "\n")
                       vis)
                 (cl-incf skipped bye)
                 (setq lbol (goto-char bol))))))
    ;; Take some pains to maintain visual serenity.  Unfortunately,
    ;; this loses if there is not enough text before the corrugation.
    (recenter (max 0 (- wline (length vis))))
    (unwind-protect (momentary-string-display
                     (apply 'concat vis)
                     bol nil
                     (format "(%d line%s hidden)"
                             skipped
                             (if (= 1 skipped)
                                 ""
                               "s")))
      (delete-overlay ov)
      (recenter wline))))

;;;###autoload
(defun mouse-where-am-i (event)
  "Set point at mouse EVENT (click) position and do `where-am-i' there."
  (interactive "@e")
  (mouse-set-point event)
  (where-am-i))

;;;###autoload
(defun where-am-i/texinfo ()
  "Display current location in the echo area.
This is represented by the two lines starting with the most
recent @node.  If point is before any node, display instead
one of:
  Before any node (first node N lines down)
  Nowhere apparently (unstructured)
depending on whether or not the first @node can be found."
  (interactive)
  (let ((p (point)))
    (message "%s" (save-excursion
                    (cond ((re-search-backward "^@node" nil t)
                           (buffer-substring (match-beginning 0)
                                             (line-end-position 2)))
                          ((re-search-forward "^@node" nil t)
                           (format "Before any node (first node %d lines down)"
                                   (1- (count-lines p (point)))))
                          (t
                           "Nowhere apparently (unstructured)"))))))

;;;###autoload
(defun what-defun-am-i-in ()
  "Returns string of current mode-specific \"defun\".
If run interactively, display using `message'."
  (interactive)
  (let (ret)
    (save-excursion
      (let ((defun-prompt-regexp (cl-case major-mode
                                   ((verilog-mode)
                                    "module\\s-+\\S-+\\s-*")
                                   (t
                                    nil))))
        (beginning-of-defun))
      (cl-case major-mode
        ((scheme-mode)
         (re-search-forward "define\\S-* +\\(.*\\)"))
        ((lisp-interaction-mode lisp-mode emacs-lisp-mode)
         (re-search-forward "defun +\\(\\S-+\\)"))
        ((c-mode c++-mode)
         (re-search-backward "\\(^\\S-+\\)"))
        ((verilog-mode)
         (re-search-forward "module +\\(\\S-+\\)"))
        ))
    (setq ret (buffer-substring (match-beginning 1) (match-end 1)))
    (if (called-interactively-p 'interactive)
        (message ret)
      ret)))

(provide 'what-defun-am-i-in)

;;; what-defun-am-i-in.el ends here
