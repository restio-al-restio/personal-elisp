;;; my-term-fixups.el                               -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Fix keys for vt220.

;;;###autoload
(defun my-term-fixups ()
  "if on vt220, sets find to be [C-s] and sets f11 to be really [f11]."
  (when (string= "vt220" (getenv "TERM"))
    (define-key function-key-map "\e[1~" [?\C-s])
    (define-key function-key-map "\e[23~" [f11])))

(provide 'my-term-fixups)

;;; my-term-fixups.el ends here
