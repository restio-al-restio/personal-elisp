;;; vt220-term.el                                   -*- lexical-binding: t -*-
;;;
;;; Copyright (C) 1993, 1994, 1995, 1996, 1997, 1998,
;;;   2004, 2007, 2008 Thien-Thi Nguyen
;;;
;;; This file is part of ttn's personal elisp library, released under
;;; the terms of the GNU General Public License as published by the
;;; Free Software Foundation; either version 3, or (at your option) any
;;; later version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Set things up for the greatly-missed (snif) vt220.

(require 'set-keys)

;;;###autoload
(defun vt220-term ()
  "Sets up vt220 terminals."
  (interactive)
  (when (string= "vt220" (getenv "TERM"))
    (global-set-keys
     '([select]     set-mark-command
       [insertchar] yank
       [deletechar] kill-region
       [help]       eval-expression
       [menu]       advertised-undo
       [f13]        previous-buffer
       [f14]        next-buffer
       [f17]        shrink-window
       [f18]        enlarge-window
       [f19]        goto-previous-window
       [f20]        other-window
       [select]     set-mark-command
       [insertchar] yank
       [deletechar] kill-region
       [help]       eval-expression
       [menu]       advertised-undo
       [f13]        previous-buffer
       [f14]        next-buffer
       [f17]        shrink-window
       [f18]        enlarge-window
       [f19]        goto-previous-window
       [f20]        other-window
       [f10]        give-me-a-scratch-lisp-buffer-now!
       [f11]        kill-buffer-close-window
       [f12]        delete-window
       ;; Add keybindings here.
       ))))

(provide 'vt220-term)

;;; vt220-term.el ends here
