$! vmsbuild.com
$!
$! Copyright (C) 2008 Thien-Thi Nguyen
$!
$! This file is part of ttn's personal elisp library, released under
$! the terms of the GNU General Public License as published by the
$! Free Software Foundation; either version 3, or (at your option) any
$! later version.  There is NO WARRANTY.  See file COPYING for details.
$!
$ emacs -batch -l [.do]make.el
$!
$! vmsbuild.com ends here
